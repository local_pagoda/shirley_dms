<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * PACKAGE DESCRIPTION
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

$lang['menu_home']		 		= 'Home';

$lang['menu_dashboard'] 		= 'Dashboard';
$lang['menu_system'] 			= 'System ';
$lang['menu_users'] 			= 'Users';
$lang['menu_groups'] 			= 'Groups';
$lang['menu_permissions'] 		= 'Permissions';
$lang['menu_group_permissions'] = 'Group Permissions';
$lang['menu_user_permissions'] 	= 'User Permissions';
$lang['menu_masters'] 	= 'Master Data';
$lang['menu_styles'] 	= 'Styles';
$lang['menu_masters'] 	= 'Master Data';
$lang['menu_products_codes'] 	= 'Product Codes';
$lang['menu_products'] 	= 'Product';
$lang['menu_product_weights'] 	= 'Product Weights';
$lang['menu_product_prices'] 	= 'Product Prices';
$lang['menu_product_order_details'] 	= 'Place Order';
$lang['menu_product_colors'] 	= 'Product Colors';
$lang['menu_order'] 	= 'Product Order';
$lang['menu_color'] 	= 'Product Color';

$lang['menu_confirm_order'] 	= 'Confirm Order';
$lang['menu_wholesaler_profiles'] 	= 'Wholesaler Profile';