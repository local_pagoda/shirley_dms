<?php $uri = explode("/", $this->uri->uri_string()); ?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <?php $css = (!isset($uri[1])) ? 'class="active"' : ''; ?> 
            <li <?php echo $css;?>>
                <a href="<?php echo site_url('admin'); ?>">
                    <i class="fa fa-dashboard"></i> <span><?php echo lang('menu_dashboard'); ?></span>
                </a>
            </li>

            <?php if(control('System', FALSE)):?>
                <?php $css = (isset($uri[1]) && in_array($uri[1], array('users', 'groups', 'permissions'))) ? 'active' : ''; ?>
                <li class="treeview <?php echo $css; ?>">
                    <a href="javascript:void(0)">
                        <i class="fa fa-gear"></i>
                        <span><?php echo lang('menu_system'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                         <?php if(control('Permissions', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'permissions') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/permissions')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_permissions');?></a></li>
                        <?php endif;?>
                        <?php if(control('Groups', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'groups') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/groups')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_groups');?></a></li>
                        <?php endif;?>
                        <?php if(control('Users', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'users') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/users')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_users');?></a></li>
                        <?php endif;?>

                        <?php if(control('Wholesaler Profiles', FALSE)):?>
                        <?php $css = (isset($uri[1]) && $uri[1] == 'wholesaler_profiles') ? 'class="active"' : ''; ?> 
                        <li <?php echo $css; ?>><a href="<?php echo site_url('admin/wholesaler_profiles')?>">
                            <i class="fa fa-circle-o"></i><?php echo lang('menu_wholesaler_profiles');?></a></li>
                        <?php endif;?>
                    </ul>
                </li>
            <?php endif;?>


             <?php if(control('Permissions', FALSE)):?>
             <li class="treeview <?php echo $css; ?>">
                    <a href="#">
                        <i class="fa fa-files-o"></i>
                        <span><?php echo lang('menu_masters'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if(control('Masters', FALSE)):?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'masters') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/masters')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_masters');?></a></li>
                        <?php endif;?>
                         
                          <?php if(control('Product Weights', FALSE)):?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'product_weights') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/product_weights')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_product_weights');?></a></li>
                        <?php endif;?>

                          <?php if(control('Product Prices', FALSE)):?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'product_prices') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/product_prices')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_product_prices');?></a></li>
                        <?php endif;?>

                        
                         
                          
                    </ul>
            </li>
           <?php endif;?>


            <?php if(control('Product Orders Details', FALSE)):?>
             <li class="treeview <?php echo $css; ?>">
                    <a href="<?php echo site_url('admin/product_order_details')?>">
                        <i class="fa fa-cubes"></i>
                        <span><?php echo lang('menu_order'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                     <ul class="treeview-menu">
                         <?php if(control('Product Orders Details', FALSE)):?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'product_orders') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/product_order_details')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_product_order_details');?></a></li>

                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/product_order_details/view_confirm_order')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_confirm_order');?></a></li>
                        <?php endif;?>
                      </ul>
                        
                    
            </li>
           <?php endif;?>


            <?php //if(control('Product Colors', FALSE)):?>
          <!--    <li class="treeview <?php echo $css; ?>">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span><?php echo lang('menu_color'); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                     <ul class="treeview-menu">
                         <?php if(control('Product Colors', FALSE)):?>
                            <?php $css = (isset($uri[1]) && $uri[1] == 'product_colors') ? 'class="active"' : ''; ?> 
                            <li <?php echo $css; ?>><a href="<?php echo site_url('admin/product_colors')?>"><i class="fa fa-circle-o"></i><?php echo lang('menu_product_colors');?></a></li>
                        <?php endif;?>
                      </ul>
                        
                  
            </li> -->
           <?php //endif;?>
           <?php if(control('Product Colors', FALSE)):?>
           <li <?php echo $css;?>>
                <a href="<?php echo site_url('admin/product_colors')?>">
                    <i class="fa fa-list"></i> <span><?php echo lang('menu_product_colors'); ?></span>
                </a>
            </li>

            <?php endif;?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>