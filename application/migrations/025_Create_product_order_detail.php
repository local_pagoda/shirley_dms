<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_pms
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_product_order_detail extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('odr_product_order_detail'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp', 'default'    => null),
                'updated_at'            => array('type' => 'timestamp', 'default'    => null),
                'deleted_at'            => array('type' => 'timestamp', 'default'    => null),
                
                'w_id'                  => array('type' => 'int',   'constraint' => 11,    'null' => TRUE),
                'date_of_order'                  => array('type' => 'varchar',   'constraint' => 225,    'null' => TRUE),
                'season_id'             => array('type' => 'int',   'constraint' => 11,    'null' => TRUE),
                'year_id'               => array('type' => 'varchar',   'constraint' => 255,    'null' => TRUE),
                
                'deposite'              => array('type' => 'varchar',   'constraint' => 255,    'null' => TRUE),
                'deposite_received'              => array('type' => 'date',   'null' => TRUE),
                'deposite_date'              => array('type' => 'varchar',   'constraint' => 255,    'null' => TRUE),

                'order_status'              => array('type' => 'varchar',   'constraint' => 255,    'null' => TRUE),

                'flag_id'             => array('type' => 'tinyint',   'constraint' => 2,    'null' => TRUE),
                
                ));

            $this->dbforge->create_table('odr_product_order_detail', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('odr_product_order_detail');
    }
}