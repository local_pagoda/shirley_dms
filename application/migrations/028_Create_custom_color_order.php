<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_pms
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_custom_color_order extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('odr_custom_color_order'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp', 'default'    => null),
                'updated_at'            => array('type' => 'timestamp', 'default'    => null),
                'deleted_at'            => array('type' => 'timestamp', 'default'    => null),
                
                
                'order_id'                  => array('type' => 'int',   'constraint' => 11,    'null' => TRUE),
                'color_id'                  => array('type' => 'int',   'constraint' => 11,    'null' => TRUE),
                'product_weight_id'           => array('type' => 'int',   'constraint' => 11,    'null' => TRUE),
                'qty'             => array('type' => 'int',   'constraint' => 11,    'null' => TRUE),
                'price'             => array('type' => 'decimal',   'constraint' => 11,    'null' => TRUE),
                'total'             => array('type' => 'decimal',   'constraint' => 11,    'null' => TRUE),
                
               ));

            $this->dbforge->create_table('odr_custom_color_order', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('odr_custom_color_order');
    }
}