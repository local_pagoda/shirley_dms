<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_pms
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_user_profile extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('auth_user_profile'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp', 'default'    => null),
                'updated_at'            => array('type' => 'timestamp', 'default'    => null),
                'deleted_at'            => array('type' => 'timestamp', 'default'    => null),
                

              

                'contact_no'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),

                'country_id'                  => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'state'                  => array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE,     'unsigned' => TRUE),
               
                'city'                  => array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE,     'unsigned' => TRUE),
                'street'                  => array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE,     'unsigned' => TRUE),
                'zip'                  => array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE,     'unsigned' => TRUE),
                'user_id'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'address'                  => array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE,     'unsigned' => TRUE),
               
               
                
                ));

            $this->dbforge->create_table('auth_user_profile', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('auth_user_profile');
    }
}