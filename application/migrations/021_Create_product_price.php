<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_pms
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_product_price extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('odr_product_price'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp', 'default'    => null),
                'updated_at'            => array('type' => 'timestamp', 'default'    => null),
                'deleted_at'            => array('type' => 'timestamp', 'default'    => null),
                

              

                'product_weight_id'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),

                'currency_id'                  => array('type' => 'int','constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
               
                
                'season_id'                  => array('type' => 'int', 'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
               
                'price'                  => array('type' => 'decimal', 'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'year_id'                  => array('type' => 'int','constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
          
    
                ));

            $this->dbforge->create_table('odr_product_price', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('odr_product_price');
    }
}