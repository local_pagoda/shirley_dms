<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_pms
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_wholesaler_profile extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('auth_wholesaler_profile'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',       'constraint' => 11,     'auto_increment' => TRUE),
                'created_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'updated_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'deleted_by'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'created_at'            => array('type' => 'timestamp', 'default'    => null),
                'updated_at'            => array('type' => 'timestamp', 'default'    => null),
                'deleted_at'            => array('type' => 'timestamp', 'default'    => null),
                

                'company_name'          => array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE,     'unsigned' => TRUE),
                'email_id'              => array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE,     'unsigned' => TRUE),
                'depositer_type_id'       => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'currency_id'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),

                'contact_no'            => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),

                'country_id'                  => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
                'state'                  => array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE,     'unsigned' => TRUE),
               
                'city'                  => array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE,     'unsigned' => TRUE),
                'street'                  => array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE,     'unsigned' => TRUE),
                'zip'                  => array('type' => 'varchar',       'constraint' => 255,     'null' => TRUE,     'unsigned' => TRUE),
                
                 'year_id'                  => array('type' => 'int',       'constraint' => 11,     'null' => TRUE,     'unsigned' => TRUE),
               
                
                ));

            $this->dbforge->create_table('auth_wholesaler_profile', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('auth_wholesaler_profile');
    }
}