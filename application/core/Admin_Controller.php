<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * PACKAGE DESCRIPTION
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Admin_Controller
 *
 * Extends the MY_Controller class
 * 
 */

class Admin_Controller extends MY_Controller 
{

	var $_container;
	var $_user_id;
	  var $_total;
	/**
	 * Load and set data for some common used libraries.
	 */
	public function __construct()
	{
		parent::__construct();
		 $this->_total =$this->get_confirm();

		// if ajax request & not user is logged in 
		// then redirect to login page
		if ( $this->input->is_ajax_request() && !is_loggedin()) {
			$this->output->set_status_header('999', 'ERROR');
			exit;
		}
		// Set container variable
		$this->_container = $this->config->item('template_admin') . "container.php";

		control('Control Panel');

		$this->_user_id = $this->session->userdata('id');
	}

	  public function get_confirm(){
        $this->load->model('product_order_details/product_order_detail_model');
        $id = $this->session->userdata('id');
        $this->db->where('created_by',$id);
        $this->db->where('flag_id',1);
        $total=$this->product_order_detail_model->find_count();
        $this->session->set_userdata('total',$total);

       //print_r($this->session->userdata('total'));
    }
}