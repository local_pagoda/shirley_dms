<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class Deposit_type_model extends MY_Model
{

    protected $_table = 'mst_deposit_type';

    protected $blamable = TRUE;

}