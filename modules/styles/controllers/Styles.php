<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Styles
 *
 * Extends the Public_Controller class
 * 
 */

class Styles extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Styles');

        $this->load->model('styles/style_model');
        $this->lang->load('styles/style');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('styles');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'styles';
		$this->load->view($this->_container,$data);
	}
}