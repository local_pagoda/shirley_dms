<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Styles
 *
 * Extends the Project_Controller class
 * 
 */

class AdminStyles extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Styles');

        $this->load->model('styles/style_model');
        $this->lang->load('styles/style');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('styles');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'styles';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->style_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->style_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->style_model->insert($data);
        }
        else
        {
            $success=$this->style_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		// $data['created_by'] = $this->input->post('created_by');
		// $data['updated_by'] = $this->input->post('updated_by');
		// $data['deleted_by'] = $this->input->post('deleted_by');
		// $data['created_at'] = $this->input->post('created_at');
		// $data['updated_at'] = $this->input->post('updated_at');
		// $data['deleted_at'] = $this->input->post('deleted_at');
		$data['style_no'] = $this->input->post('style_no');
		$data['style_name'] = $this->input->post('style_name');
		
		
        return $data;
   }
   public function get_styles(){
   	search_params();
		
		$total=$this->style_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->style_model->findAll();
		
		echo json_encode($rows);
		exit;
   
   }

   public function deleteStyles(){
   	$data['id'] = $this->input->post('id');

   	 $success = $this->style_model->delete($data['id']);
      
       
      
       $success = TRUE;
       $msg=lang('general_success');    
       echo json_encode(array('msg'=>$msg,'success'=>$success));
       exit;
   }
}