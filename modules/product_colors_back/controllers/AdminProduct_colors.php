<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Product_colors
 * 
 * Extends the Project_Controller class
 * 
 */

class AdminProduct_colors extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Product Colors');

        $this->load->model('product_colors/product_color_model');
        $this->lang->load('product_colors/product_color');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('product_colors');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'product_colors';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->product_color_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->product_color_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->product_color_model->insert($data);
        }
        else
        {
            $success=$this->product_color_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['product_id'] = $this->input->post('product_id');
		$data['color_id'] = $this->input->post('color_id');
		$data['ratio'] = $this->input->post('ratio');
		$data['group'] = $this->input->post('group');
		$data['custom'] = $this->input->post('custom');

        return $data;
   }
}