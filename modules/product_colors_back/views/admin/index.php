<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('product_colors'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('product_colors'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridProduct_colorToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridProduct_colorInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridProduct_colorFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridProduct_color"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowProduct_color">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-product_colors', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "product_colors_id"/>
            <table class="form-table">
				
				<tr>
					<td><lable for="style">Style</lable></td>
					<td><div id="style_id" class="number_general" name="style_id"></div></td>
				</tr>
				<tr>
					<td><label for="size">Size</label></td>
					<td><div id="product_id" class="number_general" name="product_id"></div></td>
				</tr>
				<tr>
					<td><label for='color_id'><?php echo lang('color_id')?></label></td>
					<td><div id='color_id' class='number_general' name='color_id'></div></td>
				</tr>
				<tr>
					<td><label for='ratio'><?php echo lang('ratio')?></label></td>
					<td><div id='ratio' class='number_general' name='ratio'></div></td>
				</tr>
				<tr>
					<td><label for='group'><?php echo lang('group')?></label></td>
					<td><input id='group' class='text_input' name='group'></td>
				</tr>
				<tr>
					<td><label for='custom'><?php echo lang('custom')?></label></td>
					<td><div id='custom' class='number_general' name='custom'></div></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxProduct_colorSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxProduct_colorCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">
 // var productAdapter;
var styleAdapter;

var sizeAdapter;

$(function(){

	var product_colorsDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			
			// { name: 'product_id', type: 'number' },
			{ name: 'style_name', type: 'string' },

			{ name: 'size', type: 'string' },

			{ name: 'color_id', type: 'number' },
			{ name: 'ratio', type: 'number' },
			{ name: 'group', type: 'string' },
			{ name: 'custom', type: 'number' },
			
        ],
		url: '<?php echo site_url("admin/product_colors/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	product_colorsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridProduct_color").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridProduct_color").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridProduct_color").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: product_colorsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridProduct_colorToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editProduct_colorRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer},
			{ text: 'Style Name',datafield: 'style_name',width: 150,filterable: true,renderer: gridColumnsRenderer },

			{ text: 'Size',datafield: 'size',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
			
			{ text: '<?php echo lang("color_id"); ?>',datafield: 'color_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("ratio"); ?>',datafield: 'ratio',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("group"); ?>',datafield: 'group',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("custom"); ?>',datafield: 'custom',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridProduct_color").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridProduct_colorFilterClear', function () { 
		$('#jqxGridProduct_color').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridProduct_colorInsert', function () { 
		openPopupWindow('jqxPopupWindowProduct_color', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowProduct_color").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowProduct_color").on('close', function () {
        reset_form_product_colors();
    });

    $("#jqxProduct_colorCancelButton").on('click', function () {
        reset_form_product_colors();
        $('#jqxPopupWindowProduct_color').jqxWindow('close');
    });

    /*$('#form-product_colors').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#product_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#product_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#color_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#color_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#ratio', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#ratio').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#group', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#group').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#custom', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#custom').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/
    var stylecount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'style_name',type: 'string'},
    	],
    	url:'<?php echo site_url('admin/styles/get_styles')?>'

    }
    styleAdapter = new $.jqx.dataAdapter(stylecount);

		 $("#style_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: styleAdapter,
			displayMember: "style_name",
			valueMember: "id",
	});

    var sizecount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'size',type: 'string'},
         { name: 'style_id',type: 'string'},
         
    	],
    	url:'<?php echo site_url('admin/product_prices/getstyles')?>'

    }
    sizeAdapter = new $.jqx.dataAdapter(sizecount);

		 $("#product_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: sizeAdapter,
			displayMember: "size",
			valueMember: "id",
		});
	$("#style_id").bind('select', function(event)
		{
			if (event.args)
			{
				$("#product_id").jqxComboBox({ disabled: false, selectedIndex: -1});		
				var value = event.args.item.value;
				sizecount.data = {style_id: value};
				sizeAdapter = new $.jqx.dataAdapter(sizecount, {
				    beforeLoadComplete: function (records) {
				        var filteredRecords = new Array();
				        for (var i = 0; i < records.length; i++) {
				            if (records[i].style_id == value)
				                filteredRecords.push(records[i]);
				        }
				        return filteredRecords;
				    }
				});
				$("#product_id").jqxComboBox({ source: sizeAdapter, autoDropDownHeight: sizeAdapter.records.length > 10 ? false : true});

			}
		});
    $("#jqxProduct_colorSubmitButton").on('click', function () {
        saveProduct_colorRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveProduct_colorRecord();
                }
            };
        $('#form-product_colors').jqxValidator('validate', validationResult);
        */
    });
});

function editProduct_colorRecord(index){
    var row =  $("#jqxGridProduct_color").jqxGrid('getrowdata', index);
  	if (row) {

		$('#product_id').jqxNumberInput('val', row.product_id);
		$('#color_id').jqxNumberInput('val', row.color_id);
		$('#ratio').jqxNumberInput('val', row.ratio);
		$('#group').val(row.group);
		$('#custom').jqxNumberInput('val', row.custom);
		
        openPopupWindow('jqxPopupWindowProduct_color', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveProduct_colorRecord(){
    var data = $("#form-product_colors").serialize();
	
	$('#jqxPopupWindowProduct_color').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/product_colors/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_product_colors();
                $('#jqxGridProduct_color').jqxGrid('updatebounddata');
                $('#jqxPopupWindowProduct_color').jqxWindow('close');
            }
            $('#jqxPopupWindowProduct_color').unblock();
        }
    });
}

function reset_form_product_colors(){
	$('#product_colors_id').val('');
    $('#form-product_colors')[0].reset();
}
</script>