<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['product_weight_id'] = 'Product Weight';
$lang['currency_id'] = 'Currency';
$lang['size_id'] = 'Size';
$lang['season_id'] = 'Season';
$lang['price'] = 'Price';

$lang['year_id'] = 'Year';

$lang['size_id'] = 'Size';
$lang['style_id'] = 'Style';

$lang['product_prices']='Product Prices';