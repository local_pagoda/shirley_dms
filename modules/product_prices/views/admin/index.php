<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('product_prices'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('product_prices'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridProduct_priceToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridProduct_priceInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridProduct_priceFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridProduct_price"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowProduct_price">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-product_prices', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "product_prices_id"/>
            <table class="form-table">
				<!-- <tr>
					<td><label for='created_by'><?php echo lang('created_by')?></label></td>
					<td><div id='created_by' class='number_general' name='created_by'></div></td>
				</tr>
				<tr>
					<td><label for='updated_by'><?php echo lang('updated_by')?></label></td>
					<td><div id='updated_by' class='number_general' name='updated_by'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_by'><?php echo lang('deleted_by')?></label></td>
					<td><div id='deleted_by' class='number_general' name='deleted_by'></div></td>
				</tr>
				<tr>
					<td><label for='created_at'><?php echo lang('created_at')?></label></td>
					<td><input id='created_at' class='text_input' name='created_at'></td>
				</tr>
				<tr>
					<td><label for='updated_at'><?php echo lang('updated_at')?></label></td>
					<td><input id='updated_at' class='text_input' name='updated_at'></td>
				</tr>
				<tr>
					<td><label for='deleted_at'><?php echo lang('deleted_at')?></label></td>
					<td><input id='deleted_at' class='text_input' name='deleted_at'></td>
				</tr> -->
				<!-- <tr>
					<td><label for='product_weight_id'><?php echo lang('product_weight_id')?></label></td>
					<td><div id='product_weight_id' class='number_general' name='product_weight_id'></div></td>
				</tr> -->

				<tr>
					<td><label for="style_id"><?php echo lang('style_id')?></label></td>
					<td><div id="style_id" class="number_general" name="style_id"></div></td>
				</tr>
				
				<tr>
					<td><label for="product_weight_id"><?php echo lang('size_id')?></label> </td>
					<td><div id="product_weight_id" class="number_general" name="product_weight_id"></div></td>
				</tr>

				<tr>
					<td><label for='currency_id'><?php echo lang('currency_id')?></label></td>
					<td><div id='currency_id' class='number_general' name='currency_id'></div></td>
				</tr>
				
				<tr>
					<td><label for='season_id'><?php echo lang('season_id')?></label></td>
					<td><div id='season_id' class='number_general' name='season_id'></div></td>
				</tr>
				<tr>
					<td><label for='price'><?php echo lang('price')?></label></td>
					<td><div id='price' class='number_general' name='price'></div></td>
				</tr>
				
				
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxProduct_priceSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxProduct_priceCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">
var countryAdapter;
var yearAdapter;
var styleAdapter;
var sizeAdapter;
var seasonAdapter;
$(function(){

	var product_pricesDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' 			},
			// { name: 'created_by', type: 'number' },
			// { name: 'updated_by', type: 'number' },
			// { name: 'deleted_by', type: 'number' },
			// { name: 'created_at', type: 'date' },
			// { name: 'updated_at', type: 'date' },
			// { name: 'deleted_at', type: 'date' },
			{ name: 'size_id', type: 'number' },
			{ name: 'currency_id', type: 'number' },
			{ name: 'season_id', type: 'number' },
			{ name: 'style_id', type: 'number' },
			{ name: 'style_no', type: 'string' },
			{ name: 'style_name', type: 'string' },
			{ name: 'size', type: 'string' },
			{ name: 'product_code', type: 'string' },
			{ name: 'currency', type: 'string' },
			{ name: 'style_no', type: 'string' },	
			{ name: 'currency', type: 'string' },	
			{ name: 'season', type: 'string' },			
			{ name: 'price', type: 'string' },
			{ name: 'year', type: 'number' },
			
        ],
		url: '<?php echo site_url("admin/product_prices/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	product_pricesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridProduct_price").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridProduct_price").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridProduct_price").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: product_pricesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridProduct_priceToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editProduct_priceRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					var f = '<a href ="javascript:void(0)" onclick="deletePriceRecord('+index+');return false;" title="Delete"><i class="fa fa-trash"></i></a>'
					return '<div style="text-align: center; margin-top: 8px;">' + e + '&nbsp;'+ f +'</div>';
				}
			},
		//	{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
			{ text: 'Style No',datafield: 'style_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: 'Style Name',datafield: 'style_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: 'Size',datafield: 'size',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: 'Product Code',datafield: 'product_code',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("currency_id"); ?>',datafield: 'currency',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
			{ text: '<?php echo lang("season_id"); ?>',datafield: 'season',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("price"); ?>',datafield: 'price',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
			{ text: '<?php echo lang("year_id"); ?>',datafield: 'year',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridProduct_price").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridProduct_priceFilterClear', function () { 
		$('#jqxGridProduct_price').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridProduct_priceInsert', function () { 
		openPopupWindow('jqxPopupWindowProduct_price', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowProduct_price").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowProduct_price").on('close', function () {
        reset_form_product_prices();
    });

    $("#jqxProduct_priceCancelButton").on('click', function () {
        reset_form_product_prices();
        $('#jqxPopupWindowProduct_price').jqxWindow('close');
    });

    /*$('#form-product_prices').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#product_weight_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#product_weight_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#currecny_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#currecny_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#size_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#size_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#aw_price', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#aw_price').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#ss_price', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#ss_price').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#ta_price', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#ta_price').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#cc_price', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#cc_price').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#year_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#year_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxProduct_priceSubmitButton").on('click', function () {
        saveProduct_priceRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveProduct_priceRecord();
                }
            };
        $('#form-product_prices').jqxValidator('validate', validationResult);
        */
    });


		  var currencycount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'currency',type: 'string'},
    	],
    	url:'<?php echo site_url('admin/currencies/get_currency')?>'

    }
    currencyAdapter = new $.jqx.dataAdapter(currencycount);

		 $("#currency_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: currencyAdapter,
			displayMember: "currency",
			valueMember: "id",
		});

		 	  

	var stylecount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'style_name',type: 'string'},
    	],
    	url:'<?php echo site_url('admin/styles/get_styles')?>'

    }
    styleAdapter = new $.jqx.dataAdapter(stylecount);

		 $("#style_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: styleAdapter,
			displayMember: "style_name",
			valueMember: "id",
	});
		 	 var seasoncount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'season',type: 'string'},
    	],
    	url:'<?php echo site_url('admin/seasons/getseason')?>'

    }
    seasonAdapter = new $.jqx.dataAdapter(seasoncount);

		 $("#season_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: seasonAdapter,
			displayMember: "season",
			valueMember: "id",
		});

		 

    var sizecount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'size',type: 'string'},
         { name: 'style_id',type: 'string'},
         
    	],
    	url:'<?php echo site_url('admin/product_prices/getstyles')?>'

    }
    sizeAdapter = new $.jqx.dataAdapter(sizecount);

		 $("#product_weight_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: sizeAdapter,
			displayMember: "size",
			valueMember: "id",
		});
$("#style_id").bind('select', function(event)
		{
			if (event.args)
			{
				$("#product_weight_id").jqxComboBox({ disabled: false, selectedIndex: -1});		
				var value = event.args.item.value;
				sizecount.data = {style_id: value};
				sizeAdapter = new $.jqx.dataAdapter(sizecount, {
				    beforeLoadComplete: function (records) {
				        var filteredRecords = new Array();
				        for (var i = 0; i < records.length; i++) {
				            if (records[i].style_id == value)
				                filteredRecords.push(records[i]);
				        }
				        return filteredRecords;
				    }
				});
				$("#product_weight_id").jqxComboBox({ source: sizeAdapter, autoDropDownHeight: sizeAdapter.records.length > 10 ? false : true});

			}
		});


		 

});

function editProduct_priceRecord(index){
    var row =  $("#jqxGridProduct_price").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#product_prices_id').val(row.id);		
		$('#currency_id').jqxComboBox('val',row.currency_id);
		$('#season_id').jqxComboBox('val',row.season_id);
		$('#price').val(row.price);
		$('#year_id').val(row.year);		
		$('#style_id').jqxComboBox('val',row.style_id);
		$('#product_weight_id').jqxComboBox('val',row.size_id);
			

		
        openPopupWindow('jqxPopupWindowProduct_price', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveProduct_priceRecord(){
    var data = $("#form-product_prices").serialize();
	
	$('#jqxPopupWindowProduct_price').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/product_prices/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_product_prices();
                $('#jqxGridProduct_price').jqxGrid('updatebounddata');
                $('#jqxPopupWindowProduct_price').jqxWindow('close');
            }
            $('#jqxPopupWindowProduct_price').unblock();
        }
    });
}
function deletePriceRecord(index){
	var row =  $("#jqxGridProduct_price").jqxGrid('getrowdata', index);
	var id =row.id;
	if(confirm("Are you sure want to delete!")==true){
		$.post("<?php echo site_url('admin/product_prices/deletePrices')?>",{id:id},function(result){
			
			if(result){


				reset_form_product_prices();
				
				$('#jqxGridProduct_price').jqxGrid('updatebounddata');
			}
		});

}

}
function reset_form_product_prices(){
	$('#product_prices_id').val('');
    $('#form-product_prices')[0].reset();
}
</script>