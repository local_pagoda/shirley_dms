<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Product_prices
 *
 * Extends the Public_Controller class
 * 
 */

class Product_prices extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Product Prices');

        $this->load->model('product_prices/product_price_model');
        $this->lang->load('product_prices/product_price');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('product_prices');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'product_prices';
		$this->load->view($this->_container,$data);
	}
}