<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Product_prices
 *
 * Extends the Project_Controller class
 * 
 */

class AdminProduct_prices extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Product Prices');

        $this->load->model('product_prices/product_price_model');
       
        $this->lang->load('product_prices/product_price');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('product_prices');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'product_prices';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->product_price_model->_table ="view_product_price";
		search_params();
		
		$total=$this->product_price_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->product_price_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data
        // print_r($data);
        // exit;
       $this->product_price_model->_table ="mst_year";
		search_params();
		$this->db->where('status',1);
		$rows=$this->product_price_model->find(); 
		$data['year_id'] =$rows->id;
		
		// print_r($data);
		// exit;
        if(!$this->input->post('id'))
        {
        	 
			$this->product_price_model->_table ="odr_product_price";
            $success=$this->product_price_model->insert($data);
        }
        else
        {
        	$this->product_price_model->_table ="odr_product_price";
            $success=$this->product_price_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		// $data['created_by'] = $this->input->post('created_by');
		// $data['updated_by'] = $this->input->post('updated_by');
		// $data['deleted_by'] = $this->input->post('deleted_by');
		// $data['created_at'] = $this->input->post('created_at');
		// $data['updated_at'] = $this->input->post('updated_at');
		// $data['deleted_at'] = $this->input->post('deleted_at');
		$data['product_weight_id'] = $this->input->post('product_weight_id');
		$data['currency_id'] = $this->input->post('currency_id');
		
		$data['season_id'] = $this->input->post('season_id');
		
		$data['price'] = $this->input->post('price');
		$data['year_id'] = $this->input->post('year_id');

        return $data;
   }

    public function getstyles(){
    	$this->product_price_model->_table ="view_product_weight";
   	    search_params();
		
		$total=$this->product_price_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->product_price_model->findAll();
		
		echo json_encode($rows);
		exit;
   
   }
 
 public function deletePrices(){
   	$data['id'] = $this->input->post('id');

   	 $success = $this->product_price_model->delete($data['id']);
       $success = TRUE;
       $msg=lang('general_success');    
       echo json_encode(array('msg'=>$msg,'success'=>$success));
       exit;
   }
}