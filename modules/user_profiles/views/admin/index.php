<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('user_profiles'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('user_profiles'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridUser_profileToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridUser_profileInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridUser_profileFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridUser_profile"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowUser_profile">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-user_profiles', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "user_profiles_id"/>
            <table class="form-table">
				<tr>
					<td><label for='created_by'><?php echo lang('created_by')?></label></td>
					<td><div id='created_by' class='number_general' name='created_by'></div></td>
				</tr>
				<tr>
					<td><label for='updated_by'><?php echo lang('updated_by')?></label></td>
					<td><div id='updated_by' class='number_general' name='updated_by'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_by'><?php echo lang('deleted_by')?></label></td>
					<td><div id='deleted_by' class='number_general' name='deleted_by'></div></td>
				</tr>
				<tr>
					<td><label for='created_at'><?php echo lang('created_at')?></label></td>
					<td><input id='created_at' class='text_input' name='created_at'></td>
				</tr>
				<tr>
					<td><label for='updated_at'><?php echo lang('updated_at')?></label></td>
					<td><input id='updated_at' class='text_input' name='updated_at'></td>
				</tr>
				<tr>
					<td><label for='deleted_at'><?php echo lang('deleted_at')?></label></td>
					<td><input id='deleted_at' class='text_input' name='deleted_at'></td>
				</tr>
				<tr>
					<td><label for='contact_no'><?php echo lang('contact_no')?></label></td>
					<td><div id='contact_no' class='number_general' name='contact_no'></div></td>
				</tr>
				<tr>
					<td><label for='country'><?php echo lang('country')?></label></td>
					<td><input id='country' class='text_input' name='country'></td>
				</tr>
				<tr>
					<td><label for='state'><?php echo lang('state')?></label></td>
					<td><input id='state' class='text_input' name='state'></td>
				</tr>
				<tr>
					<td><label for='address'><?php echo lang('address')?></label></td>
					<td><input id='address' class='text_input' name='address'></td>
				</tr>
				<tr>
					<td><label for='city'><?php echo lang('city')?></label></td>
					<td><input id='city' class='text_input' name='city'></td>
				</tr>
				<tr>
					<td><label for='street'><?php echo lang('street')?></label></td>
					<td><input id='street' class='text_input' name='street'></td>
				</tr>
				<tr>
					<td><label for='zip'><?php echo lang('zip')?></label></td>
					<td><input id='zip' class='text_input' name='zip'></td>
				</tr>
				<tr>
					<td><label for='user_id'><?php echo lang('user_id')?></label></td>
					<td><div id='user_id' class='number_general' name='user_id'></div></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxUser_profileSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxUser_profileCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

	var user_profilesDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'contact_no', type: 'number' },
			{ name: 'country', type: 'string' },
			{ name: 'state', type: 'string' },
			{ name: 'address', type: 'string' },
			{ name: 'city', type: 'string' },
			{ name: 'street', type: 'string' },
			{ name: 'zip', type: 'string' },
			{ name: 'user_id', type: 'number' },
			
        ],
		url: '<?php echo site_url("admin/user_profiles/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	user_profilesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridUser_profile").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridUser_profile").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridUser_profile").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: user_profilesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridUser_profileToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editUser_profileRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("contact_no"); ?>',datafield: 'contact_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("country"); ?>',datafield: 'country',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("state"); ?>',datafield: 'state',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("address"); ?>',datafield: 'address',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("city"); ?>',datafield: 'city',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("street"); ?>',datafield: 'street',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("zip"); ?>',datafield: 'zip',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("user_id"); ?>',datafield: 'user_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridUser_profile").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridUser_profileFilterClear', function () { 
		$('#jqxGridUser_profile').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridUser_profileInsert', function () { 
		openPopupWindow('jqxPopupWindowUser_profile', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowUser_profile").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowUser_profile").on('close', function () {
        reset_form_user_profiles();
    });

    $("#jqxUser_profileCancelButton").on('click', function () {
        reset_form_user_profiles();
        $('#jqxPopupWindowUser_profile').jqxWindow('close');
    });

    /*$('#form-user_profiles').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#contact_no', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#contact_no').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#country', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#country').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#state', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#state').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#address', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#address').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#city', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#city').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#street', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#street').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#zip', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#zip').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#user_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#user_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxUser_profileSubmitButton").on('click', function () {
        saveUser_profileRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveUser_profileRecord();
                }
            };
        $('#form-user_profiles').jqxValidator('validate', validationResult);
        */
    });
});

function editUser_profileRecord(index){
    var row =  $("#jqxGridUser_profile").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#user_profiles_id').val(row.id);
        $('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#created_at').val(row.created_at);
		$('#updated_at').val(row.updated_at);
		$('#deleted_at').val(row.deleted_at);
		$('#contact_no').jqxNumberInput('val', row.contact_no);
		$('#country').val(row.country);
		$('#state').val(row.state);
		$('#address').val(row.address);
		$('#city').val(row.city);
		$('#street').val(row.street);
		$('#zip').val(row.zip);
		$('#user_id').jqxNumberInput('val', row.user_id);
		
        openPopupWindow('jqxPopupWindowUser_profile', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveUser_profileRecord(){
    var data = $("#form-user_profiles").serialize();
	
	$('#jqxPopupWindowUser_profile').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/user_profiles/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_user_profiles();
                $('#jqxGridUser_profile').jqxGrid('updatebounddata');
                $('#jqxPopupWindowUser_profile').jqxWindow('close');
            }
            $('#jqxPopupWindowUser_profile').unblock();
        }
    });
}

function reset_form_user_profiles(){
	$('#user_profiles_id').val('');
    $('#form-user_profiles')[0].reset();
}
</script>