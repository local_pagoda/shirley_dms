<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * User_profiles
 *
 * Extends the Public_Controller class
 * 
 */

class User_profiles extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('User Profiles');

        $this->load->model('user_profiles/user_profile_model');
        $this->lang->load('user_profiles/user_profile');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('user_profiles');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'user_profiles';
		$this->load->view($this->_container,$data);
	}
}