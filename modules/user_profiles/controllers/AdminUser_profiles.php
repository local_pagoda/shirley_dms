<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * User_profiles
 *
 * Extends the Project_Controller class
 * 
 */

class AdminUser_profiles extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('User Profiles');

        $this->load->model('user_profiles/user_profile_model');
        $this->lang->load('user_profiles/user_profile');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('user_profiles');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'user_profiles';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->user_profile_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->user_profile_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->user_profile_model->insert($data);
        }
        else
        {
            $success=$this->user_profile_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['contact_no'] = $this->input->post('contact_no');
		$data['country'] = $this->input->post('country');
		$data['state'] = $this->input->post('state');
		$data['address'] = $this->input->post('address');
		$data['city'] = $this->input->post('city');
		$data['street'] = $this->input->post('street');
		$data['zip'] = $this->input->post('zip');
		$data['user_id'] = $this->input->post('user_id');

        return $data;
   }
}