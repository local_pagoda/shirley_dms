<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class User_profile_model extends MY_Model
{

    protected $_table = 'auth_user_profile';

    protected $blamable = TRUE;

}