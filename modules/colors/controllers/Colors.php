<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Colors
 *
 * Extends the Public_Controller class
 * 
 */

class Colors extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Colors');

        $this->load->model('colors/color_model');
        $this->lang->load('colors/color');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('colors');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'colors';
		$this->load->view($this->_container,$data);
	}
}