<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Colors
 *
 * Extends the Project_Controller class
 * 
 */

class AdminColors extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Colors');

        $this->load->model('colors/color_model');
        $this->lang->load('colors/color');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('colors');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'colors';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->color_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->color_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}
    public function get_color_all()
    {
		$rows=$this->color_model->findAll();
		echo json_encode(array('rows'=>$rows));
		exit;

    } 
	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->color_model->insert($data);
        }
        else
        {
            $success=$this->color_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['color'] = $this->input->post('color');

        return $data;
   }

   public function get_Color(){
   		search_params();
		
		$total=$this->color_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->color_model->findAll();
		
		echo json_encode($rows);
		exit;
   
   }

   public function deleteColors(){
   	$data['id'] = $this->input->post('id');

   	 $success = $this->color_model->delete($data['id']);
      
       
      
       $success = TRUE;
       $msg=lang('general_success');    
       echo json_encode(array('msg'=>$msg,'success'=>$success));
       exit;
   }
}