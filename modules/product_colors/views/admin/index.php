<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('product_colors'); ?></h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active"><?php echo lang('product_colors'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content"> 
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridProduct_colorToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridProduct_colorInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridProduct_colorFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridProduct_color"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowProduct_color">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title"></span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' =>'form-product_colors', 'onsubmit' => 'return false')); ?>
		<input type = "hidden" name = "id" id = "product_colors_id"/>
		<table class="form-table" >
			<tr>
				<td><label for="style">Style</label></td>
				<td><div id="style_id" class="number_general" name="style_id"></div></td>

				<td><label for="size">Size</label></td>
				<td><div id="product_id" class="number_general" name="product_id"></div></td>
			</tr>
			<tr>
				<td><label for='product_group'><?php echo lang('product_group')?></label></td>
				<td><input id='product_group' class='text_input' name='product_group'></td>

				<td><label for='custom'><?php echo lang('custom')?></label></td>
				<td><div id='custom' class='number_general' name='custom'></div></td>
			</tr>
		   <!--  <tr>
				<td><label for='color_id'><?php echo lang('color_id')?></label></td>
				<td><div id='color_id' class='number_general color_id' name='color_id[]'></div></td>
			</tr>
			<tr>
				<td><label for='ratio'><?php echo lang('ratio')?></label></td>
				<td><div id='ratio' class='number_general' name='ratio[]'></div></td>
			</tr> -->
			<!-- 
			<tr>
			    <td>
			        <input id="jqxGridColor_ratio2Insert" class="btn-primary" type="button" value="Add New" /> 
			    </td>    
			</tr> -->
			<tr>

				<td colspan="4">
					<div class="col-xs-12 connectedSortable">
						<?php echo displayStatus(); ?>
						<div id='jqxGridColor_ratioToolbar' class='grid-toolbar'>
							<p>Product Color Realation</p>
						</div>
						<div id="jqxGridColor_ratio"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td><button id="jqxcolor_ratioaddone" class="btn-primary">Add Color Ratio</button></td>
			</tr>



			<tr>
				<th colspan="2">
					<button type="button" class="btn btn-success btn-xs btn-flat" id="jqxProduct_colorSubmitButton"><?php echo lang('general_save'); ?></button>
					<button type="button" class="btn btn-default btn-xs btn-flat" id="jqxProduct_colorCancelButton"><?php echo lang('general_cancel'); ?></button>
				</th>
			</tr>

		</table>
		<?php echo form_close(); ?>
	</div>
</div>
<div id="jqxPopupWindowviewProduct_color_ratio">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title_color_ratio"></span>
	</div>
	<div class="col-xs-12 connectedSortable">
		<?php echo displayStatus(); ?>
		<div id='jqxGridColor_ratio2Toolbar' class='grid-toolbar'>

		</div>
		<div id="jqxGridColor_ratio2"></div>
	</div><!-- /.col -->
</div>	

<script language="javascript" type="text/javascript">
	var styleAdapter;
	var colorAdapter;
	var sizeAdapter;

	$(function(){
		$('#remove_color_ratio').attr('disabled',true);

		var product_colorsDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },

			{ name: 'product_id', type: 'number' },
			{ name: 'style_name', type: 'string' },

			{ name: 'size', type: 'string' },
			{ name: 'color', type: 'string' },

			{ name: 'style_id', type: 'number' },
			{ name: 'size_id', type: 'number' },

			{ name: 'color_id', type: 'number' },
			{ name: 'ratio', type: 'number' },
			{ name: 'product_group', type: 'string' },
			{ name: 'custom', type: 'number' },

			],
			url: '<?php echo site_url("admin/product_colors/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	product_colorsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridProduct_color").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridProduct_color").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridProduct_color").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: product_colorsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridProduct_colorToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index) {
				var e = '<a href="javascript:void(0)" onclick="editProduct_colorRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
				var f = '<a href="javascript:void(0)" onclick="deleteProduct_colorRecord(' + index + '); return false;" title="Delete"><i class="fa fa-trash"></i></a>';
				var v = '<a href="javascript:void(0)" onclick="viewProduct_colorRecord(' + index + '); return false;" title="View"><i class="fa fa-eye"></i></a>';

				return '<div style="text-align: center; margin-top: 8px;">' + v +'&nbsp;'+ e +'&nbsp;'+ f +'</div>';
			}
		},
		// { text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer},
		{ text: 'Style Name',datafield: 'style_name',width: 150,filterable: true,renderer: gridColumnsRenderer },

		{ text: 'Size',datafield: 'size',width: 150,filterable: true,renderer: gridColumnsRenderer },


		// { text: 'Color',datafield: 'color',width: 150,filterable: true,renderer: gridColumnsRenderer },
		// { text: '<?php echo lang("ratio"); ?>',datafield: 'ratio',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("product_group"); ?>',datafield: 'product_group',width: 150,filterable: true,renderer: gridColumnsRenderer },
		// { text: '<?php echo lang("custom"); ?>',datafield: 'custom',width: 150,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridProduct_color").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridProduct_colorFilterClear', function () { 
		$('#jqxGridProduct_color').jqxGrid('clearfilters');
	});

 //    $('#jqxGridProduct_colorInsert').click(function(){

	// });
	$(document).on('click','#jqxGridProduct_colorInsert', function () { 
		// $("#jqxGridColor_ratio").jqxGrid('clear');

		openPopupWindow('jqxPopupWindowProduct_color', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
	});
	// initialize the popup window

	$("#jqxPopupWindowProduct_color").jqxWindow({ 
		theme: theme,
		width: '75%',
		maxWidth: '75%',
		height: '75%',  
		maxHeight: '75%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});


	$("#jqxPopupWindowviewProduct_color_ratio").jqxWindow({ 
		theme: theme,
		width: '50%',
		maxWidth: '50%',
		height: '40%',  
		maxHeight: '40%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});
	$("#jqxPopupWindowProduct_color").on('close', function () {
		reset_form_product_colors();
	});

	$("#jqxProduct_colorCancelButton").on('click', function () {
		$("#jqxGridColor_ratio").jqxGrid('clear');

		reset_form_product_colors();
		$('#jqxPopupWindowProduct_color').jqxWindow('close');
	});
	var stylecount=
	{
		datatype:"json",
		datafields:[
		{ name: 'id',type: 'string'},
		{ name: 'style_name',type: 'string'},
		],
		url:'<?php echo site_url('admin/styles/get_styles')?>'

	}
	styleAdapter = new $.jqx.dataAdapter(stylecount);

	$("#style_id").jqxComboBox({
		theme: theme,
		width: 195,
		height: 25,
		selectionMode: 'dropDownList',
		autoComplete: true,
		searchMode: 'containsignorecase',
		source: styleAdapter,
		displayMember: "style_name",
		valueMember: "id",
	});

	var sizecount=
	{
		datatype:"json",
		datafields:[
		{ name: 'id',type: 'string'},
		{ name: 'size',type: 'string'},
		{ name: 'style_id',type: 'string'},

		],
		url:'<?php echo site_url('admin/product_prices/getstyles')?>'

	}
	sizeAdapter = new $.jqx.dataAdapter(sizecount);

	$("#product_id").jqxComboBox({
		theme: theme,
		width: 195,
		height: 25,
		selectionMode: 'dropDownList',
		autoComplete: true,
		searchMode: 'containsignorecase',
		source: sizeAdapter,
		displayMember: "size",
		valueMember: "id",
	});
    // var colorcount=
    // {
    // 	datatype:"json",
    // 	datafields:[
    // 	{ name: 'id',type: 'string'},
    // 	{ name: 'color',type: 'string'},
    // 	],
    // 	url:'<?php echo site_url('admin/colors/get_color_all')?>'

    // }
    // colorAdapter = new $.jqx.dataAdapter(colorcount);

    // $(".color_id").jqxComboBox({
    // 	theme: theme,
    // 	width: 195,
    // 	height: 25,
    // 	selectionMode: 'dropDownList',
    // 	autoComplete: true,
    // 	searchMode: 'containsignorecase',
    // 	source: colorAdapter,
    // 	displayMember: "color",
    // 	valueMember: "id",
    // });	 
    $("#style_id").bind('select', function(event)
    {
    	if (event.args)
    	{
    		$("#product_id").jqxComboBox({ disabled: false, selectedIndex: -1});		
    		var value = event.args.item.value;
    		sizecount.data = {style_id: value};
    		sizeAdapter = new $.jqx.dataAdapter(sizecount, {
    			beforeLoadComplete: function (records) {
    				var filteredRecords = new Array();
    				for (var i = 0; i < records.length; i++) {
    					if (records[i].style_id == value)
    						filteredRecords.push(records[i]);
    				}
    				return filteredRecords;
    			}
    		});
    		$("#product_id").jqxComboBox({ source: sizeAdapter, autoDropDownHeight: sizeAdapter.records.length > 10 ? false : true});

    	}
    });

    $("#jqxProduct_colorSubmitButton").on('click', function () {
    	saveProduct_colorRecord();

    });
});

function editProduct_colorRecord(index){
	// console.log("first"+index);
	// console.log("last"+index);

	var row =  $("#jqxGridProduct_color").jqxGrid('getrowdata', index);

	if (row) {
		$('#product_colors_id').val(row.id);

		$('#style_id').jqxComboBox('val',row.style_id);;
		$('#product_id').jqxComboBox('val',row.size_id);
		$('#product_group').val(row.product_group);
		$('#custom').jqxNumberInput('val', row.custom);

		var datarow_new;

		$.ajax({
			url: "<?php echo base_url('admin/color_ratios/json')?>",
			data: {rel_id: row.id},
			method: 'GET',
			success: function(data){
				$.each(data.rows,function(index,value){
					console.log(value);
					datarow_new = {
						'id'            :value.id,
						'color_ratio_id' :value.color_ratio_id,
						'color'         :value.color,
						'ratio'		  :value.ratio,  	  
					}
					$('#jqxGridColor_ratio').jqxGrid('addrow',null,datarow_new);
				});

				
			},
			dataType: 'json',
		});
		
		openPopupWindow('jqxPopupWindowProduct_color', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
	}
}

function saveProduct_colorRecord(){
	// console.log($('#jqxGridColor_ratio').jqxGrid('getrows'));
	var method = JSON.stringify($('#jqxGridColor_ratio').jqxGrid('getrows'));
	
	var data = $("#form-product_colors").serialize()+ '&method=' + method;
	// console.log(data);
	$('#jqxPopupWindowProduct_color').block({ 
		message: '<span>Processing your request. Please be patient.</span>',
		css: { 
			width                   : '75%',
			border                  : 'none', 
			padding                 : '50px', 
			backgroundColor         : '#000', 
			'-webkit-border-radius' : '10px', 
			'-moz-border-radius'    : '10px', 
			opacity                 : .7, 
			color                   : '#fff',
			cursor                  : 'wait' 
		}, 
	});

	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/product_colors/save"); ?>',
		data: data,
		success: function (result) {
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_product_colors();
				$('#jqxGridProduct_color').jqxGrid('updatebounddata');
				$('#jqxPopupWindowProduct_color').jqxWindow('close');
			}
			$('#jqxPopupWindowProduct_color').unblock();
		}
	});
}
function deleteProduct_colorRecord(index)
{
	var row =  $("#jqxGridProduct_color").jqxGrid('getrowdata', index);
	// console.log(row);
	if(confirm("Are you sure want to delete!!!") == true){
		$.post("<?php   echo site_url('admin/product_colors/delete')?>", {id:[row.id]}, function(result){
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_product_colors();
				$('#jqxGridProduct_color').jqxGrid('updatebounddata');
			}	
			
		});

	}

}

function viewProduct_colorRecord(index)
{
	var row =  $("#jqxGridProduct_color").jqxGrid('getrowdata', index);
		$("#jqxGridColor_ratio2").jqxGrid('clear');

	$.ajax({
		url: "<?php echo base_url('admin/color_ratios/json')?>",
		data: {rel_id: row.id},
		method: 'GET',
		success: function(data){
			$.each(data.rows,function(index,value){
				console.log(value);
				datarow_new = {
					'id'            :value.id,
					'color_ratio_id' :value.color_ratio_id,
					'color'         :value.color,
					'ratio'		  :value.ratio,  	  
				}
				$('#jqxGridColor_ratio2').jqxGrid('addrow',null,datarow_new);
			});


		},
		dataType: 'json',
		
	});
	openPopupWindow('jqxPopupWindowviewProduct_color_ratio', 'Show Color Relation');

}
function reset_form_product_colors(){
	$('#product_colors_id').val('');
	$('#form-product_colors')[0].reset();
}
</script>




<script language="javascript" type="text/javascript">
	var colorRatio;
	$(function(){

		var color_ratiosDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			
			{ name: 'color_ratio_id', type: 'number' },
			{ name: 'color', type: 'string' },

			{ name: 'ratio', type: 'number' },
			
			],
			url: '<?php echo site_url("admin/color_ratios/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	color_ratiosDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridColor_ratio").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridColor_ratio").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridColor_ratio").jqxGrid({
		theme: theme,
		width: '100%',
		height: '300',
		source: color_ratiosDataSource,	
		everpresentrowposition: "top",
		altrows: true,
		pageable: true,
		rowsheight: 30,
		columnsheight:30,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		editable: true,
		
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridColor_ratioToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, editable:false, cellsrenderer: rownumberRenderer , filterable: false},
		
		
		{ text: 'Color', datafield: 'color', displayfield: 'color', columntype: 'dropdownlist',
		createeditor: function (row, value, editor) {
			editor.jqxDropDownList({ source: colorratioAdapter, displayMember: 'color', valueMember: 'id' });
		}
	},			
		// { text: 'Color',datafield: 'color',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("ratio"); ?>',datafield: 'ratio',width: 150,filterable: true,renderer: gridColumnsRenderer },


		],
		rendergridrows: function (result) {
			return result.data;
		}
	});
	$("#jqxGridColor_ratio").on('cellendedit', function (event) {
		var column = $("#jqxGridColor_ratio").jqxGrid('getcolumn', event.args.datafield);
		console.log(event);
		if (column.displayfield != column.datafield) {
		}
		else {
		}
	});
	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridColor_ratio").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridColor_ratioFilterClear', function () { 
		$('#jqxGridColor_ratio').jqxGrid('clearfilters');
	});

	$('#jqxcolor_ratioaddone').click(function(){
		// alert();
		$('#jqxGridColor_ratio').jqxGrid('addrow', null, {});

	});

	var colorRatio=
	{
		datatype:"json",
		datafields:[
		{ name: 'id', type: 'number'},
		{ name: 'color',type: 'string'},
		],
		url:'<?php echo site_url('admin/colors/get_color_all')?>',
		cache: false

	}
	var colorratioAdapter = new $.jqx.dataAdapter(colorRatio);
	console.log(colorratioAdapter);

  //   $("#jqxColor_ratioSubmitButton").on('click', function () {
		// $("#jqxGridColor_ratio").jqxGrid('clear');

  //   	var color_id = $('#color_ratio_id').val();
  //   	var ratio = $('#ratio').val(); 
  //   	var newdata = {
	 //    		'ratio'             :ratio,
	 //    		'color'             :'Green',
  //   		};


  //   	console.log(newdata);
  //   	$("#jqxGridColor_ratio").jqxGrid('addrow', null, newdata);
  //       $('#jqxPopupWindowColor_ratio').jqxWindow('close');

  //   });



});


</script>
<script language="javascript" type="text/javascript">

	$(function(){

		var color_ratiosDataSource2 =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },
			
			{ name: 'color_ratio_id', type: 'number' },
			{ name: 'color', type: 'string' },

			{ name: 'ratio', type: 'number' },
			
			],
			url: '<?php echo site_url("admin/color_ratios/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	color_ratiosDataSource2.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridColor_ratio2").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridColor_ratio2").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridColor_ratio2").jqxGrid({
			theme: theme,
		width: '100%',
		height: '300',
		source: color_ratiosDataSource2,	
		altrows: true,
		pageable: true,
		rowsheight: 30,
		columnsheight:30,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		editable: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; margin-top: 10px; height:50px'></div>");
			container.append($('#jqxGridColor_ratio2Toolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		
	
		{ text: 'Color',datafield: 'color',width: 290,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("ratio"); ?>',datafield: 'ratio',width: 250,filterable: true,renderer: gridColumnsRenderer },


		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridColor_ratio2").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridColor_ratio2FilterClear', function () { 
		$('#jqxGridColor_ratio2').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridColor_ratio2Insert', function () { 
		openPopupWindow('jqxPopupWindowColor_ratio2', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
	});

});


</script>