<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Product_colors
 *
 * Extends the Public_Controller class
 * 
 */

class Product_colors extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Product Colors');

        $this->load->model('product_colors/product_color_model');
        $this->lang->load('product_colors/product_color');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('product_colors');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'product_colors';
		$this->load->view($this->_container,$data);
	}
}