<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Product_colors
 *
 * Extends the Project_Controller class
 * 
 */

class AdminProduct_colors extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Product Colors');

        $this->load->model('product_colors/product_color_model');
        $this->lang->load('product_colors/product_color');
        $this->load->model('color_ratios/color_ratio_model');
        $this->load->model('colors/color_model');


    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('product_colors');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'product_colors';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->product_color_model->_table = 'view_product_color';
		search_params();
		
		$total=$this->product_color_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->product_color_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}
	public function delete()
	{
       $data['id'] = $this->input->post('id')[0];
       $success = $this->product_color_model->delete($data['id']);
       // print_r($success); exit;
       $this->db->where('product_color_id',$data['id']);
       $this->color_ratio_model->delete_by();
       $success = TRUE;
       $msg=lang('general_success');    
       echo json_encode(array('msg'=>$msg,'success'=>$success));
       exit;

	}
	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data
        $method = json_decode($this->input->post('method')); 
		// print_r($method); exit;

        if(!$this->input->post('id'))
        {
        	// $data['date_of_order']= date('Y-m-d');
            $success=$this->product_color_model->insert($data);
            $data_id = $success;
            // print_r($data_id); exit;
        
        	foreach ($method as $key => $value) {
        		$color = $value->color;
        		$this->db->where('color',$color);
		        $colors=$this->color_model->find();
		        $data_['color_ratio_id'] = $colors->id;
        		$data_['ratio'] = $value->ratio;
        		$data_['product_color_id']  = $data_id;
                $success=$this->color_ratio_model->insert($data_);
                
        	}
        	// exit;
        }
        else
        {
            $success=$this->product_color_model->update($data['id'],$data);

            $this->db->where('product_color_id',$data['id']);
            $this->db->delete('rel_color_ratio');

            foreach ($method as $key => $value) {
        		$color = $value->color;
        		$this->db->where('color',$color);
		        $colors=$this->color_model->find();
		        $data_['color_ratio_id'] = $colors->id;
        		$data_['ratio'] = $value->ratio;
        		$data_['product_color_id']  = $data['id'];
                $success=$this->color_ratio_model->insert($data_);
                
        	}

        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}

		$data['product_id'] = $this->input->post('product_id');
		// $data['color_id'] = $this->input->post('color_id');
		// $data['ratio'] = $this->input->post('ratio');
		$data['product_group'] = $this->input->post('product_group');
		$data['custom'] = $this->input->post('custom');
        
        // print_r($data); exit;  
        return $data;
   }

   public function getcolors()
	{
		$this->product_color_model->_table ="odr_product_color";
		search_params();
		
		$total=$this->product_color_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->product_color_model->findAll();
		
		echo json_encode($rows);
		exit;
	}
}