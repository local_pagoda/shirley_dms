<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class Color_ratio_model extends MY_Model
{

    public $_table = 'rel_color_ratio';

    protected $blamable = TRUE;

}