<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('color_ratios'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('color_ratios'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridColor_ratioToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridColor_ratioInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridColor_ratioFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridColor_ratio"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowColor_ratio">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-color_ratios', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "color_ratios_id"/>
            <table class="form-table">
				
				<tr>
					<td><label for='color_ratio_id'><?php echo lang('color_ratio_id')?></label></td>
					<td><div id='color_ratio_id' class='number_general' name='color_ratio_id'></div></td>
				</tr>
				<tr>
					<td><label for='ratio'><?php echo lang('ratio')?></label></td>
					<td><div id='ratio' class='number_general' name='ratio'></div></td>
				</tr>
				<tr>
					<td><label for='product_color_id'><?php echo lang('product_color_id')?></label></td>
					<td><div id='product_color_id' class='number_general' name='product_color_id'></div></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxColor_ratioSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxColor_ratioCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

	var color_ratiosDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			
			{ name: 'color_ratio_id', type: 'number' },
			{ name: 'color', type: 'string' },

			{ name: 'ratio', type: 'number' },
			
        ],
		url: '<?php echo site_url("admin/color_ratios/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	color_ratiosDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridColor_ratio").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridColor_ratio").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridColor_ratio").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: color_ratiosDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridColor_ratioToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editColor_ratioRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
			{ text: 'Color',datafield: 'color',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("ratio"); ?>',datafield: 'ratio',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridColor_ratio").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridColor_ratioFilterClear', function () { 
		$('#jqxGridColor_ratio').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridColor_ratioInsert', function () { 
		openPopupWindow('jqxPopupWindowColor_ratio', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowColor_ratio").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowColor_ratio").on('close', function () {
        reset_form_color_ratios();
    });

    $("#jqxColor_ratioCancelButton").on('click', function () {
        reset_form_color_ratios();
        $('#jqxPopupWindowColor_ratio').jqxWindow('close');
    });

    /*$('#form-color_ratios').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#color_ratio_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#color_ratio_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#ratio', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#ratio').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#product_color_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#product_color_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxColor_ratioSubmitButton").on('click', function () {
        saveColor_ratioRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveColor_ratioRecord();
                }
            };
        $('#form-color_ratios').jqxValidator('validate', validationResult);
        */
    });
});

function editColor_ratioRecord(index){
    var row =  $("#jqxGridColor_ratio").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#color_ratios_id').val(row.id);
        $('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#created_at').val(row.created_at);
		$('#updated_at').val(row.updated_at);
		$('#deleted_at').val(row.deleted_at);
		$('#color_ratio_id').jqxNumberInput('val', row.color_ratio_id);
		$('#ratio').jqxNumberInput('val', row.ratio);
		$('#product_color_id').jqxNumberInput('val', row.product_color_id);
		
        openPopupWindow('jqxPopupWindowColor_ratio', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveColor_ratioRecord(){
    var data = $("#form-color_ratios").serialize();
	
	$('#jqxPopupWindowColor_ratio').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/color_ratios/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_color_ratios();
                $('#jqxGridColor_ratio').jqxGrid('updatebounddata');
                $('#jqxPopupWindowColor_ratio').jqxWindow('close');
            }
            $('#jqxPopupWindowColor_ratio').unblock();
        }
    });
}

function reset_form_color_ratios(){
	$('#color_ratios_id').val('');
    $('#form-color_ratios')[0].reset();
}
</script>