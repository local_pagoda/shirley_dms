<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Color_ratios
 *
 * Extends the Public_Controller class
 * 
 */

class Color_ratios extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Color Ratios');

        $this->load->model('color_ratios/color_ratio_model');
        $this->lang->load('color_ratios/color_ratio');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('color_ratios');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'color_ratios';
		$this->load->view($this->_container,$data);
	}
}