<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Color_ratios
 *
 * Extends the Project_Controller class
 * 
 */

class AdminColor_ratios extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Color Ratios');

        $this->load->model('color_ratios/color_ratio_model');
        $this->lang->load('color_ratios/color_ratio');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('color_ratios');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'color_ratios';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
	    $id = $this->input->get('rel_id'); 
	    if($id){
	    	$this->color_ratio_model->_table= 'view_color_relation';
			search_params();
			$this->db->where('product_color_id',$id);
			$total=$this->color_ratio_model->find_count();
			
			// $this->db->where('product_color_id',$id);
			paging('id');
			// $this->db->where('product_color_id',$id);
			search_params();
			$this->db->where('product_color_id',$id);
			
			$rows=$this->color_ratio_model->findAll();
	    	
	    }
	    else{
			
			$total = 0;
	    	$rows = array();
	    	
	    }
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->color_ratio_model->insert($data);
        }
        else
        {
            $success=$this->color_ratio_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['color_ratio_id'] = $this->input->post('color_ratio_id');
		$data['ratio'] = $this->input->post('ratio');
		$data['product_color_id'] = $this->input->post('product_color_id');

        return $data;
   }
}