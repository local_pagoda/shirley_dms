<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Product_weights
 *
 * Extends the Public_Controller class
 * 
 */

class Product_weights extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Product Weights');

        $this->load->model('product_weights/product_weight_model');
        $this->lang->load('product_weights/product_weight');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('product_weights');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'product_weights';
		$this->load->view($this->_container,$data);
	}
}