<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Product_weights
 *
 * Extends the Project_Controller class
 * 
 */

class AdminProduct_weights extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Product Weights');

        $this->load->model('product_weights/product_weight_model');
        $this->lang->load('product_weights/product_weight');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('product_weights');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'product_weights';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->product_weight_model->_table ="view_product_weight";
		
		search_params();
		
		$total=$this->product_weight_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->product_weight_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->product_weight_model->insert($data);
        }
        else
        {
            $success=$this->product_weight_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		// $data['created_by'] = $this->input->post('created_by');
		// $data['updated_by'] = $this->input->post('updated_by');
		// $data['deleted_by'] = $this->input->post('deleted_by');
		// $data['created_at'] = $this->input->post('created_at');
		// $data['updated_at'] = $this->input->post('updated_at');
		// $data['deleted_at'] = $this->input->post('deleted_at');
		$data['style_id'] = $this->input->post('style_id');
		$data['size_id'] = $this->input->post('size_id');
		$data['weight'] = $this->input->post('weight');
		$data['product_code'] = $this->input->post('product_code');
		
        return $data;
   }

    public function deleteWeights(){
   	$data['id'] = $this->input->post('id');

   	 $success = $this->product_weight_model->delete($data['id']);
      
     
      
       $success = TRUE;
       $msg=lang('general_success');    
       echo json_encode(array('msg'=>$msg,'success'=>$success));
       exit;
   }
}