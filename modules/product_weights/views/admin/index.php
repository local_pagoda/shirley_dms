<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('product_weights'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('product_weights'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridProduct_weightToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridProduct_weightInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridProduct_weightFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridProduct_weight"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowProduct_weight">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-product_weights', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "product_weights_id"/>
            <table class="form-table">
				<!-- <tr>
					<td><label for='created_by'><?php echo lang('created_by')?></label></td>
					<td><div id='created_by' class='number_general' name='created_by'></div></td>
				</tr>
				<tr>
					<td><label for='updated_by'><?php echo lang('updated_by')?></label></td>
					<td><div id='updated_by' class='number_general' name='updated_by'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_by'><?php echo lang('deleted_by')?></label></td>
					<td><div id='deleted_by' class='number_general' name='deleted_by'></div></td>
				</tr>
				<tr>
					<td><label for='created_at'><?php echo lang('created_at')?></label></td>
					<td><input id='created_at' class='text_input' name='created_at'></td>
				</tr>
				<tr>
					<td><label for='updated_at'><?php echo lang('updated_at')?></label></td>
					<td><input id='updated_at' class='text_input' name='updated_at'></td>
				</tr>
				<tr>
					<td><label for='deleted_at'><?php echo lang('deleted_at')?></label></td>
					<td><input id='deleted_at' class='text_input' name='deleted_at'></td>
				</tr> -->
				<tr>
					<td><label for='style_id'><?php echo lang('style_id')?></label></td>
					<td><div id='style_id' class='number_general' name='style_id'></div></td>
				</tr>
				<tr>
					<td><label for='size_id'><?php echo lang('size_id')?></label></td>
					<td><div id='size_id' class='number_general' name='size_id'></div></td>
				</tr>
				<tr>
					<td><label for='weight'><?php echo lang('weight')?></label></td>
					<td><div id='weight' class='number_general' name='weight'></div></td>
				</tr>
				<tr>
					<td><label for='product_code'><?php echo lang('product_code')?></label></td>
					<td><input id='product_code' class='text_input' name='product_code'></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxProduct_weightSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxProduct_weightCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">
 var sizeAdapter;
 var styleAdapter;
$(function(){

	var product_weightsDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			// { name: 'created_by', type: 'number' },
			// { name: 'updated_by', type: 'number' },
			// { name: 'deleted_by', type: 'number' },
			// { name: 'created_at', type: 'date' },
			{ name: 'size_id', type: 'number' },
			{ name: 'style_id', type: 'number' },
			{ name: 'style_no', type: 'string' },
			{ name: 'style_name', type: 'string' },
			{ name: 'product_code', type: 'string' },
			{ name: 'size', type: 'string' },
			{ name: 'description', type: 'string' },
			{ name: 'weight', type: 'number' },
			{ name: 'product_code', type: 'string' },
			
			
        ],
		url: '<?php echo site_url("admin/product_weights/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	product_weightsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridProduct_weight").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridProduct_weight").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridProduct_weight").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: product_weightsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridProduct_weightToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editProduct_weightRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					var f = '<a href ="javascript:void(0)" onclick="deleteWeightRecord('+index+');return false;" title="Delete"><i class="fa fa-trash"></i></a>'
					return '<div style="text-align: center; margin-top: 8px;">' + e + '&nbsp;'+ f +'</div>';
				}
			},
			
			{ text: 'Style No',datafield: 'style_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: 'Style Name',datafield: 'style_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
			{ text: '<?php echo lang("size_id"); ?>',datafield: 'size',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: 'Description',datafield: 'description',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("weight"); ?>',datafield: 'weight',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("product_code"); ?>',datafield: 'product_code',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridProduct_weight").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridProduct_weightFilterClear', function () { 
		$('#jqxGridProduct_weight').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridProduct_weightInsert', function () { 
		openPopupWindow('jqxPopupWindowProduct_weight', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowProduct_weight").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowProduct_weight").on('close', function () {
        reset_form_product_weights();
    });

    $("#jqxProduct_weightCancelButton").on('click', function () {
        reset_form_product_weights();
        $('#jqxPopupWindowProduct_weight').jqxWindow('close');
    });

    /*$('#form-product_weights').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#style_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#style_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#size_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#size_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#weight', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#weight').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#ratio', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#ratio').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxProduct_weightSubmitButton").on('click', function () {
        saveProduct_weightRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveProduct_weightRecord();
                }
            };
        $('#form-product_weights').jqxValidator('validate', validationResult);
        */
    });

     var sizecount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'size',type: 'string'},
    	],
    	url:'<?php echo site_url('admin/sizes/get_sizes')?>'

    }
    sizeAdapter = new $.jqx.dataAdapter(sizecount);

		 $("#size_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: sizeAdapter,
			displayMember: "size",
			valueMember: "id",
		});


		  var stylecount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'style_name',type: 'string'},
    	],
    	url:'<?php echo site_url('admin/styles/get_styles')?>'

    }
    styleAdapter = new $.jqx.dataAdapter(stylecount);

		 $("#style_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: styleAdapter,
			displayMember: "style_name",
			valueMember: "id",
		});
});

function editProduct_weightRecord(index){
    var row =  $("#jqxGridProduct_weight").jqxGrid('getrowdata', index);
    console.log(row);
  	if (row) {
  		$('#product_weights_id').val(row.id);
  //       $('#created_by').jqxNumberInput('val', row.created_by);
		// $('#updated_by').jqxNumberInput('val', row.updated_by);
		// $('#deleted_by').jqxNumberInput('val', row.deleted_by);
		// $('#created_at').val(row.created_at);
		// $('#updated_at').val(row.updated_at);
		// $('#deleted_at').val(row.deleted_at);
		//$('#style_id').val(row.style_name);
		//console.log(row.style_id);
		$('#style_id').jqxComboBox('val',row.style_id);
		// $('#style_id').val(row.style_name);
		$('#size_id').jqxComboBox('val',row.size_id);
		$('#weight').val(row.weight);
		$('#product_code').val(row.product_code);
		
		
        openPopupWindow('jqxPopupWindowProduct_weight', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}
function deleteWeightRecord(index){
	var row =  $("#jqxGridProduct_weight").jqxGrid('getrowdata', index);
	var id =row.id;
	if(confirm("Are you sure want to delete!")==true){
		$.post("<?php echo site_url('admin/product_weights/deleteWeights')?>",{id:id},function(result){
			
			if(result){


				reset_form_product_weights();
				
				$('#jqxGridProduct_weight').jqxGrid('updatebounddata');
			}
		});

}

}
function saveProduct_weightRecord(){
    var data = $("#form-product_weights").serialize();
	console.log(data);
	$('#jqxPopupWindowProduct_weight').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/product_weights/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_product_weights();
                $('#jqxGridProduct_weight').jqxGrid('updatebounddata');
                $('#jqxPopupWindowProduct_weight').jqxWindow('close');
            }
            $('#jqxPopupWindowProduct_weight').unblock();
        }
    });
}

function reset_form_product_weights(){
	$('#product_weights_id').val('');
    $('#form-product_weights')[0].reset();
}
</script>