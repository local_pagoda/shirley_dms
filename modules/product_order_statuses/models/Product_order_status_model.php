<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class Product_order_status_model extends MY_Model
{

    protected $_table = 'rel_product_order_status';

    protected $blamable = TRUE;

}