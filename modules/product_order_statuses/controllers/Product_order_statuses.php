<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Product_order_statuses
 *
 * Extends the Public_Controller class
 * 
 */

class Product_order_statuses extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Product Order Statuses');

        $this->load->model('product_order_statuses/product_order_status_model');
        $this->lang->load('product_order_statuses/product_order_status');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('product_order_statuses');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'product_order_statuses';
		$this->load->view($this->_container,$data);
	}
}