<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('product_orders'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('product_orders'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridProduct_orderToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridProduct_orderInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridProduct_orderFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridProduct_order"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowProduct_order">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-product_orders', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "product_orders_id"/>
            <table class="form-table">
				
				<tr>
					<td><label for='order_id'><?php echo lang('order_id')?></label></td>
					<td><div id='order_id' class='number_general' name='order_id'></div></td>
				</tr>
				<tr>
					<td><label for='group'><?php echo lang('group')?></label></td>
					<td><input id='group' class='text_input' name='group'></td>
				</tr>
				
				<tr>
					<td><label for='price'><?php echo lang('price')?></label></td>
					<td><input id='price' class='text_input' name='price'></td>
				</tr>
				<tr>
					<td><label for='total'><?php echo lang('total')?></label></td>
					<td><input id='total' class='text_input' name='total'></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxProduct_orderSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxProduct_orderCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

	var product_ordersDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'order_id', type: 'number' },
			{ name: 'group', type: 'string' },
			
			{ name: 'price', type: 'string' },
			{ name: 'total', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/product_orders/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	product_ordersDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridProduct_order").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridProduct_order").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridProduct_order").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: product_ordersDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridProduct_orderToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editProduct_orderRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("order_id"); ?>',datafield: 'order_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("group"); ?>',datafield: 'group',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
			{ text: '<?php echo lang("price"); ?>',datafield: 'price',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("total"); ?>',datafield: 'total',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridProduct_order").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridProduct_orderFilterClear', function () { 
		$('#jqxGridProduct_order').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridProduct_orderInsert', function () { 
		openPopupWindow('jqxPopupWindowProduct_order', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowProduct_order").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowProduct_order").on('close', function () {
        reset_form_product_orders();
    });

    $("#jqxProduct_orderCancelButton").on('click', function () {
        reset_form_product_orders();
        $('#jqxPopupWindowProduct_order').jqxWindow('close');
    });

   

    $("#jqxProduct_orderSubmitButton").on('click', function () {
        saveProduct_orderRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveProduct_orderRecord();
                }
            };
        $('#form-product_orders').jqxValidator('validate', validationResult);
        */
    });
});

function editProduct_orderRecord(index){
    var row =  $("#jqxGridProduct_order").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#product_orders_id').val(row.id);
        $('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#created_at').val(row.created_at);
		$('#updated_at').val(row.updated_at);
		$('#deleted_at').val(row.deleted_at);
		$('#order_id').jqxNumberInput('val', row.order_id);
		$('#group').val(row.group);
		
		$('#price').val(row.price);
		$('#total').val(row.total);
		
        openPopupWindow('jqxPopupWindowProduct_order', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveProduct_orderRecord(){
    var data = $("#form-product_orders").serialize();
	
	$('#jqxPopupWindowProduct_order').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/product_orders/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_product_orders();
                $('#jqxGridProduct_order').jqxGrid('updatebounddata');
                $('#jqxPopupWindowProduct_order').jqxWindow('close');
            }
            $('#jqxPopupWindowProduct_order').unblock();
        }
    });
}

function reset_form_product_orders(){
	$('#product_orders_id').val('');
    $('#form-product_orders')[0].reset();
}
</script>