<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Product_orders
 *
 * Extends the Public_Controller class
 * 
 */

class Product_orders extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Product Orders');

        $this->load->model('product_orders/product_order_model');
        $this->lang->load('product_orders/product_order');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('product_orders');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'product_orders';
		$this->load->view($this->_container,$data);
	}
}