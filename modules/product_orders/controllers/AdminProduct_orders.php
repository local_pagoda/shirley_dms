<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Product_orders
 *
 * Extends the Project_Controller class
 * 
 */

class AdminProduct_orders extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Product Orders');

        $this->load->model('product_orders/product_order_model');
        $this->lang->load('product_orders/product_order');
       //  $this->load->library('upload', $config);
       // $this->upload->initialize($config);
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('product_orders');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'product_orders';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->product_order_model->_table = "view_product_order";
		search_params();
		
		$total=$this->product_order_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->product_order_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->product_order_model->insert($data);
        }
        else
        {
            $success=$this->product_order_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['order_id'] = $this->input->post('order_id');
		$data['group_color'] = $this->input->post('group_color');
		
		$data['price'] = $this->input->post('price');
		$data['total'] = $this->input->post('total');

        return $data;
   }

   public function getProductorders(){
   	
   	    $order_id = $this->input->post('id');
    	// print_r($order_id);
     //    exit;
	    	$this->product_order_model->_table= 'view_product_order';
			search_params();
			$this->db->where('order_id',$order_id);
			$total=$this->product_order_model->find_count();
			
			
			//paging('id');
			
			search_params();
			$this->db->where('order_id',$order_id);
			
			$rows=$this->product_order_model->findAll();
            
	   
		echo json_encode($rows);
        exit;
	}
   		


    public function order_json()
    {
        $order_id = $this->input->post('id');
        $this->product_order_model->_table = "view_product_order";
        search_params();
        $this->db->where('order_id',$order_id);
        $total=$this->product_order_model->find_count();
        
        paging('id');
        
        search_params();
        $this->db->where('order_id',$order_id);
        $rows=$this->product_order_model->findAll();
        
        echo json_encode(array('total'=>$total,'rows'=>$rows));
        exit;
    }
function import_data(){

       $table ='product_order';
    	$config['upload_path'] = 'uploads/excel_imports/'.$table;
    	$config['allowed_types'] = 'xlsx|csv|xls';
    	$config['max_size'] = 10000;

    	$this->load->library('upload', $config);

    	if (!$this->upload->do_upload('userfile')) {
    		$error = array('error' => $this->upload->display_errors());
    		print_r($error);
    	} else {
    		$data = array('upload_data' => $this->upload->data());
    	}
    	$file = FCPATH . 'uploads/excel_imports/'.$table.'/' . $data['upload_data']['file_name'];

        // EXCEL Read
    	$this->load->library('Excel');
    	$objPHPExcel = PHPExcel_IOFactory::load($file);
    	$objReader = PHPExcel_IOFactory::createReader('Excel2007');

    	$objReader->setReadDataOnly(false);
        // $objPHPExcel = $objReader->load($file); // error in this line

    	$index = array(
    		


    		'product_order' => array(
    			
    			
                'order_id',
                'color_group_id',
                'product_weight_id',
    			'qty',
    			'price',
    			'total'
               
    			),

    		
    	
    			

    		);
    	$raw_data = array();
    	$data = array();
    	$view_data = array();

    	foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
    		if ($key == 0) {
    			$worksheetTitle = $worksheet->getTitle();
                $highestRow = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $nrColumns = ord($highestColumn) - 64;

                for ($row = 2; $row <= $highestRow; ++$row) {
                	for ($col = 0; $col < $highestColumnIndex; ++$col) {
                		$cell = $worksheet->getCellByColumnAndRow($col,
                			$row);
                		$val = $cell->getValue();
                		$raw_data[$row][$index[$table][$col]] = $val;
                		//$raw_data[$row]['id'] = $id;
                	}
                }
            }
        }//foreach
        // echo "<pre>";

      
            echo "<pre>";
            print_r($raw_data);
            exit;
    		 $this->db->trans_start();
		  $this->db->insert_batch('odr_'.$table, $raw_data); 

		    if ($this->db->trans_status() === FALSE) {
		    	$this->db->trans_rollback();
		    } else {
		    	$this->db->trans_commit();
		    }
		    // $referred_from = $this->session->userdata('referred_from');
		    $this->db->trans_complete();
              redirect($_SERVER['HTTP_REFERER']);

		}

		
public function getstyles(){
    $id = $this->input->post('id');
        //$this->product_order_model->_table ="view_product_weight";
        search_params();

        $this->db->select('size,id');
        $this->db->where('style_id',$id);
        $rows=$this->db->get('view_product_weight')->result();
        
        echo json_encode($rows);
        exit;
   
   }

   
}