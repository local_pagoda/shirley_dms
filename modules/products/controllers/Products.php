<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Products
 *
 * Extends the Public_Controller class
 * 
 */

class Products extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Products');

        $this->load->model('products/product_model');
        $this->lang->load('products/product');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('products');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'products';
		$this->load->view($this->_container,$data);
	}
}