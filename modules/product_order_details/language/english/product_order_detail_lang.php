<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['w_id'] = 'W Id';
$lang['date_of_order'] = 'Date Of Order';
$lang['season_id'] = 'Season';
$lang['year_id'] = 'Year';
$lang['order_id'] = 'Order';
$lang['color_group_id'] = 'Group Color';
$lang['product_weight_id'] = 'Product Weight Id';
$lang['price'] = 'Price';
$lang['total'] = 'Total';
$lang['qty'] = 'QTY';
$lang['color_id'] = 'Color';

$lang['product_order_details']='Order';
$lang['confirm_order_details']='Confirm Orders';