<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.custom-file-input.js'); ?>"></script>
<style type="text/css">
	.inputfile {
		width: 0.1px;
		height: 0.1px;
		opacity: 0;
		overflow: hidden;
		position: absolute;
		z-index: -1;
	}
	.inputfile + label {
		font-size: 12px;
		font-weight: 100;
		color: white;
		background-color: grey;
		padding: 0.625rem 1.25rem;
		/*display: inline-block;*/
	}

	.inputfile:focus + label,
	.inputfile + label:hover {
		background-color: cadetblue;
	}

</style>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('product_order_details'); ?></h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active"><?php echo lang('product_order_details'); ?></li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridProduct_order_detailToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridProduct_order_detailInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridProduct_order_detailFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridProduct_order_detail"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowProduct_order_detail">
	<div class='jqxExpander-custom-div'>
		<span class='popup_title' id="window_poptup_title"></span>
	</div>
	<div class="form_fields_area">
		<?php echo form_open('', array('id' =>'form-product_order_details', 'onsubmit' => 'return false')); ?>
		<input type = "hidden" name = "id" id = "product_order_details_id"/>

		<input type="hidden" id="w_id" name="w_id" value="<?php echo $this->session->userdata('w_id')?>">

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3"> <label for="season_id"><?php echo lang('season_id')?></label></div>
						<div class="col-sm-4">  <div id='season_id' class='number_general' name='season_id'></div></div>
					</div>



				</div>
			</div>

			
		</div>




		<!-- </table> -->
		<!--    <input id="jqxGridProduct_orderInsert" type="button" value="Add New" /> -->
		<div class="col-xs-12 connectedSortable" style="margin:20px">
			<?php echo displayStatus(); ?>
			<div style="margin-left: 10px; float: left;">
			</div>
			<div id='jqxGridProduct_orderToolbar' class='grid-toolbar'>
				<p>Product Order</p>
			</div>
			<div id="jqxGridProduct_order"></div>



		</div><!-- /.col -->
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-2">
				<button id="jqxproductorderone" class=" btn btn-primary">Add Porduct Order</button>
			</div>
			<div class="col-sm-6">
					<div class="input-group" id="customshow"> 

				<label>Custom Color Order</label> &nbsp &nbsp
                     <input type="checkbox" id="checkbox1"/>
          </div>
			</div>
			
	
		</div>
		
<div id="customselect" hidden>
		<div class="col-xs-12 connectedSortable" style="margin:20px">
					<?php echo displayStatus(); ?>
					<div style="margin-left: 10px; float: left;">
					</div>
					<div id='jqxGridCustom_color_orderToolbar' class='grid-toolbar'>
						<p>Customer Color Order</p>
			</div>
			<div id="jqxGridCustom_color_order"></div>



		</div><!-- /.col -->
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-2">
				<button id="jqxcustomadd" class="btn btn-primary" >Add Custom Color</button>
			</div>

		</div>
		
			
	</div>

		
<div class="footer" style="margin:15px;">
		  
		  
			
				<div class="row">
						<div class="col-sm-1"></div>
						<?php if($this->session->userdata('group_id')==2):?>
						<div class="col-sm-2" >
							<div id="confirmbutton"></div> 
						</div>
<?php endif;?>

						<div class="col-sm-2" style="float: right;">
							<button type="button" class="btn btn-success" id="jqxProduct_order_detailSubmitButton"><?php echo lang('general_save'); ?></button>
				<button type="button" class="btn btn-danger" id="jqxProduct_order_detailCancelButton"><?php echo lang('general_cancel'); ?></button>
						</div>
					</div>

				

	





			
		</div>
		<?php echo form_close(); ?>


      <!--   <form  action="<?php echo site_url('admin/product_orders/import_data'); ?>" method="post" enctype="multipart/form-data" style="text-align:right;">
							
							
								<input type="file" name="userfile" id="userfile" class="inputfile" data-multiple-caption="{count} files selected">										
								<label for="userfile"><i class="fa fa-upload"></i><span>Choose a file</span></label>
								<button class="btn bg-green waves-effect" id="btn_import_file" type="submit">Import</button>
								
								
								
							</form> -->

						</div>
					</div>
					<div id="jqxPopupWindowOrder">
						<div class="col-xs-12 connectedSortable" style="margin:20px">
						<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3"> <label for="season_id"><?php echo lang('season_id')?></label></div>
						<div class="col-sm-4"><input id='season' class='text_input' name='season' readonly>  </div>
					</div>



				</div>
			</div>

			
		</div>
						<div class="col-xs-12 connectedSortable" style="margin:20px">
							
							<div style="margin-left: 10px; float: left;">
							</div>
							<div id='jqxGridOrderToolbar' class='grid-toolbar'>
								<p>Product Order</p>
							</div>
							<div id="jqxGridOrder"></div>



						</div><!-- /.col -->


						<div class="col-xs-12 connectedSortable" style="margin:20px">
				
					<div style="margin-left: 10px; float: left;">
					</div>
					<div id='jqxGridCustomColorToolbar' class='grid-toolbar'>
						<p>Customer Color Order</p>
			</div>
			<div id="jqxGridCustomColor"></div>



		</div>



					</div>
				</div>
					<div id="jqxPopupWindowInvoice">
						<div class="col-xs-12 connectedSortable" style="margin:20px">
							
								<div class="row">
									<div class="col-md-3">
										<p>Season : </p><div id="season_invoice"></div>
									</div>   
									<div class="col-md-3">								 	
										<p>Order Date : </p><div id="date_of_order_invoice"></div>
									</div>
									<div class="col-md-3">								   	
										<p>Year : </p><div id="year_invoice"></div> 
									</div>
									<div class="col-md-3">

										<p>Deposite : </p><?php if($logingrop == 2){ ?><input type="text" id="deposite_invoice" class="text_input"><?php } else{   ?><p id="deposite_invoice" ></p><?php } ?>
									</div>    	
								</div>
								<br>

								<div class="col-xs-12 connectedSortable" style="margin:20px">
				
								<div style="margin-left: 10px; float: left;">
								</div>
								<div id='jqxGridOrderInvoiceToolbar' class='grid-toolbar'>
												<p>Customer Color Order</p>
									</div>
									<div id="jqxGridOrderInvoice"></div>



								</div>
											

								<div style="margin:20px;"></div>
								<div class="col-xs-12 connectedSortable" style="margin:20px">
							
								<div style="margin-left: 10px; float: left;">
								</div>
								<div id='jqxGridCustomInvoiceToolbar' class='grid-toolbar'>
									<p>Customer Color Order</p>
								</div>
								<div id="jqxGridCustomInvoice"></div>



							</div>
										
								<input type="hidden" id="order_id_invoice">
								<?php if($logingrop == 2){ ?>
								<input type="radio" name="status" value="Pending" id="pending">Pending
								<input type="radio" name="status" value="Editing" id="editing">Editing
								<input type="radio" name="status" value="Sent" id="sent">Sent
								<div class=" pull-right">
								   <button id="jqx_invoiceorder_save" class="btn-success">Save</button>
								   <button id="jqx_invoiceorder_Cancel" class="btn-danger">Cancel</button>

								</div>
								<?php } ?>
						</div><!-- /.col -->
					</div>
					<script language="javascript" type="text/javascript">
						var yearAdapter;
						var  colorgroupAdapter;
						var seasonAdapter;
						var wholesalerAdapter;
						var styleAdapter;
						var sizeAdapter;
						var group = '<?php echo $logingrop; ?>';
						$(function(){

							var product_order_detailsDataSource =
							{
								datatype: "json",
								datafields: [
								{ name: 'id', type: 'number' },
								{ name: 'deposite', type: 'number' },
								{ name: 'status', type: 'string' },
								{ name: 'order_status', type: 'string' },

								{ name: 'flag_id', type: 'number' },

			// { name: 'created_by', type: 'number' },
			// { name: 'updated_by', type: 'number' },
			// { name: 'deleted_by', type: 'number' },
			// { name: 'created_at', type: 'date' },
			// { name: 'updated_at', type: 'date' },
			// { name: 'deleted_at', type: 'date' },

			{ name: 'season', type: 'string' },
			{ name: 'season_id', type: 'number' },
			{ name: 'created_at', type: 'string' },
			{ name: 'year', type: 'string' },
			
			
			],
			url: '<?php echo site_url("admin/product_order_details/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	product_order_detailsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridProduct_order_detail").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridProduct_order_detail").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridProduct_order_detail").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: product_order_detailsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridProduct_order_detailToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
	    text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
	        cellsrenderer: function (index, row, columnfield, value, defaulthtml, columnproperties) {
	        	 
		        var check =  $("#jqxGridProduct_order_detail").jqxGrid('getrowdata', index);
				var e = '<a href="javascript:void(0)" onclick="viewProduct_order_detailRecord(' + index + '); return false;" title="View"><i class="fa fa-eye"></i></a>&nbsp;';
                if(group == 2){
			        e += '<a href="javascript:void(0)" onclick="editProduct_order_detailRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;';
					e += '<a href="javascript:void(0)" onclick="deleteProduct_order_detailRecord(' + index + '); return false;" title="Delete"><i class="fa fa-trash"></i></a>&nbsp;';
				}
				else if(check.flag_id == 0 && group == 105){	
					e += '<a href="javascript:void(0)" onclick="editProduct_order_detailRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;';
					e += '<a href="javascript:void(0)" onclick="deleteProduct_order_detailRecord(' + index + '); return false;" title="Delete"><i class="fa fa-trash"></i></a>&nbsp;';
				}
				else{

				}
	            if(group == 2){
	            	e += '<a href="javascript:void(0)" onclick="show_InvoiceProductOrder(' + index + '); return false;" title="Invoice"><i class="fa fa-files-o"></i></a>&nbsp;';
	            }
	            else if(check.status == 'Sent' && group == 105){
                   
					e += '<a href="javascript:void(0)" onclick="show_InvoiceProductOrder(' + index + '); return false;" title="Invoice"><i class="fa fa-files-o"></i></a>&nbsp;';
			    }
			    else{
			    	
			    }
				
				return '<div style="text-align: center; margin-top: 8px;">'+ e +'</div>';
	        }
		 
		},
		{ text: '<?php echo lang("season_id"); ?>',datafield: 'season',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("date_of_order"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("year_id"); ?>',datafield: 'year',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: 'Upfront Invoice Status',datafield: 'status',width: 150,filterable: true,renderer: gridColumnsRenderer },		
		// { text: 'Final Invoice Status',datafield: 'order_status',width: 150,filterable: true,renderer: gridColumnsRenderer },		

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridProduct_order_detail").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridProduct_order_detailFilterClear', function () { 
		$('#jqxGridProduct_order_detail').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridProduct_order_detailInsert', function () { 
		$('#jqxGridCustom_color_order').jqxGrid('clear');
		$('#jqxGridProduct_order').jqxGrid('clear');
		openPopupWindow('jqxPopupWindowProduct_order_detail', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
	});

	// initialize the popup window
	$("#jqxPopupWindowProduct_order_detail").jqxWindow({ 
		theme: theme,
		width: '75%',
		maxWidth: '75%',
		height: '75%',  
		maxHeight: '75%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});

	$("#jqxPopupWindowProduct_order_detail").on('close', function () {
		reset_form_product_order_details();
	});

	$("#jqxProduct_order_detailCancelButton").on('click', function () {
		
		reset_form_product_order_details();
		$('#jqxPopupWindowProduct_order_detail').jqxWindow('close');
	});


	$("#jqxProduct_order_detailSubmitButton").on('click', function () {
		saveProduct_order_detailRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveProduct_order_detailRecord();
                }
            };
        $('#form-product_order_details').jqxValidator('validate', validationResult);
        */
    });

	var seasoncount=
	{
		datatype:"json",
		datafields:[
		{ name: 'id',type: 'number'},
		{ name: 'season',type: 'string'},
		],
		url:'<?php echo site_url('admin/seasons/getseason')?>'

	}
	seasonAdapter = new $.jqx.dataAdapter(seasoncount);

	$("#season_id").jqxComboBox({
		theme: theme,
		width: 195,
		height: 25,
		selectionMode: 'dropDownList',
		autoComplete: true,
		searchMode: 'containsignorecase',
		source: seasonAdapter,
		displayMember: "season",
		valueMember: "id",
	});


	var colorgroupcount=
	{
		datatype:"json",
		datafields:[
		{ name: 'id',type: 'string'},
		{ name: 'product_group',type: 'string'},
		],
		url:'<?php echo site_url('admin/product_colors/getcolors')?>'

	}
	colorgroupAdapter = new $.jqx.dataAdapter(colorgroupcount);


	var stylecount=
	{
		datatype:"json",
		datafields:[
		{ name: 'id',type: 'number'},
		{ name: 'style_name',type: 'string'},
		],
		url:'<?php echo site_url('admin/styles/get_styles')?>'

	}
	styleAdapter = new $.jqx.dataAdapter(stylecount);

		//  $("#style_id").jqxComboBox({
		// 	theme: theme,
		// 	width: 195,
		// 	height: 25,
		// 	selectionMode: 'dropDownList',
		// 	autoComplete: true,
		// 	searchMode: 'containsignorecase',
		// 	source: styleAdapter,
		// 	displayMember: "style_name",
		// 	valueMember: "id",
		// });
		var sizecount=
		{
			datatype:"json",
			datafields:[
			{ name: 'id',type: 'string'},
			{ name: 'size',type: 'string'},
			{ name: 'style_id',type: 'string'},

			],
			url:'<?php echo site_url('admin/product_prices/getstyles')?>'

		}
		sizeAdapter = new $.jqx.dataAdapter(sizecount);

		//  $("#product_weight_id").jqxComboBox({
		// 	theme: theme,
		// 	width: 195,
		// 	height: 25,
		// 	selectionMode: 'dropDownList',
		// 	autoComplete: true,
		// 	searchMode: 'containsignorecase',
		// 	source: sizeAdapter,
		// 	displayMember: "size",
		// 	valueMember: "id",
		// });
$("#style_id").bind('select', function(event)
		{
			if (event.args)
			{
				$("#product_weight_id").jqxComboBox({ disabled: false, selectedIndex: -1});		
				var value = event.args.item.value;
				sizecount.data = {style_id: value};
				sizeAdapter = new $.jqx.dataAdapter(sizecount, {
				    beforeLoadComplete: function (records) {
				        var filteredRecords = new Array();
				        for (var i = 0; i < records.length; i++) {
				            if (records[i].style_id == value)
				                filteredRecords.push(records[i]);
				        }
				        return filteredRecords;
				    }
				});
				$("#product_weight_id").jqxComboBox({ source: sizeAdapter, autoDropDownHeight: sizeAdapter.records.length > 10 ? false : true});

			}
		});
});

function editProduct_order_detailRecord(index){
	var row =  $("#jqxGridProduct_order_detail").jqxGrid('getrowdata', index);
	$('#jqxGridCustom_color_order').jqxGrid('clear');
	$('#jqxGridProduct_order').jqxGrid('clear');

	var id =row.id;
	
$('#confirm_order').val(row.id);
	if (row) {
		$('#product_order_details_id').val(row.id);
		$('#date_of_order').val(row.date_of_order);
		
		$('#season_id').jqxComboBox('val',row.season_id);
		$.post("<?php echo site_url('admin/product_orders/getProductorders')?>",{id:id},function(data){
			
			
			
			$.each(data,function(key,val){
				
				
				var id 					= val.id;
				var order_id 				=	val.order_id;
				var style_name 				=	val.style_name;
				var style_id 				=	val.style_id;
				var size 				=	val.size;
				var product_weight_id 		=	val.product_weight_id;
				var product_group 				=	val.product_group;
				var qty 				=	val.qty;
				var price 				=	val.price;
				var total 			=	val.total;

				var dataadd = {

					'id' 		                :id,
					'order_id' 			        :order_id,
					'style_id' 			        :style_id,
					'size' 			        :size,
					'style_name' 			        :style_name,
					'product_weight_id' 			:product_weight_id,
					'product_group'			     :product_group,
					'qty' 				        :qty,
					'price' 				    :price,
					'total' 				    :total,

				};
				//console.log(dataadd);

				$('#jqxGridProduct_order').jqxGrid('addrow', null, dataadd);
				
				cal_cash_calculate();
			});




		},'json');

		
			$.post("<?php echo site_url('admin/custom_color_orders/getCustomOrders')?>",{id:id},function(data){
			
			console.log(data);
			if(data==""){


				 
				  $('#customshow').show();
			}

			else{
				

				$('#customselect').fadeToggle();
				$('#customshow').hide();
			}
			
			$.each(data,function(key,val){
				
				
				var id 					    = val.id;
				var order_id 				=	val.order_id;
				var style_name 				=	val.style_name;
				var style_id 				=	val.style_id;
				var size 				    =	val.size;
				var product_weight_id 		=	val.product_weight_id;
				var color_id 				=	val.color_id;
				var color 				    =	val.color;
				var qty 				    =	val.qty; 
				var price 				    =	val.price;
				var total 			        =	val.total;

				var customadd = {

					'id' 		                :id,
					'order_id' 			        :order_id,
					'style_id' 			        :style_id,
					'size' 			            :size,
					'style_name' 			    :style_name,
					'product_weight_id' 		:product_weight_id,
					'color_id'			        :color_id,
					'color'			            :color,
					'qty' 				        :qty,
					'price' 				    :price,
					'total' 				    :total,

				};
				console.log(customadd);

				$('#jqxGridCustom_color_order').jqxGrid('addrow', null, customadd);
				
				// cal_cash_calculate();
			});




		},'json');
			
		
		
		$('#confirmbutton').html( 
			'<button type="button" class="btn btn-info" id="confirm_order" onclick="send_confirm()">Confirm Order</button>');
		openPopupWindow('jqxPopupWindowProduct_order_detail', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');

	}
}

function deleteProduct_order_detailRecord(index)
{
	var row =  $("#jqxGridProduct_order_detail").jqxGrid('getrowdata', index);
	// console.log(row);
	if(confirm("Are you sure want to delete!!!") == true){
		$.post("<?php   echo site_url('admin/product_colors/delete')?>", {id:[row.id]}, function(result){
			var result = eval('('+result+')');
			if (result.success) {
				reset_form_product_order_details();
				$('#jqxGridProduct_order_detail').jqxGrid('updatebounddata');
			}	
			
		});

	}

}


function viewProduct_order_detailRecord(index){
	var row =  $("#jqxGridProduct_order_detail").jqxGrid('getrowdata', index);

	$('#jqxGridOrder').jqxGrid('clear');
	
	var id =row.id;
	$('#season').val(row.season);
	
	$.post("<?php echo site_url('admin/product_orders/getProductorders')?>",{id:id},function(data){

		$.each(data,function(key,val){
			

			var id 					= val.id;
			var order_id 				=	val.order_id;
			var style_name 				=	val.style_name;
			var product_code 				=	val.product_code;
			var size 		          =	val.size;
			var product_group 				=	val.product_group;
			var qty 				=	val.qty;
			var price 				=	val.price;
			var total 			    =	val.total;

			var detailadd = {

				'id' 		                :id,
				'order_id' 			        :order_id,
				'style_name' 			        :style_name,
				'product_code' 			        :product_code,
				'size' 			                  :size,
				'product_group'			     :product_group,
				'qty' 				        :qty,
				'price' 				    :price,
				'total' 				    :total,

			};

			$('#jqxGridOrder').jqxGrid('addrow', null, detailadd);

			
		});




		
	},'json');

	$.post("<?php echo site_url('admin/custom_color_orders/getCustomcolorOrders')?>",{id:id},function(data){
			
			
			
			$.each(data,function(key,val){
				
				
				var id 					    = val.id;
				var order_id 				=	val.order_id;
				var style_name 				=	val.style_name;
				var style_id 				=	val.style_id;
				var size 				    =	val.size;
				var product_weight_id 		=	val.product_weight_id;
				var color_id 				=	val.color_id;
				var color 				    =	val.color;
				var qty 				    =	val.qty; 
				var price 				    =	val.price;
				var total 			        =	val.total;

				var customadd = {

					'id' 		                :id,
					'order_id' 			        :order_id,
					'style_id' 			        :style_id,
					'size' 			            :size,
					'style_name' 			    :style_name,
					'product_weight_id' 		:product_weight_id,
					'color_id'			        :color_id,
					'color'			            :color,
					'qty' 				        :qty,
					'price' 				    :price,
					'total' 				    :total,

				};
				

				$('#jqxGridCustomColor').jqxGrid('addrow', null, customadd);
				
				// cal_cash_calculate();
			});




		},'json');
			

	
	openPopupWindow('jqxPopupWindowOrder', 'Show Order');
}

function saveProduct_order_detailRecord(){
	var product_order =JSON.stringify($("#jqxGridProduct_order").jqxGrid('getrows'));
	var custom_color =JSON.stringify($("#jqxGridCustom_color_order").jqxGrid('getrows'));



	var data = $("#form-product_order_details").serialize() + '&product_order=' + product_order + '&custom_color=' + custom_color;
   
   $('#jqxPopupWindowProduct_order_detail').block({ 
   	message: '<span>Processing your request. Please be patient.</span>',
   	css: { 
   		width                   : '75%',
   		border                  : 'none', 
   		padding                 : '50px', 
   		backgroundColor         : '#000', 
   		'-webkit-border-radius' : '10px', 
   		'-moz-border-radius'    : '10px', 
   		opacity                 : .7, 
   		color                   : '#fff',
   		cursor                  : 'wait' 
   	}, 
   });

   $.ajax({
   	type: "POST",
   	url: '<?php echo site_url("admin/product_order_details/save"); ?>',
   	data: data,
   	success: function (result) {
   		var result = eval('('+result+')');
   		if (result.success) {
   			reset_form_product_order_details();
   			$("#jqxGridProduct_order").jqxGrid('clear');
   			$('#jqxGridProduct_order_detail').jqxGrid('updatebounddata');
   			$('#jqxPopupWindowProduct_order_detail').jqxWindow('close');
   		}
   		$('#jqxPopupWindowProduct_order_detail').unblock();
   	}
   });
}

function reset_form_product_order_details(){
	$('#product_order_details_id').val('');
	$('#form-product_order_details')[0].reset();
}
$("#qty").change(function(){
	var qty = ($.isNumeric($("#qty").val()))?$("#qty").val():0;
	var price = ($.isNumeric($("#price").val()))?$("#price").val():0;
	var total_amount= qty*price;
	$('#total').val(total_amount);
});

$("#price").change(function(){
	var qty = ($.isNumeric($("#qty").val()))?$("#qty").val():0;
	var price = ($.isNumeric($("#price").val()))?$("#price").val():0;
	var amount= qty*price;
	$('#total').val(amount);
});
</script>

<script language="javascript" type="text/javascript">

	$(function(){


		var product_ordersDataSource =
		{
			datatype: "array",
			datafields: [
			{ name: 'id', type: 'number' },
			
			
			{ name: 'style_name', type: 'string' },
			{ name: 'style_id', type: 'number' },
			{ name: 'size', type: 'string' },
			{ name: 'color_group_id', type: 'string' },
			{ name: 'qty', type: 'string' },
			
			
			{ name: 'price', type: 'string' },
			{ name: 'total', type: 'string' },
			
			],
			url: '<?php echo site_url("admin/product_orders/json"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	product_ordersDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridProduct_order").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridProduct_order").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridProduct_order").jqxGrid({
		theme: theme,
		width: '100%',
		height: '300px',
		source: product_ordersDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		editable:true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		
        //everpresentrowposition: "top",
        rendertoolbar: function (toolbar) {
        	var container = $("<div style='margin: 5px; height:50px'></div>");
        	container.append($('#jqxGridProduct_orderToolbar').html());
        	toolbar.append(container);
        },
        columns: [
			

			{
				text: 'Style', datafield: 'style_id', displayfield: 'style_name',  width: 150, columntype: 'combobox',
				createeditor: function (row, column, editor) {
                        	
                            editor.jqxComboBox({ 
                            	autoDropDownHeight: true, 
                            	source:styleAdapter,
                            	displayMember: 'style_name', 
                            	valueMember: 'id'  
                            });


                        },



                        // update the editor's value before saving it.
                        cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
                            // return the old value, if the new value is empty.
                             if (newvalue !== oldvalue) {

                             	 $("#jqxGridProduct_order").jqxGrid('setcellvalue', row, "size", "Select a size..");


                             }
                        
                        }
                    },
                    {
                    	text: 'Size', datafield: 'product_weight_id',displayfield: 'size', width: 150, columntype: 'combobox',
     

                    	initeditor: function (row, cellvalue, editor, celltext, cellwidth, cellheight) {
	                           var id = $("#jqxGridProduct_order").jqxGrid('getcellvalue',row,"style_id");
	                            var size = editor.val();
	                            var sizes = new Array();
			                           	$.post("<?php echo site_url('admin/product_orders/getstyles')?>",{id:id},function(dataAdapter){
			                            		
			                          			
			        					sizes = dataAdapter;
			        					   editor.jqxComboBox({ autoDropDownHeight: true, source: sizes,displayMember: 'size',valueMember: 'id' });
			        					

											 },'json');
                           
                         

	                            // if (size != "Select a size...") {
	                            //     var index = sizes.indexOf(size);
	                            //     editor.jqxComboBox('selectIndex', index);
	                            // }

                        },
		                        cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
		                           
				                            if (newvalue !== oldvalue) {

				                            	
				                            	//console.log(newvalue.value);
				                            	var id = newvalue.value;
				                            	//alert(id);
				                            	
										$.post("<?php echo site_url('admin/product_order_details/getPrices')?>",{id:id},function(currency_data){

											
										var currency_data = currency_data.price;
										$("#jqxGridProduct_order").jqxGrid('setcellvalue', row, "price", (currency_data));
											},'json');

				                            } 
		                            	
		                        }
                    },

			

			{
				text: '<?php echo lang("color_group_id"); ?>', datafield: 'product_group', width: 200, columntype: 'combobox',
				createeditor: function (row, column, editor) {

					editor.jqxComboBox({ autoDropDownHeight: true, source:colorgroupAdapter ,displayMember: 'product_group', valueMember: 'id' });
				},



                        // update the editor's value before saving it.
                        cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
                            // return the old value, if the new value is empty.
                            if (newvalue == "") return oldvalue;
                        }
                    },
                    { 
                    	text: 'QTY',
                    	datafield: 'qty',
                    	width: 200,
                    	filterable: true,

                    	columntype: 'numberinput', 
                    	cellbeginedit: false,

                    	cellvaluechanging: function (row, datafield, columntype, oldvalue, newvalue) {

                    		if (newvalue != oldvalue) {

                    			//console.log(newvalue);
                    			var price = $("#jqxGridProduct_order").jqxGrid('getcellvalue', row, "price");

                    			var total_amount;
                    			total_amount =(price * newvalue);




                    			$("#jqxGridProduct_order").jqxGrid('setcellvalue', row, "total", (total_amount).toFixed(2));

                    			cal_cash_calculate(newvalue,price);


                    		};

                    	}


                    },
                   { text: '<?php echo lang("price"); ?>',datafield: 'price',width: 200,editable:false, filterable: true,renderer: gridColumnsRenderer },

                    { text: '<?php echo lang("total"); ?>',datafield: 'total',width: 200,filterable: true,renderer: gridColumnsRenderer },

                    ],
                    rendergridrows: function (result) {
                    	return result.data;
                    }
                });

$("[data-toggle='offcanvas']").click(function(e) {
	e.preventDefault();
	setTimeout(function() {$("#jqxGridProduct_order").jqxGrid('refresh');}, 500);
});

$(document).on('click','#jqxGridProduct_orderFilterClear', function () { 
	$('#jqxGridProduct_order').jqxGrid('clearfilters');
});

$(document).on('click','#jqxGridProduct_orderInsert', function () { 

		$("#jqxGridProduct_order").jqxGrid('clear');
	openPopupWindow('jqxPopupWindowProduct_order', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
});
$('#jqxproductorderone').click(function(){
		// alert();
		$('#jqxGridProduct_order').jqxGrid('addrow', null, {});

	});



});


function cal_cash_calculate(price,quantity) {


	var total = (price*quantity);
	
	$('#total').val(total);

	

}


</script>

s
<script language="javascript" type="text/javascript">

	$(function(){

		
	
	$("#jqxGridOrder").jqxGrid({
		theme: theme,
		width: '100%',
		height: '300px',
		
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridOrderToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		
		{ text: 'Product Code',datafield: 'product_code',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: 'Style Name',datafield: 'style_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: 'Size',datafield: 'size',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("qty"); ?>',datafield: 'qty',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: 'Color Group',datafield: 'product_group',width: 150,filterable: true,renderer: gridColumnsRenderer },

		{ text: '<?php echo lang("price"); ?>',datafield: 'price',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("total"); ?>',datafield: 'total',width: 150,filterable: true,renderer: gridColumnsRenderer },

		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridOrder").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridOrderFilterClear', function () { 
		$('#jqxGridOrder').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridOrderInsert', function () { 
		openPopupWindow('jqxPopupWindowOrder', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
	});

	// initialize the popup window
	$("#jqxPopupWindowOrder").jqxWindow({ 
		theme: theme,
		width: '75%',
		maxWidth: '75%',
		height: '75%',  
		maxHeight: '75%',  
		isModal: true, 
		autoOpen: false,
		modalOpacity: 0.7,
		showCollapseButton: false 
	});

	$("#jqxPopupWindowOrder").on('close', function () {

	});

	$("#jqxOrderCancelButton").on('click', function () {

		$('#jqxPopupWindowOrder').jqxWindow('close');
	});




});

function show_InvoiceProductOrder(index)
{
	var dataInvoiceOrder;
	var row =  $("#jqxGridProduct_order_detail").jqxGrid('getrowdata', index);
	//console.log(row);
	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/product_order_details/get_invoice_json"); ?>',
		data: {id : row.id},
		datatype: 'json',
		success: function (result) {
			result = eval('('+result+')')
			
			$('#season_invoice').html(row.season);
			$('#date_of_order_invoice').html(row.date_of_order);
			$('#year_invoice').html(row.year);
			$('#order_id_invoice').val(row.id);
			if(group == 2){
				$('#deposite_invoice').val(result.rows[2]['deposite']);
		    }
		    else{
				$('#deposite_invoice').html(result.rows[2]['deposite']);
		    }
			$.each(result.rows[0],function(key,val){
				dataInvoiceOrder = {
           			// 'season'            : row.season,
           			// 'date_of_order'		: row.date_of_order,
           			// 'year'				: row.year,
           			'product_code'		: val.product_code,
           			'size'				: val.size,
           			'qty'				: val.qty,
           			'style_name'		: val.style_name,
           			'product_group'		: val.product_group,
           			'price'				: val.price,
           			'total'				: val.total,
           			'product_weight_id' : val.product_weight_id,
           		}
           // console.log(dataInvoiceOrder);
           $('#jqxGridOrderInvoice').jqxGrid('addrow',null,dataInvoiceOrder);
				cal_cash_calculate();
           

       });

	$.each(result.rows[3],function(key,val){
				dataInvoiceCustom = {
           			// 'season'            : row.season,
           			// 'date_of_order'		: row.date_of_order,
           			// 'year'				: row.year,
           			
           			'size'				: val.size,
           			'qty'				: val.qty,
           			'style_name'		: val.style_name,
           			'color'		: val.color,
           			'price'				: val.price,
           			'total'				: val.total,
           			'product_weight_id' : val.product_weight_id,
           		}
           console.log(dataInvoiceCustom);
           $('#jqxGridCustomInvoice').jqxGrid('addrow',null,dataInvoiceCustom);
				//cal_cash_calculate();
           

       });




			if(result.rows[1][0]['status'] == 'Pending')
			{ 
				$('#pending').attr('checked', true);
			}
			else if(result.rows[1][0]['status'] == 'Editing')
			{
				$('#editing').attr('checked', true);
			}
			else{
				$('#sent').attr('checked', true);
			}
            // $('#jqxPopupWindowProduct_order_detail').unblock();
        }
    });
	// alert(row);
	openPopupWindow('jqxPopupWindowInvoice', 'Upfront <?php echo $header ?> Invoice ');

}



$("#jqxGridOrderInvoice").jqxGrid({
	theme: theme,
	width: '90%',
	height: '300px',
	altrows: true,
	pageable: true,
	sortable: true,
	rowsheight: 30,
	columnsheight:30,
	showfilterrow: true,
	filterable: true,
	columnsresize: true,
	autoshowfiltericon: true,
	columnsreorder: true,
	selectionmode: 'none',
	virtualmode: true,
	enableanimations: false,
	editable: true,
	pagesizeoptions: pagesizeoptions,
	showtoolbar: true,
	rendertoolbar: function (toolbar) {
		var container = $("<div style='margin: 5px; height:50px'></div>");
		container.append($('#jqxGridOrderInvoiceToolbar').html());
		toolbar.append(container);
	},
	columns: [
	{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
	{ text: 'Product Weight Id', editable: false, datafield: 'product_weight_id',width: 100,filterable: true,renderer: gridColumnsRenderer },
	
	{ text: 'Product Code', editable: false, datafield: 'product_code',width: 100,filterable: true,renderer: gridColumnsRenderer },
	{ text: 'Style Name',datafield: 'style_name', editable: false, width: 100,filterable: true,renderer: gridColumnsRenderer },
	{ text: 'Size',datafield: 'size',width: 100, editable: false, filterable: true,renderer: gridColumnsRenderer },
	{ text: 'Groups',datafield: 'product_group',width: 100, editable: false, filterable: true,renderer: gridColumnsRenderer },
	{ 
		text: 'QTY',
		datafield: 'qty',
		width: 100,
		filterable: true,

		columntype: 'numberinput', 
		cellbeginedit: false,

		cellvaluechanging: function (row, datafield, columntype, oldvalue, newvalue) {

			if (newvalue != oldvalue) {

				//console.log(newvalue);
				var price = $("#jqxGridOrderInvoice").jqxGrid('getcellvalue', row, "price");

				var total_amount;
				total_amount =(price * newvalue);




				$("#jqxGridOrderInvoice").jqxGrid('setcellvalue', row, "total", (total_amount).toFixed(2));

				cal_cash_calculate(newvalue,price);


			};

		}


	},

	{ 
		text: 'Price',
		datafield: 'price',
		width: 100,
		filterable: true,

		columntype: 'numberinput', 
		cellbeginedit: false,

		cellvaluechanging: function (row, datafield, columntype, oldvalue, newvalue) {

			if (newvalue != oldvalue) {

				var quantity = $("#jqxGridOrderInvoice").jqxGrid('getcellvalue', row, "qty");

				var total_amount;
				total_amount =(newvalue * quantity);




				$("#jqxGridOrderInvoice").jqxGrid('setcellvalue', row, "total", (total_amount).toFixed(2));

				cal_cash_calculate(newvalue,quantity);


			};

		}

	},

	{ text: '<?php echo lang("total"); ?>',datafield: 'total',width: 100,filterable: true,renderer: gridColumnsRenderer },

	],
	rendergridrows: function (result) {
		return result.data;
	}
});


$("#jqxGridCustomInvoice").jqxGrid({
		theme: theme,
	width: '90%',
	height: '300px',
	altrows: true,
	pageable: true,
	sortable: true,
	rowsheight: 30,
	columnsheight:30,
	showfilterrow: true,
	filterable: true,
	columnsresize: true,
	autoshowfiltericon: true,
	columnsreorder: true,
	selectionmode: 'none',
	virtualmode: true,
	enableanimations: false,
	editable: true,
	pagesizeoptions: pagesizeoptions,
	showtoolbar: true,

		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridCustomInvoiceToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
	
		
			{
				text: 'Style', datafield: 'style_id', displayfield: 'style_name',  width: 150, columntype: 'combobox',
				createeditor: function (row, column, editor) {
                        	
                            editor.jqxComboBox({ 
                            	autoDropDownHeight: true, 
                            	source:styleAdapter,
                            	displayMember: 'style_name', 
                            	valueMember: 'id'  
                            });


                        },



                        // update the editor's value before saving it.
                        cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
                            // return the old value, if the new value is empty.
                             if (newvalue !== oldvalue) {

                             	 $("#jqxGridCustom_color_order").jqxGrid('setcellvalue', row, "size", "Select a size..");


                             }
                        
                        }
                    },
                    {
                    	text: 'Size', datafield: 'product_weight_id',displayfield: 'size', width: 150, columntype: 'combobox',
     

                    	initeditor: function (row, cellvalue, editor, celltext, cellwidth, cellheight) {
	                           var id = $("#jqxGridCustom_color_order").jqxGrid('getcellvalue',row,"style_id");
	                            var size = editor.val();
	                            var sizes = new Array();
			                           	$.post("<?php echo site_url('admin/product_orders/getstyles')?>",{id:id},function(dataAdapter){
			                            		
			                          			
			        					sizes = dataAdapter;
			        					   editor.jqxComboBox({ autoDropDownHeight: true, source: sizes,displayMember: 'size',valueMember: 'id' });
			        					

											 },'json');
                           
                         

	                            // if (size != "Select a size...") {
	                            //     var index = sizes.indexOf(size);
	                            //     editor.jqxComboBox('selectIndex', index);
	                            // }

                        },
		                        cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
		                           
				                            if (newvalue !== oldvalue) {

				                            	
				                            	//console.log(newvalue.value);
				                            	var id = newvalue.value;
				                            	//alert(id);
				                            	
										$.post("<?php echo site_url('admin/product_order_details/getPrices')?>",{id:id},function(currency_data){

											
										var currency_data = currency_data.price;
										$("#jqxGridCustom_color_order").jqxGrid('setcellvalue', row, "price", (currency_data));
											},'json');

				                            } 
		                            	
		                        }
                    },

			

				{
				text: '<?php echo lang("color_id"); ?>', datafield: 'color_id', displayfield: 'color',  width: 150, columntype: 'combobox',
				createeditor: function (row, column, editor) {
                        	
                            editor.jqxComboBox({ 
                            	autoDropDownHeight: true, 
                            	source:colorsAdapter,
                            	displayMember: 'color', 
                            	valueMember: 'id'  
                            });


                        },



                        // update the editor's value before saving it.
                        cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
                            // return the old value, if the new value is empty.
                             if (newvalue !== oldvalue) {

                             	


                             }
                        
                        }
                    },
                    { 
                    	text: 'QTY',
                    	datafield: 'qty',
                    	width: 200,
                    	filterable: true,

                    	columntype: 'numberinput', 
                    	cellbeginedit: false,

                    	cellvaluechanging: function (row, datafield, columntype, oldvalue, newvalue) {

                    		if (newvalue != oldvalue) {

                    			//console.log(newvalue);
                    			var price = $("#jqxGridCustom_color_order").jqxGrid('getcellvalue', row, "price");

                    			var total_amount;
                    			total_amount =(price * newvalue);




                    			$("#jqxGridCustom_color_order").jqxGrid('setcellvalue', row, "total", (total_amount).toFixed(2));

                    			//cal_cash_calculate(newvalue,price);


                    		};

                    	}


                    },
                   { text: '<?php echo lang("price"); ?>',datafield: 'price',width: 200,editable:false, filterable: true,renderer: gridColumnsRenderer },

                    { text: '<?php echo lang("total"); ?>',datafield: 'total',width: 200,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});



$('#jqx_invoiceorder_Cancel').click(function(){
	// alert();
	$('#jqxGridOrderInvoice').jqxGrid('clear');
   	$('#jqxPopupWindowInvoice').jqxWindow('close');
   
});

$('#jqx_invoiceorder_save').click(function(){
	var row = JSON.stringify($('#jqxGridOrderInvoice').jqxGrid('getrows'));
	// var row =  $("#").jqxGrid('getrowdata');
	var status = $('input[name=status]:checked').val();
	var deposite = $('#deposite_invoice').val(); 
	var order_id = $('#order_id_invoice').val();
	$.ajax({
   	type: "POST",
   	url: '<?php echo site_url("admin/product_order_details/invoice_update"); ?>',
   	data: {status: status, deposite : deposite, order_id: order_id, row: row},
   	success: function (result) {
   		var result = eval('('+result+')');
   		if (result.success) {
	        $('#jqxGridOrderInvoice').jqxGrid('clear');
   			// reset_form_product_order_details();
   			// $('#jqxGridProduct_order_detail').jqxGrid('updatebounddata');
   			$('#jqxPopupWindowInvoice').jqxWindow('close');
   		}
   		$('#jqxPopupWindowProduct_order_detail').unblock();
   	}
   });
	// console.log(order_id);
});

//initialize new popup for invoice
$("#jqxPopupWindowInvoice").jqxWindow({ 
	theme: theme,
	width: '75%',
	maxWidth: '75%',
	height: '75%',  
	maxHeight: '75%',  
	isModal: true, 
	autoOpen: false,
	modalOpacity: 0.7,
	showCollapseButton: false 
});


function send_confirm(){
	alert(id);

	$.post("<?php echo site_url('admin/product_order_details/confirmOrder')?>",{id:id},function(data){
		$('#jqxGridProduct_order_detail').jqxGrid('updatebounddata');
   			$('#jqxPopupWindowProduct_order_detail').jqxWindow('close');

		},'json');
}
</script>

<script language="javascript" type="text/javascript">
 
 var colorsAdapter;
$(function(){

	var custom_color_ordersDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			
			{ name: 'order_id', type: 'number' },
			{ name: 'color_id', type: 'number' },
			{ name: 'style_id', type: 'number' },
			{ name: 'style_name', type: 'string' },
			{ name: 'size', type: 'string' },
			{ name: 'color', type: 'string' },
			{ name: 'product_weight_id', type: 'number' },
			{ name: 'qty', type: 'number' },
			{ name: 'price', type: 'string' },
			{ name: 'total', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/custom_color_orders/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	custom_color_ordersDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridCustom_color_order").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridCustom_color_order").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridCustom_color_order").jqxGrid({
		theme: theme,
		width: '100%',
		height: '300px',
		source: custom_color_ordersDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		editable: true,

		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridCustom_color_orderToolbar').html());
			toolbar.append(container);
		},
		columns: [
		
			{
				text: 'Style', datafield: 'style_id', displayfield: 'style_name',  width: 150, columntype: 'combobox',
				createeditor: function (row, column, editor) {
                        	
                            editor.jqxComboBox({ 
                            	autoDropDownHeight: true, 
                            	source:styleAdapter,
                            	displayMember: 'style_name', 
                            	valueMember: 'id'  
                            });


                        },



                        // update the editor's value before saving it.
                        cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
                            // return the old value, if the new value is empty.
                             if (newvalue !== oldvalue) {

                             	 $("#jqxGridCustom_color_order").jqxGrid('setcellvalue', row, "size", "Select a size..");


                             }
                        
                        }
                    },
                    {
                    	text: 'Size', datafield: 'product_weight_id',displayfield: 'size', width: 150, columntype: 'combobox',
     

                    	initeditor: function (row, cellvalue, editor, celltext, cellwidth, cellheight) {
	                           var id = $("#jqxGridCustom_color_order").jqxGrid('getcellvalue',row,"style_id");
	                            var size = editor.val();
	                            var sizes = new Array();
			                           	$.post("<?php echo site_url('admin/product_orders/getstyles')?>",{id:id},function(dataAdapter){
			                            		
			                          			
			        					sizes = dataAdapter;
			        					   editor.jqxComboBox({ autoDropDownHeight: true, source: sizes,displayMember: 'size',valueMember: 'id' });
			        					

											 },'json');
                           
                         

	                            // if (size != "Select a size...") {
	                            //     var index = sizes.indexOf(size);
	                            //     editor.jqxComboBox('selectIndex', index);
	                            // }

                        },
		                        cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
		                           
				                            if (newvalue !== oldvalue) {

				                            	
				                            	//console.log(newvalue.value);
				                            	var id = newvalue.value;
				                            	//alert(id);
				                            	
										$.post("<?php echo site_url('admin/product_order_details/getPrices')?>",{id:id},function(currency_data){

											
										var currency_data = currency_data.price;
										$("#jqxGridCustom_color_order").jqxGrid('setcellvalue', row, "price", (currency_data));
											},'json');

				                            } 
		                            	
		                        }
                    },

			

				{
				text: '<?php echo lang("color_id"); ?>', datafield: 'color_id', displayfield: 'color',  width: 150, columntype: 'combobox',
				createeditor: function (row, column, editor) {
                        	
                            editor.jqxComboBox({ 
                            	autoDropDownHeight: true, 
                            	source:colorsAdapter,
                            	displayMember: 'color', 
                            	valueMember: 'id'  
                            });


                        },



                        // update the editor's value before saving it.
                        cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
                            // return the old value, if the new value is empty.
                             if (newvalue !== oldvalue) {

                             	


                             }
                        
                        }
                    },
                    { 
                    	text: 'QTY',
                    	datafield: 'qty',
                    	width: 200,
                    	filterable: true,

                    	columntype: 'numberinput', 
                    	cellbeginedit: false,

                    	cellvaluechanging: function (row, datafield, columntype, oldvalue, newvalue) {

                    		if (newvalue != oldvalue) {

                    			//console.log(newvalue);
                    			var price = $("#jqxGridCustom_color_order").jqxGrid('getcellvalue', row, "price");

                    			var total_amount;
                    			total_amount =(price * newvalue);




                    			$("#jqxGridCustom_color_order").jqxGrid('setcellvalue', row, "total", (total_amount).toFixed(2));

                    			//cal_cash_calculate(newvalue,price);


                    		};

                    	}


                    },
                   { text: '<?php echo lang("price"); ?>',datafield: 'price',width: 200,editable:false, filterable: true,renderer: gridColumnsRenderer },

                    { text: '<?php echo lang("total"); ?>',datafield: 'total',width: 200,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridCustom_color_order").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridCustom_color_orderFilterClear', function () { 
		$('#jqxGridCustom_color_order').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridCustom_color_orderInsert', function () { 
		openPopupWindow('jqxPopupWindowCustom_color_order', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });


   	$('#jqxcustomadd').click(function(){
		// alert();
		$('#jqxGridCustom_color_order').jqxGrid('addrow', null, {});

	});

    var colorscount=
	{
		datatype:"json",
		datafields:[
		{ name: 'id',type: 'number'},
		{ name: 'color',type: 'string'},
		],
		url:'<?php echo site_url('admin/colors/get_Color')?>'

	}
	colorsAdapter = new $.jqx.dataAdapter(colorscount);



});


</script>


<script>


 $(document).ready(function () {
    $('#checkbox1').change(function () {
      $('#customselect').fadeToggle();
    });
});
</script>


<script language="javascript" type="text/javascript">

$(function(){

	
	$("#jqxGridCustomColor").jqxGrid({
		theme: theme,
		width: '100%',
		height: '300px',
		
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridCustomColorToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			
			
			{ text: 'Style Name',datafield: 'style_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: 'Size',datafield: 'size',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("color_id"); ?>',datafield: 'color',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("price"); ?>',datafield: 'price',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("total"); ?>',datafield: 'total',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridCustomColor").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridCustomColorFilterClear', function () { 
		$('#jqxGridCustomColor').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridCustomColorInsert', function () { 
		openPopupWindow('jqxPopupWindowCustomColor', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	


    
});




</script>



