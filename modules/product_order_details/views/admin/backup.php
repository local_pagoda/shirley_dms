<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('product_order_details'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('product_order_details'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridProduct_order_detailToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridProduct_order_detailInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridProduct_order_detailFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridProduct_order_detail"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowProduct_order_detail">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-product_order_details', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "product_order_details_id"/>
           
				<input type="hidden" id="wholesaler_id" name="wholesaler_id" value="<?php echo $this->session->userdata('w_id')?>">
			
			<div class="row">
				<div class="col-sm-6">
					 <div class="form-group">
					 	<div class="row">
							<div class="col-sm-3"> <label for="season_id"><?php echo lang('season_id')?></label></div>
							<div class="col-sm-4">  <div id='season_id' class='number_general' name='season_id'></div></div>
						</div>
			   

			   
			  </div>
				</div>

			
			</div>


              
               
          <!-- </table> -->
        <!--    <input id="jqxGridProduct_orderInsert" type="button" value="Add New" /> -->
           <div class="col-xs-12 connectedSortable" style="margin:20px">
				<?php echo displayStatus(); ?>
				<div style="margin-left: 10px; float: left;">
		            </div>
				<div id='jqxGridProduct_orderToolbar' class='grid-toolbar'>
					<p>Product Order</p>
				</div>
					<div id="jqxGridProduct_order"></div>
					
				
				
			</div><!-- /.col -->
			  <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxProduct_order_detailSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxProduct_order_detailCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </div>
    </div>
        <?php echo form_close(); ?>
         
    </div>
</div>
<div id="jqxPopupWindowProduct_order">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-product_orders', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "product_orders_id"/>
            <table class="form-table">
				
			<tr>
					<td><label for="style_id">Style</label></td>
					<td><div id="style_id" class="number_general" name="style_id"></div></div></td>
				</tr>

				<tr>
					<td><label for="product_weight_id">Size</label></td>
					<td><div id="product_weight_id" class="number_general" name="product_weight_id"></div></div></td>
				</tr>
				<tr>
					<td><label for='color_group_id'><?php echo lang('color_group_id')?></label></td>
					<td><input id='color_group_id' class='text_input' name='color_group_id'></td>
				</tr>
				
				 <tr>
					<td><label for='qty'><?php echo lang('qty')?></label></td>
					<td><div id='qty' class='number_general' name='qty'></div></td>
				</tr>
				<tr>
					<td><label for='price'><?php echo lang('price')?></label></td>
					<td><input id='price' class='text_input' name='price'></td>
				</tr>
				<tr>
					<td><label for='total'><?php echo lang('total')?></label></td>
					<td><input id='total' class='text_input' name='total'></td>
				</tr> 
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxProduct_orderSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxProduct_orderCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>
<script language="javascript" type="text/javascript">
var yearAdapter;
var  colorgroupAdapter;
var seasonAdapter;
 var wholesalerAdapter;
 var styleAdapter;
var sizeAdapter;
$(function(){

	var product_order_detailsDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			// { name: 'created_by', type: 'number' },
			// { name: 'updated_by', type: 'number' },
			// { name: 'deleted_by', type: 'number' },
			// { name: 'created_at', type: 'date' },
			// { name: 'updated_at', type: 'date' },
			// { name: 'deleted_at', type: 'date' },
		
			 { name: 'season', type: 'string' },
			{ name: 'date_of_order', type: 'string' },
			{ name: 'year', type: 'string' },
			
			
        ],
		url: '<?php echo site_url("admin/product_order_details/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	product_order_detailsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridProduct_order_detail").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridProduct_order_detail").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridProduct_order_detail").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: product_order_detailsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridProduct_order_detailToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editProduct_order_detailRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
					{ text: '<?php echo lang("season_id"); ?>',datafield: 'season',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("date_of_order"); ?>',datafield: 'date_of_order',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("year_id"); ?>',datafield: 'year',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridProduct_order_detail").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridProduct_order_detailFilterClear', function () { 
		$('#jqxGridProduct_order_detail').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridProduct_order_detailInsert', function () { 
		openPopupWindow('jqxPopupWindowProduct_order_detail', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowProduct_order_detail").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowProduct_order_detail").on('close', function () {
        reset_form_product_order_details();
    });

    $("#jqxProduct_order_detailCancelButton").on('click', function () {
        reset_form_product_order_details();
        $('#jqxPopupWindowProduct_order_detail').jqxWindow('close');
    });

    /*$('#form-product_order_details').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#w_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#w_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#date_of_order', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#date_of_order').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#season_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#season_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#year_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#year_id').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxProduct_order_detailSubmitButton").on('click', function () {
        saveProduct_order_detailRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveProduct_order_detailRecord();
                }
            };
        $('#form-product_order_details').jqxValidator('validate', validationResult);
        */
    });
  //    var yearcount=
  //   {
  //   	datatype:"json",
  //   	datafields:[
  //   	{ name: 'id',type: 'string'},
  //        { name: 'year',type: 'string'},
  //   	],
  //   	url:'<?php echo site_url('admin/years/get_years')?>'

  //   }
  //   yearAdapter = new $.jqx.dataAdapter(yearcount);

		//  $("#year_id").jqxComboBox({
		// 	theme: theme,
		// 	width: 195,
		// 	height: 25,
		// 	selectionMode: 'dropDownList',
		// 	autoComplete: true,
		// 	searchMode: 'containsignorecase',
		// 	source: yearAdapter,
		// 	displayMember: "year",
		// 	valueMember: "id",
		// });

     var seasoncount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'season',type: 'string'},
    	],
    	url:'<?php echo site_url('admin/seasons/getseason')?>'

    }
    seasonAdapter = new $.jqx.dataAdapter(seasoncount);

		 $("#season_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: seasonAdapter,
			displayMember: "season",
			valueMember: "id",
		});


 var colorgroupcount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'product_group',type: 'string'},
    	],
    	url:'<?php echo site_url('admin/product_colors/getcolors')?>'

    }
    colorgroupAdapter = new $.jqx.dataAdapter(colorgroupcount);

		 $("#color_group_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: seasonAdapter,
			displayMember: "product_group",
			valueMember: "id",
		});
		 // var wholesalercount=
   //  {
   //      datatype:"json",
   //      datafields:[
   //      { name: 'id',type: 'string'},
   //       { name: 'company_name',type: 'string'},
   //      ],
   //      url:'<?php echo site_url('admin/wholesaler_profiles/get_wholesaler')?>'

   //  }
   //  wholesalerAdapter = new $.jqx.dataAdapter(wholesalercount);

   //       $("#w_id").jqxComboBox({
   //          theme: theme,
   //          width: 195,
   //          height: 25,
   //          selectionMode: 'dropDownList',
   //          autoComplete: true,
   //          searchMode: 'containsignorecase',
   //          source: wholesalerAdapter,
   //          displayMember: "company_name",
   //          valueMember: "id",
   //      });
          var stylecount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'style_name',type: 'string'},
    	],
    	url:'<?php echo site_url('admin/styles/get_styles')?>'

    }
    styleAdapter = new $.jqx.dataAdapter(stylecount);

		 $("#style_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: styleAdapter,
			displayMember: "style_name",
			valueMember: "id",
		});
		 	 	  var sizecount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'size',type: 'string'},
         { name: 'style_id',type: 'string'},
         
    	],
    	url:'<?php echo site_url('admin/product_prices/getstyles')?>'

    }
    sizeAdapter = new $.jqx.dataAdapter(sizecount);

		 $("#product_weight_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: sizeAdapter,
			displayMember: "size",
			valueMember: "id",
		});
$("#style_id").bind('select', function(event)
		{
			if (event.args)
			{
				$("#product_weight_id").jqxComboBox({ disabled: false, selectedIndex: -1});		
				var value = event.args.item.value;
				sizecount.data = {style_id: value};
				sizeAdapter = new $.jqx.dataAdapter(sizecount, {
				    beforeLoadComplete: function (records) {
				        var filteredRecords = new Array();
				        for (var i = 0; i < records.length; i++) {
				            if (records[i].style_id == value)
				                filteredRecords.push(records[i]);
				        }
				        return filteredRecords;
				    }
				});
				$("#product_weight_id").jqxComboBox({ source: sizeAdapter, autoDropDownHeight: sizeAdapter.records.length > 10 ? false : true});

			}
		});
});

function editProduct_order_detailRecord(index){
    var row =  $("#jqxGridProduct_order_detail").jqxGrid('getrowdata', index);
   var id =row.id;
 
  	if (row) {
  		$('#product_order_details_id').val(row.id);
		$('#date_of_order').val(row.date_of_order);
		$('#season_id').val(row.season);
		$.post("<?php echo site_url('admin/product_orders/getProductorders')?>",{id:id},function(data){
			
			
				console.log(data);
		$.each(data,function(key,val){
			console.log(val);
				
		var id 					= val.id;
		var order_id 				=	val.order_id;
		var style_name 				=	val.style_name;
		var product_weight_id 		=	val.product_weight_id;
		var product_group 				=	val.product_group;
		var qty 				=	val.qty;
		var price 				=	val.price;
		var total 			=	val.total;
	
            var dataadd = {

           'id' 		                :id,
		 'order_id' 			        :order_id,
		 'style_name' 			        :style_name,
		 'product_weight_id' 			:product_weight_id,
           'product_group'			     :product_group,
			 'qty' 				        :qty,
			 'price' 				    :price,
			 'total' 				    :total,
	          	
            };
                 
     $('#jqxGridProduct_order').jqxGrid('addrow', null, dataadd);
				
			 cal_cash_calculate();
				});
			
		
		},'json');

		
        openPopupWindow('jqxPopupWindowProduct_order_detail', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveProduct_order_detailRecord(){
	var product_order =JSON.stringify($("#jqxGridProduct_order").jqxGrid('getrows'));
	var data = $("#form-product_order_details").serialize() + '&product_order=' + product_order;
   // var data = $("#form-product_order_details").serialize();
	console.log(data);
	$('#jqxPopupWindowProduct_order_detail').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/product_order_details/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_product_order_details();
                $('#jqxGridProduct_order_detail').jqxGrid('updatebounddata');
                $('#jqxPopupWindowProduct_order_detail').jqxWindow('close');
            }
            $('#jqxPopupWindowProduct_order_detail').unblock();
        }
    });
}

function reset_form_product_order_details(){
	$('#product_order_details_id').val('');
    $('#form-product_order_details')[0].reset();
}
 $("#qty").change(function(){
 	var qty = ($.isNumeric($("#qty").val()))?$("#qty").val():0;
    	var price = ($.isNumeric($("#price").val()))?$("#price").val():0;
 	var total_amount= qty*price;
 $('#total').val(total_amount);
});

$("#price").change(function(){
 	var qty = ($.isNumeric($("#qty").val()))?$("#qty").val():0;
    	var price = ($.isNumeric($("#price").val()))?$("#price").val():0;
 	var amount= qty*price;
 $('#total').val(amount);
});
</script>

<script language="javascript" type="text/javascript">

$(function(){


	var product_ordersDataSource =
	{
		datatype: "array",
		datafields: [
			{ name: 'id', type: 'number' },
			
			
			{ name: 'style_name', type: 'string' },
			//{ name: 'style_id', type: 'number' },
			{ name: 'size', type: 'string' },
			{ name: 'color_group_id', type: 'string' },
			{ name: 'qty', type: 'string' },
			
			
			{ name: 'price', type: 'string' },
			{ name: 'total', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/product_orders/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	product_ordersDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridProduct_order").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridProduct_order").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridProduct_order").jqxGrid({
		theme: theme,
		width: '80%',
		height: '300px',
		source: product_ordersDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		editable:true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		 selectionmode: 'multiplecellsadvanced',
  columnsresize: true,
  showeverpresentrow: true,
        //everpresentrowposition: "top",
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridProduct_orderToolbar').html());
			toolbar.append(container);
		},
		columns: [
			// { text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			// {
			// 	text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			// 	cellsrenderer: function (index) {
			// 		var e = '<a href="javascript:void(0)" onclick="editProduct_orderRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
			// 		return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
			// 	}
			// },
			
		
			//{ text: 'Style',datafield: 'style_name',width: 150,editable:true,renderer: gridColumnsRenderer },

			{
                        text: 'Style', datafield: 'style_name', width: 150, columntype: 'combobox',
                        createeditor: function (row, column, editor) {
                        	// var style_id =$('#jqxGridProduct_order').jqxGrid('getcellvalue', row,"style_id");
                        	// var style = eidtor.val();
                            // assign a new data source to the combobox.
                            console.log(styleAdapter);
                            // var list = ['Bubble', 'Pompom', 'sweater'];
                            editor.jqxComboBox({ autoDropDownHeight: true, source:styleAdapter,displayMember: 'style_name', valueMember: 'id'  });
                        },



                        // update the editor's value before saving it.
                        cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
                            // return the old value, if the new value is empty.
                            if (newvalue == "") return oldvalue;
                        }
                    },
			{
                        text: 'Size', datafield: 'product_weight_id', width: 150, columntype: 'combobox',
                        createeditor: function (row, column, editor) {
                           
                            editor.jqxComboBox({ autoDropDownHeight: true, source:sizeAdapter ,displayMember: 'size', valueMember: 'id' });
                        },

                        

                        // update the editor's value before saving it.
                        cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
                            // return the old value, if the new value is empty.
                            if (newvalue == "") return oldvalue;
                        }
                    },
			//{ text: '<?php echo lang("color_group_id"); ?>',datafield: 'color_group_id',width: 150,filterable: true,renderer: gridColumnsRenderer },

			{
                        text: '<?php echo lang("color_group_id"); ?>', datafield: 'product_group', width: 200, columntype: 'combobox',
                        createeditor: function (row, column, editor) {
                           
                            editor.jqxComboBox({ autoDropDownHeight: true, source:colorgroupAdapter ,displayMember: 'product_group', valueMember: 'id' });
                        },

                        

                        // update the editor's value before saving it.
                        cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
                            // return the old value, if the new value is empty.
                            if (newvalue == "") return oldvalue;
                        }
                    },
			{ 
				text: 'QTY',
				datafield: 'qty',
				width: 200,
				filterable: true,
			
				columntype: 'numberinput', 
				cellbeginedit: false,
                
				cellvaluechanging: function (row, datafield, columntype, oldvalue, newvalue) {
					
                  if (newvalue != oldvalue) {
                    	
                    	console.log(newvalue);
                        var price = $("#jqxGridProduct_order").jqxGrid('getcellvalue', row, "price");
					
					var total_amount;
                       total_amount =(price * newvalue);

                    

                       
                          $("#jqxGridProduct_order").jqxGrid('setcellvalue', row, "total", (total_amount).toFixed(2));

                          cal_cash_calculate(newvalue,price);

                      
                    };
                    
                }


			},
			 { 
				text: 'Price',
				datafield: 'price',
				width: 200,
				filterable: true,
			
				columntype: 'numberinput', 
				cellbeginedit: false,
                
				cellvaluechanging: function (row, datafield, columntype, oldvalue, newvalue) {
					
                  if (newvalue != oldvalue) {
                    	
                        var quantity = $("#jqxGridProduct_order").jqxGrid('getcellvalue', row, "qty");
					
					var total_amount;
                       total_amount =(newvalue * quantity);

                    

                       
                          $("#jqxGridProduct_order").jqxGrid('setcellvalue', row, "total", (total_amount).toFixed(2));

                          cal_cash_calculate(newvalue,quantity);

                      
                    };
                    
                }

			},
			
			{ text: '<?php echo lang("total"); ?>',datafield: 'total',width: 200,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridProduct_order").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridProduct_orderFilterClear', function () { 
		$('#jqxGridProduct_order').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridProduct_orderInsert', function () { 
		openPopupWindow('jqxPopupWindowProduct_order', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowProduct_order").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowProduct_order").on('close', function () {
        reset_form_product_orders();
    });

    $("#jqxProduct_orderCancelButton").on('click', function () {
        reset_form_product_orders();
        $('#jqxPopupWindowProduct_order').jqxWindow('close');
    });

   

    $("#jqxProduct_orderSubmitButton").on('click', function () {
        saveProduct_orderRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveProduct_orderRecord();
                }
            };
        $('#form-product_orders').jqxValidator('validate', validationResult);
        */
    });
});


function cal_cash_calculate(price,quantity) {

	var total = (price*quantity);
	
	$('#total').val(total);

	

}
</script>