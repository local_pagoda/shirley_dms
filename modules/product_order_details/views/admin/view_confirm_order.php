

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('confirm_order_details'); ?></h1>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active"><?php echo lang('confirm_order_details'); ?></li>
		</ol>

	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				
				<div id="jqxGridProduct_order_detail"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div id="jqxPopupWindowInvoice">
	<div class="col-xs-12 connectedSortable" style="margin:20px">

		<div class="row">
			<div class="col-md-4">
				<p>Season : </p><div id="season_invoice"></div>
			</div>   
			<div class="col-md-4">								 	
				<p>Order Date : </p><div id="date_of_order_invoice"></div>
			</div>
			<div class="col-md-4">								   	
				<p>Year : </p><div id="year_invoice"></div> 
			</div>
			    	
		</div>
		<br>
		<div id="jqxGridOrderInvoice"></div>	
		<input type="hidden" id="order_id_invoice">
		<?php if($logingrop == 2){ ?>
		<input type="radio" name="status" value="Pending" id="pending">Pending
		<input type="radio" name="status" value="Editing" id="editing">Editing
		<input type="radio" name="status" value="Sent" id="sent">Sent
		<div class="pull-right">
		<button id="jqx_invoiceorderClose" class="btn-danger">Cancel</button>
		<button id="jqx_invoiceorder_save" class="btn-success">Save</button>
		</div>
		<?php } ?>
    </div><!-- /.col -->
</div>

<div id="jqxPopupWindowPayment">
	<div class="col-xs-12 connectedSortable" style="margin:50px">
		<div class="row">
			<div class="col-md-6">								   	
				<p>Deposite Date : </p><div id="dateTimeInput"></div> 
			</div>
			<div class="col-md-6">
				<p>Deposite Received: </p><input type="text" id="deposite_received_invoice" class="text_input" style="width: 100%">
			</div> 
			<br><br>  
			<div class="container">
				<div class="row">
				    <table class="table table-striped">
				    	<thead>
				    		
				    		<th>Product Size</th>
				    		<th>Product Style Name</th>
				    		<th>Product Quantity</th>
				    		<th>Product Price</th>
				    		<th>Product Sub Total</th>


				    	</thead>
				    	<tbody id="payment_detail_table">
				    		
				    	</tbody>
				    	<tfoot>
				    		<tr>
				    		    <td></td>
				    			<td colspan="3"><strong>Grand Total</strong></td>
				    			<td id="grand_total"></td>
				    		</tr>
				    	</tfoot>
				    </table> 
				    <div>
				        <input type="hidden" id="order_detail_payment_id">
				    	<button id="save_payment_informtaion" class="btn btn-success">Save</button>
				    </div>
				</div> 
			</div>	
		</div>
		
    </div><!-- /.col -->
</div>



<script language="javascript" type="text/javascript">
	var group = '<?php echo $logingrop; ?>';
    
	$(function(){

		var product_order_detailsDataSource =
		{
			datatype: "json",
			datafields: [
			{ name: 'id', type: 'number' },

			{ name: 'created_at', type: 'string' },

			{ name: 'product_code', type: 'string' },
			{ name: 'style_no', type: 'string' },
			{ name: 'style_name', type: 'string' },
			{ name: 'size', type: 'string' },
			{ name: 'product_group', type: 'string' },
			{ name: 'season', type: 'string' },
			{ name: 'year', type: 'string' },
			{ name: 'deposite', type: 'string' },
			{ name: 'deposite_received', type: 'string' },
			{ name: 'order_status', type: 'string'},
			{ name: 'qty', type: 'number' },
			{ name: 'price', type: 'string' },
			{ name: 'total', type: 'string' },
			{ name: 'order_status', type: 'string'},


			
			
			],

			
			url: '<?php echo site_url("admin/product_order_details/confirm_detail_json/.$id"); ?>',
			pagesize: defaultPageSize,
			root: 'rows',
			id : 'id',
			cache: true,
			pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	product_order_detailsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridProduct_order_detail").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridProduct_order_detail").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridProduct_order_detail").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: product_order_detailsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridProduct_order_detailToolbar').html());
			toolbar.append(container);
		},
		columns: [
		{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
		{
			text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
			cellsrenderer: function (index, row, columnfield, value, defaulthtml, columnproperties) {
		        var check =  $("#jqxGridProduct_order_detail").jqxGrid('getrowdata', index);

                if(group == 105 && check.order_status == 'Sent'){
                	var e = '<a href="javascript:void(0)" onclick="show_InvoiceProductOrder(' + index + '); return false;" title="Final Invoice"><i class="fa fa-files-o"></i></a>&nbsp;';
                }
				else if(group == 2){
					var e = '<a href="javascript:void(0)" onclick="show_InvoiceProductOrder(' + index + '); return false;" title="Final Invoice"><i class="fa fa-files-o"></i></a>&nbsp;';
					e += '<a href="javascript:void(0)" onclick="show_paymentProductOrder(' + index + '); return false;" title="Payment"><i class="fa fa-money"></i></a>&nbsp;';

				}
				else{
					var e = '<a href="javascript:void(0)"  title="No Action"><i class="fa fa-times"></i></a>&nbsp;';
					
				}
				return '<div style="text-align: center; margin-top: 8px;">'+ e +'</div>';
			}

		},

		{ text: '<?php echo lang("date_of_order"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: 'Product Code',datafield: 'product_code',width: 150,filterable: true,renderer: gridColumnsRenderer },
		
		{ text: 'Style No',datafield: 'style_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: 'Style Name',datafield: 'style_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: 'Size',datafield: 'size',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: 'Color Group',datafield: 'product_group',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("season_id"); ?>',datafield: 'season',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("qty"); ?>',datafield: 'qty',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("price"); ?>',datafield: 'price',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: '<?php echo lang("total"); ?>',datafield: 'total',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: 'Deposite',datafield: 'deposite',width: 150,filterable: true,renderer: gridColumnsRenderer },
		{ text: 'Final Invoice Status',datafield: 'order_status',width: 150,filterable: true,renderer: gridColumnsRenderer },		
		


		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
		e.preventDefault();
		setTimeout(function() {$("#jqxGridProduct_order_detail").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridProduct_order_detailFilterClear', function () { 
		$('#jqxGridProduct_order_detail').jqxGrid('clearfilters');
	});

	

	

	
});
$("#jqxGridOrderInvoice").jqxGrid({
	theme: theme,
	width: '120%',
	height: gridHeight,
	altrows: true,
	pageable: true,
	sortable: true,
	rowsheight: 30,
	columnsheight:30,
	showfilterrow: true,
	filterable: true,
	columnsresize: true,
	autoshowfiltericon: true,
	columnsreorder: true,
	selectionmode: 'none',
	virtualmode: true,
	enableanimations: false,
	editable: true,
	pagesizeoptions: pagesizeoptions,
	showtoolbar: true,
	rendertoolbar: function (toolbar) {
		var container = $("<div style='margin: 5px; height:50px'></div>");
		container.append($('#jqxGridOrderInvoiceToolbar').html());
		toolbar.append(container);
	},
	columns: [
	{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
	{ text: 'Product Weight Id', editable: false, datafield: 'product_weight_id',width: 100,filterable: true,renderer: gridColumnsRenderer },
	{ text: 'Product Order Id', editable: false, datafield: 'product_order_id',width: 100,filterable: true,renderer: gridColumnsRenderer },
	
	{ text: 'Product Code', editable: false, datafield: 'product_code',width: 100,filterable: true,renderer: gridColumnsRenderer },
	{ text: 'Style Name',datafield: 'style_name', editable: false, width: 100,filterable: true,renderer: gridColumnsRenderer },
	{ text: 'Size',datafield: 'size',width: 100, editable: false, filterable: true,renderer: gridColumnsRenderer },
	{ text: 'Color Groups',datafield: 'product_group',width: 100, editable: false, filterable: true,renderer: gridColumnsRenderer },
	{ 
		text: 'QTY',
		datafield: 'qty',
		width: 100,
		filterable: true,

		columntype: 'numberinput', 
		cellbeginedit: false,

		cellvaluechanging: function (row, datafield, columntype, oldvalue, newvalue) {

			if (newvalue != oldvalue) {

				console.log(newvalue);
				var price = $("#jqxGridOrderInvoice").jqxGrid('getcellvalue', row, "price");

				var total_amount;
				total_amount =(price * newvalue);




				$("#jqxGridOrderInvoice").jqxGrid('setcellvalue', row, "total", (total_amount).toFixed(2));

				cal_cash_calculate(newvalue,price);


			};

		}


	},

	{ 
		text: 'Price',
		datafield: 'price',
		width: 100,
		filterable: true,

		columntype: 'numberinput', 
		cellbeginedit: false,

		cellvaluechanging: function (row, datafield, columntype, oldvalue, newvalue) {

			if (newvalue != oldvalue) {

				var quantity = $("#jqxGridOrderInvoice").jqxGrid('getcellvalue', row, "qty");

				var total_amount;
				total_amount =(newvalue * quantity);




				$("#jqxGridOrderInvoice").jqxGrid('setcellvalue', row, "total", (total_amount).toFixed(2));

				cal_cash_calculate(newvalue,quantity);


			};

		}

	},

	{ text: '<?php echo lang("total"); ?>',datafield: 'total',width: 100,filterable: true,renderer: gridColumnsRenderer },
	{ text: 'Cancellation', dataField: 'cancel_order', columntype: 'checkbox', width: 100, editable: true, resizable: false},
	],
	rendergridrows: function (result) {
		return result.data;
	}
});


function cal_cash_calculate(price,quantity) {


	var total = (price*quantity);
	
	$('#total').val(total);

	

}
$(document).ready(function () {
   $("#dateTimeInput").jqxDateTimeInput({ formatString: 'yyyy-MM-dd', width: '350px', height: '25px' });
});
function show_InvoiceProductOrder(index)
{
	var dataInvoiceOrder;
	var row =  $("#jqxGridProduct_order_detail").jqxGrid('getrowdata', index);
	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/product_order_details/get_final_invoice_json"); ?>',
		data: {id : row.id},
		datatype: 'json',
		success: function (result) {
			result = eval('('+result+')')
			
			$('#season_invoice').html(row.season);
			$('#date_of_order_invoice').html(row.created_at);
			$('#year_invoice').html(row.year);
			$('#order_id_invoice').val(row.id);
	
			$.each(result.rows,function(key,val){
				dataInvoiceOrder = {
           			// 'season'            : row.season,
           			// 'date_of_order'		: row.date_of_order,
           			// 'year'				: row.year,
           			'product_order_id'  : val.product_order_id,  
           			'cancel_order'      : val.product_cancellation_status,
           			'product_code'		: val.product_code,
           			'size'				: val.size,
           			'qty'				: val.qty,
           			'style_name'		: val.style_name,
           			'product_group'		: val.product_group,
           			'price'				: val.price,
           			'total'				: val.total,
           			'product_weight_id' : val.product_weight_id,
           		}
           // console.log(dataInvoiceOrder);
           $('#jqxGridOrderInvoice').jqxGrid('addrow',null,dataInvoiceOrder);

      
			if(val.order_status == 'Pending')
			{ 
				$('#pending').attr('checked', true);
			}
			else if(val.order_status == 'Editing')
			{
				$('#editing').attr('checked', true);
			}
			else{
				$('#sent').attr('checked', true);
			}
		 });	
            // $('#jqxPopupWindowProduct_order_detail').unblock();
        }
    });
	// alert(row);
	openPopupWindow('jqxPopupWindowInvoice', 'Upfront <?php echo $header ?> Invoice ');

}

function show_paymentProductOrder(index)
{
	var row =  $("#jqxGridProduct_order_detail").jqxGrid('getrowdata', index);
	var total = 0;
	$('#payment_detail_table').empty();
	$('#order_detail_payment_id').val(row.id);
	// console.log(row.id);
	$('#deposite_received_invoice').val(row.deposite_received);
	$.ajax({
		type: "POST",
		url: '<?php echo site_url("admin/product_order_details/get_payment_data_json"); ?>',
		data: {id : row.id},
		datatype: 'json',
		success: function (result) {
			result = eval('('+result+')')
			$.each(result.rows,function(key,val){
                $('#payment_detail_table').append('<tr><td>'+val.size+'</td><td>'+val.style_name+'</td><td>'+val.qty+'</td><td>'+val.price+'</td><td>'+val.total+'</td></tr>');
                total = total + parseInt(val.total);
			});
			$('#grand_total').html(total);
        }
    });
	openPopupWindow('jqxPopupWindowPayment', 'Upfront <?php echo $header ?> Invoice ');
     
}

$('#jqx_invoiceorderClose').click(function(){
   	$("#jqxGridOrderInvoice").jqxGrid('clear');
   	$('#jqxPopupWindowInvoice').jqxWindow('close');

});
//initialize new popup for invoice
$("#jqxPopupWindowInvoice").jqxWindow({ 
	theme: theme,
	width: '75%',
	maxWidth: '75%',
	height: '75%',  
	maxHeight: '75%',  
	isModal: true, 
	autoOpen: false,
	modalOpacity: 0.7,
	// showCollapseButton: false 
});

//initialize new popup for invoice
$("#jqxPopupWindowPayment").jqxWindow({ 
	theme: theme,
	width: '75%',
	maxWidth: '75%',
	height: '75%',  
	maxHeight: '75%',  
	isModal: true, 
	autoOpen: false,
	modalOpacity: 0.7,
	// showCollapseButton: false 
});

$('#jqx_invoiceorder_save').click(function(){
	var status = $('input[name=status]:checked').val();
	// var deposite = $('#deposite_received_invoice').val(); 
	var order_id = $('#order_id_invoice').val();
	var row =  JSON.stringify($('#jqxGridOrderInvoice').jqxGrid('getrows'));
	console.log(row);
	$.ajax({
   	type: "POST",
   	url: '<?php echo site_url("admin/product_order_details/final_invoice_update"); ?>',
   	data: {status: status, order_id: order_id, row: row},
   	success: function (result) {
   		var result = eval('('+result+')');
   		if (result.success) {
   			$("#jqxGridOrderInvoice").jqxGrid('clear');
   			// reset_form_product_order_details();
   			// $('#jqxGridProduct_order_detail').jqxGrid('updatebounddata');
   			$('#jqxPopupWindowInvoice').jqxWindow('close');
   		}
   		$('#jqxPopupWindowProduct_order_detail').unblock();
   	}
   });
});
$('#save_payment_informtaion').click(function(){
	var deposite_date = $('#dateTimeInput').val();	
	var deposite = $('#deposite_received_invoice').val(); 
    var order_id = $('#order_detail_payment_id').val();
	$.ajax({
   	type: "POST",
   	url: '<?php echo site_url("admin/product_order_details/save_payment_informtaion"); ?>',
   	data: {deposite_date: deposite_date, order_id: order_id, deposite: deposite},
   	success: function (result) {
   		var result = eval('('+result+')');
   		if (result.success) {
   			$('#jqxPopupWindowPayment').jqxWindow('close');
   		}
   		$('#jqxPopupWindowProduct_order_detail').unblock();
   	}
   });
});


</script>

