<style>

.ui-datepicker-calendar{
    background: #fff;
}
#number-pre-sec{
    position: relative;
}


#number-pre-sec:after{
    content: '+977+';
    position: absolute;
    height: 34px;
    width: 64px;
     background: #159051;
    left: 0;
    bottom: 0;
    padding: 7px 6px;
    color: #fff;
}
#number-pre-sec input{
        padding-left: 74px;
}

#number-pre-imediate{
   position: relative; 
}
#number-pre-imediate:after{
    content: '+977+';
    position: absolute;
    height: 34px;
    width: 64px;
     background: #159051;
    left: 0;
    bottom: 0;
    padding: 7px 6px;
    color: #fff;
}
#number-pre-imediate input{
    padding-left: 74px;
}
#number-pre{
    position: relative;
}



#number-pre:after{
    content: '+977+';
    position: absolute;
    height: 34px;
    width: 80px;
     background: #159051;
    left: 0;
    bottom: 0;
    padding: 7px 6px;
    color: #fff;
}

#number-pre input{
        padding-left: 90px;
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
  /*background: :#53bea1;*/
}
.styled-select.slate {
   /*background: url(http://i62.tinypic.com/2e3ybe1.jpg) no-repeat right center;*/
   height: 33px;
   width: 345px;
   border-color: #52b89c;
}

.select_avai{
    width: 100% !important;
}

.title-each-tab{
    margin: 20px 0;
}
</style>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="rates-page list-all-page">
    <section class="home-slider">
        <div class="bannerSlider">
            
              
                     <?php foreach($banners as $banner):?>
               <div class="cz_banner_item <?php echo $banner['design']?>" style="background-image: url('<?php echo base_url()?>uploads/banner/<?php echo $banner['background'] ?>')">
                <div class="image-container">
                    <img src="<?php echo base_url()?>uploads/banner/<?php echo $banner['image_name'] ?>">
                </div>
                
                <div class="text-container">
                    <div class="text-inner">
                        <div class="right-img">
                            <img src="<?php echo base_url()?>uploads/banner/<?php echo $banner['logo'] ?>">
                        </div>
                         <div class="right-text <?php echo $banner['color']?> bottom-<?php echo $banner['bottom-color']?> " >
                            <h1 ><?php echo $banner['top_box_description'] ?></h1>
                            <p><?php echo $banner['bottom_box_description'] ?></p>
                        </div>
                    </div>
                </div>
                
               
            
        </div>
            <?php endforeach;?>
        </div>
    </section>

    <section class="page-content-area">
        <div class="outer-custom-container">
            <div class="left-big-contianer">

                
                <div class="department">
            <div class="department-header">
                <div class="dept-logo">
                    <img src="<?php echo theme_url()?>assets/img/banner icon -73.png">
                </div>
                <div class="blog-title">
                    <h3 class="title-each-tab">  Application form  </h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="dept-tabs">
            
                <div class="container">
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
            <p>Personal Detail</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
            <p>Education</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
            <p>Work Experience</p>
        </div>

        <div class="stepwizard-step">
            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
            <p>Availability and Preferences</p>
        </div>
    </div>
</div>
  <?php if($this->session->userdata('msg')){ ?>
                     <div class="alert alert-danger"><?php echo $this->session->userdata('msg'); ?>
                      <?php  $this->session->unset_userdata('msg'); ?>
                     </div>
                        <?php } ?>
<?php echo form_open_multipart('personal_detail/save')?>
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3 class="title-each-tab"> Personal Details</h3>
                 
                 
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">First Name</label>
                                <input  maxlength="100" type="text" required="required" name="first_name" class="form-control" placeholder="Enter First Name"  />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"> Middle Name</label>
                                <input  maxlength="100" type="text"   name="midle_name" class="form-control" placeholder="Enter Middle Name"  />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Last  Name</label>
                                <input  maxlength="100" type="text" required="required" name="last_name" class="form-control" placeholder="Enter Last Name"  />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                    <label class="control-label">Permanent Address</label>
                                    <input  maxlength="100" type="text" required="required"  name="permanent_address" class="form-control" placeholder="Enter Permanent Address"  />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Zone</label>
                                <select class="styled-select slate" id="zone_name_section_form" name="p_zone" style="margin:2px;" required>
                                    <option value="">Select</option>
                                         <?php foreach ($zones as $zone):?>
                                                <option value="<?php echo $zone['zone']?>"><?php echo $zone['zone']?></option>
                                              <?php endforeach;?>
                                </select>
                       
                            </div>
                        </div>
                        <?php  $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash()  ); ?>
                        <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" id="csrf"  />
                        <div class="col-sm-4">
                             <div class="form-group">
                                <label class="control-label">District</label>
                                 
                                <select class="styled-select slate" name="p_district" id="district_name_section_form" style="margin:2px;">
                                              <option value="">Select</option>
                                             
                                                 <?php foreach ($districts as $district):?>
                                                <option value="<?php echo $district['district']?>"><?php echo $district['district']?></option>
                                              <?php endforeach;?> 
                                  </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Temporary Address</label>
                                <input  maxlength="100" type="text"  name="temporaray_address" class="form-control" placeholder="Enter Temporary Address"  />
                            </div>

                        </div>
                        <div class="col-sm-4">
                             <div class="form-group">
                                <label class="control-label">Zone</label>
                                 
                             <select class="styled-select slate" id="t_zone_change"  name="t_zone" style="margin:2px;">
                                               <option value="">Select</option>
                                         <?php foreach ($zones as $zone):?>
                                                <option value="<?php echo $zone['zone']?>"><?php echo $zone['zone']?></option>
                                              <?php endforeach;?>
                                             
                                                  
                                          </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                             <div class="form-group">
                                <label class="control-label">District</label>
                                 
                             <select class="styled-select slate" id="t_district_change"  name="t_district" style="margin:2px;">
                                              <option value="District">Select</option>
                                              <?php foreach ($districts as $district):?>
                                                <option value="<?php echo $district['district']?>"><?php echo $district['district']?></option>
                                              <?php endforeach;?> 
                                                  
                                          </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                    
                            <div class="form-group">
                                <label class="control-label">Date of Birth</label>
                   
                                    <select  class="styled-select slate" name="y" id="y" style="width:67px;">
                                        <option value="">Year</option>
                                        <?php for ($i = date('Y'); $i >= 1944; $i--) { ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="styled-select slate"  name="m" id="m" style="width:75px">
                                        <option value="">Month</option>
                                        <?php for ($i = 1; $i <= 12; $i++) { ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php } ?>
                                    </select>
                                    <select  class="styled-select slate" name="d" id="d" style="width:62px">
                                        <option value="">Day</option>
                                        <?php for ($i = 1; $i <= 32; $i++) { ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                        <?php } ?>
                                    </select>
                            </div>
                        </div>




                        <div class="col-sm-6">
                            <div class="form-group">
                                    <label class="control-label">Gender</label>
                                   <input type="radio" name="gender" value="Female" required>Female&nbsp;
                                   <input type="radio" name="gender" value="Male" >Male
                            </div>
                        </div>

                    </div>


                      <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Nationality</label>
                                    <input  maxlength="100" type="text" required="required" name="nationality" class="form-control" placeholder="Enter Nationality"  />
                                 </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group" style="margin:27px;">
                                    <label class="control-label">Marital Status</label>
                                   <input type="radio" name="marital_status" value="Female">Married&nbsp;
                                   <input type="radio" name="marital_status" value="Male" >Unmarried
                                 </div>
                            </div>

                        </div>

                   <!-- <div class="form-group " style="color: #53bea1;">
                                               <span id="fileselector">
                                                  <label  for="upload-file-selector">Upload CV
                                             <input type="file"  name="userfile" size="20" style="padding-top: 10px;" >

                                             </label>
                                              </span>
                                           </div> -->
                                           <div class="form-group " style="color: #53bea1;">
                                               <span id="fileselector">
                                                  <label  for="upload-file-selector">Upload CV
                                             <input type="file" onchange="check_extension()" id="uploaded_cv_file"  name="userfile" size="20" style="padding-top: 10px;" >

                                             </label>
                                              </span>
                                           </div>


                                         <!--     <div class="form-group"> 
        <label id="upload_image_name" style="display:none"></label>
          
                      <input name="cv" id="cv" class='text_input' style="display:none"/>
                      <input type="file" id="upload_image" name="userfile" style="display:block"/>
</div> -->

                                 <div class="form-group">
                    <label class="control-label">Citizenship No</label>
                    <input  maxlength="100"  required="required" name="citizenship_no" class="form-control" placeholder="Enter Citizenship No"   />
                </div>


                
             <div class="row">
                 <div class="col-sm-6">
                       <div class="form-group">
                            <label class="control-label">Langauge</label>
                            <input  maxlength="100" type="text" required="required" name="language" class="form-control" placeholder="Enter Langauge"  />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input  maxlength="100" type="text" required="required" pattern="[a-zA-Z0-9!#$%&amp;'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*" name="email"  class="form-control" placeholder="Enter Email"  />
                        </div>
                    </div>
                </div>
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group" id="number-pre">
                        <label class="control-label">Landline</label>
                        <input min="1"  maxlength="100" type="text"   name="phone_no" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" placeholder="Enter Landline"  required />
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group" id="number-pre-sec">
                        <label class="control-label">Mobile No</label>
                        <input min="1"  maxlength="100"  type="text" name="mobile_no" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" placeholder="Enter Mobile No"   required  />
                    </div>
                </div>
            </div>

              <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                    <label class="control-label">Immediate Contact Person</label>
                    <input  maxlength="100" type="text" required="required" name="immediate_contact" class="form-control" placeholder="Enter Immediate Contact Person"  />
                    </div>
                </div>
                <div class="col-sm-6">
                         <div class="form-group" id="number-pre-imediate">
                    <label class="control-label">Contact No</label>
                    <input  maxlength="100" type="text"  name="contact_no" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required="required" class="form-control" placeholder="Enter Contact No"  />
                    </div>
                </div>
            </div>
                 

                     <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
         

            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12"  id="couser_extra_values">
                <h3 class="title-each-tab"> Education </h3>
                <table>
                <th><label class="control-label">Academic Level</label></th>
                    <th><label class="control-label">Board/Univeristy</label></th>
                    <th><label class="control-label">School/College</label></th>
                    <th><label class="control-label">Degree/ Faculty</label></th>
                    <th><label class="control-label">Obtained % or GPA </label></th>
                    <th><label class="control-label">Division</label></th>
                    <th> <label class="control-label">Passed Year</label></th>
                   

                   
                    <tr>
                        <td><label class="control-label">SLC or Equivalent *</label></td>
                          
                           <td> <input maxlength="200" type="text" required="required" name="s_board" class="form-control" /></td>  
                           <td> <input maxlength="200" type="text" required="required"  name="s_school" class="form-control"   /></td>  
                            <td> <input maxlength="200" type="text"   name="s_degree" class="form-control"   /></td>  
                           <td> <input maxlength="200" type="number" step="any" required="required"  name="s_marks" class="form-control"   /></td>  
                          <td> <select class="styled-select slate" name="s_division"style="width:100px;" >
                                    <option value="Distinction">Distinction</option>
                                    <option value="First">First</option>
                                    <option value="Second">Second</option>
                                    <option value="Third">Third</option>
                                </select>
                          </td>  
                          <td> <input maxlength="200" type="text" required="required" name="s_passed_year"  class="form-control"  /></td>  
                        
                            
                        
                    </tr>
                    <tr>
                        <td><label class="control-label">10+2 or Equivalent *</label></td>
                          
                           <td> <input maxlength="200" type="text" required="required"  name="c_university"  class="form-control" /></td>  
                           <td> <input maxlength="200" type="text" required="required" name="c_college"  class="form-control"   /></td>  
                            <td> <input maxlength="200" type="text" name="c_degree"   class="form-control"   /></td>  
                           <td> <input maxlength="200" type="number" step="any" required="required" name="c_marks"  class="form-control"   /></td>  
                         <td> <select class="styled-select slate" style="width:100px;" name="c_division">
                                   <option value="Distinction">Distinction</option>
                                    <option value="First">First</option>
                                    <option value="Second">Second</option>
                                    <option value="Third">Third</option>
                                </select>
                          </td>  
                          <td> <input maxlength="200" type="text" required="required"  name="c_passed_year" class="form-control"  /></td>  
                        
                    </tr>
                    <tr>
                        <td><label class="control-label">Bachelors Degree <?php if($vacancydatas){ echo ($vacancydatas['bachelor'] == 1)?'*':''; } ?></label></td>
                          
                           <td> <input maxlength="200" type="text"   name="b_university"  class="form-control" <?php if($vacancydatas){ echo ($vacancydatas['bachelor'] == 1)?'required':''; } ?>/></td>  
                           <td> <input maxlength="200" type="text"   name=" b_college" class="form-control"  <?php if($vacancydatas){ echo ($vacancydatas['bachelor'] == 1)?'required':''; } ?> /></td>  
                            <td> <input maxlength="200" type="text" name="b_faculty"  class="form-control"   <?php if($vacancydatas){ echo ($vacancydatas['bachelor'] == 1)?'required':''; } ?>  /></td>  
                           <td> <input maxlength="200" type="number" step="any" name="b_marks"  class="form-control"  <?php if($vacancydatas){ echo ($vacancydatas['bachelor'] == 1)?'required':''; } ?> /></td>  
                         <td> <select class="styled-select slate" style="width:100px;" name="b_division"  >
                                    <option value="Distinction">Distinction</option>
                                    <option value="First">First</option>
                                    <option value="Second">Second</option>
                                    <option value="Third">Third</option>
                                </select>
                          </td>  
                          <td> <input maxlength="200" type="text" name="b_passed_year" class="form-control"  <?php if($vacancydatas){ echo ($vacancydatas['bachelor'] == 1)?'required':''; } ?>/></td>  
                        
                    </tr>
                    <tr>
                        <td><label class="control-label"> Master Degree  <?php if($vacancydatas){ echo ($vacancydatas['master'] == 1)?'*':''; } ?> </label></td>
                        <td> <input maxlength="200" type="text"   name="m_university" class="form-control" <?php if($vacancydatas){ echo ($vacancydatas['master'] == 1)?'required':''; } ?> /></td>  
                        <td> <input maxlength="200" type="text"  name="m_college"  class="form-control"  <?php if($vacancydatas){ echo ($vacancydatas['master'] == 1)?'required':''; } ?>/></td>  
                        <td> <input maxlength="200" type="text"   name="m_faculty" class="form-control"  <?php if($vacancydatas){ echo ($vacancydatas['master'] == 1)?'required':''; } ?>/></td>  
                        <td> <input maxlength="200" type="number"  step="any"  name="m_marks"  class="form-control" <?php if($vacancydatas){ echo ($vacancydatas['master'] == 1)?'required':''; } ?> /></td>  
                        <td> <select class="styled-select slate" style="width:100px;" name="m_division" >
                                   <option value="Distinction">Distinction</option>
                                    <option value="First">First</option>
                                    <option value="Second">Second</option>
                                    <option value="Third">Third</option>
                                </select>
                          </td>  
                          <td> <input maxlength="200" type="text"  name="m_passed_year"  class="form-control" <?php if($vacancydatas){ echo ($vacancydatas['master'] == 1)?'required':''; } ?>  /></td>  
                            

                        
                        
                    </tr>
                    <tr>
                        <td><label class="control-label">Short-term Course/Training </label></td>
                        <td>Instrtude Name</td>
                        <td>Course Name</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td> <input maxlength="200" type="text" name="courses_name[]" class="form-control" /></td>  
                        <td><input maxlength="200" type="text" name="institude_name[]" class="form-control" /></td> 
                    </tr>
                    <tr id="couser_extra_values_add"></tr>
                    <tr>
                        <td></td>
                        <td><button class="btn btn-danger" type="button" id="add_course" >Add</button></td>
                        <td><button class="btn btn-primary" type="button" id="remove_course">Remove</button></td>
                    </tr>
                </table>
                

                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3 class="title-each-tab">Work Experience</h3>
                <table class="add_work_experience_table">
                    <th><label class="control-label">Organization</label></th>
                    <th><label class="control-label">Position</label></th>
                    <th><label class="control-label">Department</label></th>
                    <th><label class="control-label">Experience</label></th>
                    <th><label class="control-label">Experience From</label></th>
                    <th><label class="control-label">Experience To</label></th>
                    <tr>
                        <td> <input maxlength="200" type="text" name="organization[]" class="form-control" <?php echo ($vacancydatas['experience'] == 1)?'required':'' ?> /></td>  
                        <td> <input maxlength="200" type="text"   name="position[]" class="form-control" <?php echo ($vacancydatas['experience'] == 1)?'required':'' ?>   ></td>  
                        <td> <input maxlength="200" type="text"  name="department[]"  class="form-control" <?php echo ($vacancydatas['experience'] == 1)?'required':'' ?> ></td>  
                        <td> <input maxlength="200" type="text"   name="experience[]" class="form-control" <?php echo ($vacancydatas['experience'] == 1)?'required':'' ?>   ></td>  
                        <td> <input maxlength="200" type="text" id="experience_to_date"  name="experience_from[]" class="form-control" <?php echo ($vacancydatas['experience'] == 1)?'required':'' ?>   ></td>  
                        <td> <input maxlength="200" type="text" id="experience_from_date"  name="experience_to[]" class="form-control" <?php echo ($vacancydatas['experience'] == 1)?'required':'' ?>   ></td>
                    </tr>
                    
                    
                    <tr id="add_work_experience"></tr>
                    
                    <tr>
                        <td><input type="button" class="btn btn-primary" id="btnAdd" value="Add Fields" /></td>
                        <td></td>
                        <td><input type="button" class="btn btn-danger" id="btnDel" value="remove Fields" /></td>
                        <td></td>
                    </tr>
                <!--        <tr>-->
                <!--            <td> <input maxlength="200" type="text" name="organization2"   class="form-control" ></td>  -->
                <!--           <td> <input maxlength="200" type="text"  name="position2" class="form-control"   ></td>  -->
                <!--            <td> <input maxlength="200" type="text"  name="department2"  class="form-control"   ></td>  -->
                <!--           <td> <input maxlength="200" type="text"   name="experience2" class="form-control"   ></td>  -->
                <!--        </tr>-->

                <!--<tr>-->
                <!--           <td> <input maxlength="200" type="text" name="organization3"   class="form-control" ></td>  -->
                <!--           <td> <input maxlength="200" type="text"  name="position3" class="form-control"   ></td>  -->
                <!--            <td> <input maxlength="200" type="text"  name="department3"  class="form-control"   ></td>  -->
                <!--           <td> <input maxlength="200" type="text"   name="experience3" class="form-control"   ></td>  -->
                <!--        </tr>-->

                   


                 </table>

                 <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>

     <div class="row setup-content" id="step-4">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3 class="title-each-tab"> Availability & Preferences</h3>
                
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Preferred Post</label>
                            <select class="styled-select slate form-control select_avai" name="preferred_post">
                                <option value="<?php echo $vacancydatas['preferred_post']; ?>"><?php echo $vacancydatas['preferred_post']; ?></option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Vacancy Code</label>
                            <input  maxlength="100" type="text"   name="post[]" class="form-control" class="form-control" >
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Specialised Area</label>
                            <input type="text" value="<?php echo  $vacancydatas['specialized_area']; ?>" readonly class="form-control">
                            <input type="hidden" name="specialised_area" value="<?php echo  $vacancydatas['specialized_area']; ?>">
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Expected Salary</label>
                            <input  maxlength="100" type="text"  name="expected_salary" class="form-control" placeholder="NPR per Month"  />
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">Preferred Location</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                             <select class="styled-select slate select_avai" name="preferred_location" class="form-control">
                                <option value="">Inside Valley</option>
                                <?php foreach($insides as $inside):?>
                                <option value="<?php echo $inside['name'];?>"><?php echo $inside['name'];?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select class="styled-select slate select_avai" name="preferred_location" class="form-control">
                                <option value="">Outside Valley</option>
                                <?php foreach($outside as $out):?>
                                <option value="<?php echo $out['name'];?>"><?php echo $out['name'];?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
                








                        <div class="row">
                            <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Travelling Option</label>
                                       <input type="radio" name="travelling_option" value="Yes">Yes&nbsp;
                                       <input type="radio" name="travelling_option" value="No" >No
                                    </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                        <label class="control-label">Driving license</label>
                                       <input type="radio" name="driving_license" value="None">None&nbsp;
                                       <input type="radio" name="driving_license" value="2 Wheeler" >2 Wheeler&nbsp;
                                       <input type="radio" name="driving_license" value="Wheeler" >Wheeler&nbsp;
                                       <input type="radio" name="driving_license" value="Both">Both
                                    </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Reference 1 *</label>
                                    <input  maxlength="100" type="text" required="required"  name="reference[]" class="form-control"  />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Post</label>
                                    <input  maxlength="100" type="text"   name="post[]" class="form-control"  >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                                    <label class="control-label">Organization</label>
                                    <input  maxlength="100" type="text"   name="organization_ref[]" class="form-control"  >
                                    </div>
                          </div>
                           <div class="col-sm-6">
                             <div class="form-group">
                                    <label class="control-label">Address</label>
                                    <input  maxlength="100" type="text"  name="address[]" class="form-control"   >
                                    </div>
                           </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input  maxlength="100" type="text"  name="email_ref[]" class="form-control" >
                                    </div>
                          </div>
                           <div class="col-sm-6">
                             <div class="form-group">
                                    <label class="control-label">Telephone</label>
                                    <input  maxlength="100" type="text"   name="telephone[]" class="form-control">
                                    </div>
                           </div>
                        </div>
               
                        <div id="append_form"></div>
                            <div class="row btn-add-remove-form">
                                <div class="col-sm-6">
                                    <div class="form-group"> 
                                        <input type="button" class="btn btn-primary" id="btnrefADD" value="Add Reference" />
                                        <input type="button" class="btn btn-danger" id="btnrefDEL" value="Remove Reference" />  
                                    </div>
                                </div>
                            </div>
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6LcgjDsUAAAAAJa3uT3Lh9HsALT9g-B4Th_Ii4X6"  data-callback="recaptcha_callback" data-theme="light"></div>
                        </div>

                        <button class="btn btn-success btn-lg pull-right" id="vacancy-submit-button" type="submit">Finish</button>
            </div>
        </div>
    </div>
<?php echo form_close()?>
</div>
                </div>  <!-- Tab panes ends -->

            </div>
                </div>
        </div>
    </section>
</div>
<style>
    iframe.makeRed{
        border: 1px solid red;
    }
</style>
<script>
     $('#vacancy-submit-button').on('click',function(){
        var response = grecaptcha.getResponse();
        //console.log(response);
        if (!response) {
            $('.g-recaptcha iframe').addClass('makeRed');
            return false;
        } else {  
           return true;
        }
    });
    function callback_capta(){
         $('.g-recaptcha iframe').removeClass('makeRed');
    }</script>
<script>
    $(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
            
        }
    });

    allNextBtn.click(function(){
        var radio = $('input[name=gender]:checked').val();
        var filename = $('#uploaded_cv_file').val();
        // console.log(radio);
        if((radio !=null && radio != undefined && radio != '') || (filename !=null && filename != undefined && filename != '')){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
                
            }
        }
        if(isValid == false)
        {
           $(".has-error :input").first().focus();
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    } 
    else if(radio !=null && radio != undefined && radio != ''){
        alert('Please Select Gender First!!');
        return false;
    }
    else{
        alert('Please Upload CV First!!');
        return false; 
    }
    });
 
    $('div.setup-panel div a.btn-primary').trigger('click');
});
</script>

<script type="text/javascript">

var ref = 2;
$('#btnrefADD').click(function(){
    $('#append_form').append('<div class="avai-form"><div class="row"><div class="col-sm-6"><div class="form-group"><label class="control-label">Reference '+ref+' *</label><input  maxlength="100" type="text" required="required"  name="reference[]" class="form-control"  /></div></div><div class="col-sm-6"><div class="form-group"><label class="control-label">Post</label><input  maxlength="100" type="text"   name="post[]" class="form-control"  ></div></div></div><div class="row"><div class="col-sm-6"><div class="form-group"><label class="control-label">Organization</label><input  maxlength="100" type="text"   name="organization_ref[]" class="form-control"  ></div></div><div class="col-sm-6"><div class="form-group"><label class="control-label">Address</label><input  maxlength="100" type="text"  name="address[]" class="form-control"   ></div></div></div><div class="row"><div class="col-sm-6"><div class="form-group"><label class="control-label">Email</label><input  maxlength="100" type="text"  name="email_ref[]" class="form-control" ></div></div><div class="col-sm-6"><div class="form-group"><label class="control-label">Telephone</label><input  maxlength="100" type="text"   name="telephone[]" class="form-control"></div></div></div></div>');
    ref++;
});

$('#btnrefDEL').click(function(){
  $('#append_form .avai-form').last().remove();
  if(ref < 2){
    ref--;  
  }
  else{
      ref=2;
  }
  
});

</script>
<script>
    $(document).ready(function(){
        $('#experience_to_date').datepicker();
         $('#experience_from_date').datepicker();
          $('#remove_course').attr('disabled',true);
    });
    
    $('#add_course').click(function(){
         $('#couser_extra_values_add').after('<tr class="added-row"><td></td><td> <input maxlength="200" type="text" name="courses_name[]" class="form-control" /></td><td><input maxlength="200" type="text" name="institude_name[]" class="form-control" /></td></tr>');
          $('#remove_course').attr('disabled',false);
        
    });
    
    $('#remove_course').click(function(){
         $('#couser_extra_values .added-row').last().remove();
    });
    
    
    $('#btnAdd').click(function(){
        $('#add_work_experience').after('<tr class="added-row-work"><td> <input maxlength="200" type="text" name="organization[]" class="form-control" <?php echo ($vacancydatas['experience'] == 1)?'required':'' ?> /></td>  <td> <input maxlength="200" type="text"   name="position[]" class="form-control" <?php echo ($vacancydatas['experience'] == 1)?'required':'' ?>   ></td> <td> <input maxlength="200" type="text"  name="department[]"  class="form-control" <?php echo ($vacancydatas['experience'] == 1)?'required':'' ?> ></td>  <td> <input maxlength="200" type="text"   name="experience[]" class="form-control" <?php echo ($vacancydatas['experience'] == 1)?'required':'' ?>   ></td>  <td> <input maxlength="200" type="text" id="experience_to_date"  name="experience_from[]" class="form-control" <?php echo ($vacancydatas['experience'] == 1)?'required':'' ?>   ></td>  <td> <input maxlength="200" type="text" id="experience_from_date"  name="experience_to[]" class="form-control" <?php echo ($vacancydatas['experience'] == 1)?'required':'' ?>   ></td></tr>');
        $('#btnDel').attr('disabled',false);
        
    });
    
    $('#btnDel').click(function(){
         $('.add_work_experience_table .added-row-work').last().remove();
    });
</script>

<script>


    $('#zone_name_section_form').change(function(){
        $('#district_name_section_form').empty();
        var zone = $('#zone_name_section_form').val();
          var csrf_ctzn_token = $('#csrf').val();
        $.ajax({
                url: '<?php echo site_url('home/getdistrictjson')?>',
                    data:{ zone:zone ,
                    csrf_ctzn_token : csrf_ctzn_token       
                },
                // data: {bodydata:data,formdata},
                dataType: 'json',
                success: function(result){
                    if(result){
                       $.each( result.districts, function( key, value ) {
                            $('#district_name_section_form').append("<option value ='"+value.district+"'>"+value.district+"</option>");
                       }); 
                    }
                    
                },
                
                
                type: 'POST'
            });
       
    });

    $('#t_zone_change').change(function(){
        $('#t_district_change').empty();
        var zone = $('#t_zone_change').val();
          var csrf_ctzn_token = $('#csrf').val();
        $.ajax({
                url: '<?php echo site_url('home/getdistrictjson')?>',
                    data:{ zone:zone ,
                    csrf_ctzn_token : csrf_ctzn_token       
                },
                // data: {bodydata:data,formdata},
                dataType: 'json',
                success: function(result){
                    if(result){
                       $.each( result.districts, function( key, value ) {
                            $('#t_district_change').append("<option value ='"+value.district+"'>"+value.district+"</option>");
                       }); 
                    }
                    
                },
                
                
                type: 'POST'
            });
       
    });
    function check_extension()
    {
        var filename = $('#uploaded_cv_file').val();
        var extension = filename.substr( (filename.lastIndexOf('.') +1) );
        // alert(extension);
        if(extension != 'pdf' && extension != 'docx' && extension != 'doc'){
             alert("please upload cv only on pdf or doc or docx format");
             $('#uploaded_cv_file').val('');
         }
        
    }
</script>