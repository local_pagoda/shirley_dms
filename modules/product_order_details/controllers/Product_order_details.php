<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Product_order_details
 *
 * Extends the Public_Controller class
 * 
 */

class Product_order_details extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Product Order Details');

        $this->load->model('product_order_details/product_order_detail_model');
        $this->lang->load('product_order_details/product_order_detail');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('product_order_details');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'product_order_details';
		$this->load->view($this->_container,$data);
	}
}