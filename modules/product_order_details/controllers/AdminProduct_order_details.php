<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Product_order_details
 *
 * Extends the Project_Controller class
 * 
 */

class AdminProduct_order_details extends Project_Controller
{
  public function __construct()
  {
      parent::__construct();
      // var_dump(control('Product Order Details'));
      control('Product Orders Details');
      // control('Product Colors');

        $this->load->model('product_order_details/product_order_detail_model');
        $this->lang->load('product_order_details/product_order_detail');
        $this->load->model('product_orders/product_order_model');
        $this->load->model('product_order_statuses/product_order_status_model');
        $this->load->model('colors/color_model');
        $this->load->model('product_colors/product_color_model');
         $this->load->model('users/user_model');
         $this->load->model('wholesaler_profiles/wholesaler_profile_model');
$this->load->model('custom_color_orders/custom_color_order_model');


    }

  public function index()
  {
    // echo '<pre>'; print_r($this->session->userdata('group_id')); exit;
    // Display Page
    $this->product_order_model->_table ="view_order_detail";
    $data['header'] = lang('product_order_details');
    $data['page'] = $this->config->item('template_admin') . "index";
    $data['module'] = 'product_order_details';
    $data['logingrop'] = $this->session->userdata('group_id');
    $this->load->view($this->_container,$data);
  }

  public function json()
  {
    $user_id = $this->session->userdata('id');

    if($this->session->userdata('group_id')==2){

    
  $this->product_order_detail_model->_table ="view_order_detail";

    search_params();
    $this->db->where('flag_id',0);
    $total=$this->product_order_detail_model->find_count();
    
    paging('id');
    
    search_params();
    $this->db->where('flag_id',0);
    $rows=$this->product_order_detail_model->findAll();
    
    echo json_encode(array('total'=>$total,'rows'=>$rows));
    exit;
    }

    else{

     
    $this->product_order_detail_model->_table ="view_order_detail";
    search_params();

      $this->db->where('created_by',$user_id);
    $this->db->where('flag_id',0);
    $total=$this->product_order_detail_model->find_count();
    
    paging('id');
    
    search_params();
    $this->db->where('flag_id',0);
    $rows=$this->product_order_detail_model->findAll();
    // echo"<pre>";
    // print_r($rows);
    // exit;
    echo json_encode(array('total'=>$total,'rows'=>$rows));
    exit;
    }
    
    
    
  }

  public function save()
  {
     $data=$this->_get_posted_data();
     // echo"<pre>";

     // print_r($data);
     // exit;
    $this->product_order_detail_model->_table ="mst_year";
      search_params();
      $this->db->where('status',1);
      $rows=$this->product_order_detail_model->find(); 
      $data['year_id'] =$rows->id;
     
         
         $product_order = json_decode($data['product_order']);
         $custom_color = json_decode($data['custom_color']);
         
       unset($data['product_order']);
       unset($data['custom_color']);
      
    if(!$this->input->post('id'))
        {
           

          
          $this->product_order_detail_model->_table = "odr_product_order_detail";
           $success=$this->product_order_detail_model->insert($data);
           $result = $success;

           $status['product_order_detail_id'] = $result;
           $status['status'] = 'Pending';

           $this->product_order_status_model->insert($status);

         
         foreach ($product_order as $key => $value) {
            
            $order['total'] = $value->total;
            $order['price'] = $value->price;
            $order['product_weight_id'] = $value->product_weight_id;
            $order['color_group_id'] = $value->product_group;
            
            $order['qty'] = $value->qty;
            $order['order_id']  = $result;
                $this->product_order_model->insert($order);
                
          }

           foreach ($custom_color as $key => $value) {
            
            $custom['total'] = $value->total;
            $custom['price'] = $value->price;
            $custom['product_weight_id'] = $value->product_weight_id;
            $custom['color_id'] = $value->color_id;
            
            $custom['qty'] = $value->qty;
            $custom['order_id']  = $result;
            
                $this->custom_color_order_model->insert($custom);
                
          }
          
      
        } 
        else
        {

            $this->product_order_detail_model->_table = "odr_product_order_detail";
            $success=$this->product_order_detail_model->update($data['id'],$data);
            $result = $data['id'];
      //       echo"<pre>";
        // print_r( $result);
      //   exit;
          $this->db->where('order_id',$data['id']);
          $this->db->delete('odr_product_order');
          foreach($product_order as $key=>$value){
               
            $order['total'] = $value->total;
            $order['price'] = $value->price;
            $order['product_weight_id'] = $value->product_weight_id;
            $order['color_group_id'] = $value->product_group;
            $order['qty'] = $value->qty;
            $order['order_id']  = $result;           
               $this->product_order_model->insert($order);
             
          }


           $this->db->where('order_id',$data['id']);
          $this->db->delete('odr_custom_color_order');
      foreach ($custom_color as $key => $value) {
            
            $custom['total'] = $value->total;
            $custom['price'] = $value->price;
            $custom['product_weight_id'] = $value->product_weight_id;
            $custom['color_id'] = $value->color_id;
            
            $custom['qty'] = $value->qty;
            $custom['order_id']  = $result;
            
                $this->custom_color_order_model->insert($custom);
                
          }


        
         
    }

    if($success)
    {
      $success = TRUE;
      $msg=lang('general_success');
    }
    else
    {
      $success = FALSE;
      $msg=lang('general_failure');
    }

     echo json_encode(array('msg'=>$msg,'success'=>$success));
     exit;
  }

   private function _get_posted_data()
   {
      $data=array();
      if($this->input->post('id')) {
      $data['id'] = $this->input->post('id');
    }
    
    $data['w_id'] = $this->input->post('w_id');
    $data['date_of_order'] = $this->input->post('date_of_order');
    $data['season_id'] = $this->input->post('season_id');
    $data['year_id'] = $this->input->post('year_id');
    $data['product_order'] = $this->input->post('product_order');
    $data['custom_color'] = $this->input->post('custom_color');

        return $data;
   }

   public function get_final_invoice_json()
   {
      $rows = array();
      $id = $this->input->post('id');
      $this->product_order_detail_model->_table ="view_confirm_order";
      $this->db->where('id',$id);
      $rows=$this->product_order_detail_model->findAll();   
      echo json_encode(array('rows'=>$rows));
      exit;
   }

   public function get_payment_data_json()
   {
      $rows = array();
      $id = $this->input->post('id');
      $this->product_order_detail_model->_table ="view_confirm_order";
      $this->db->where('id',$id);
      $rows=$this->product_order_detail_model->findAll();
   
      echo json_encode(array('rows'=>$rows));
      exit;
   }

   public function get_invoice_json()
   {
      $rows = array();
      $id = $this->input->post('id');
      $this->product_order_detail_model->_table ="view_product_order";
      $this->db->where('order_id',$id);
      $rows[]=$this->product_order_detail_model->findAll();
      $this->db->where('product_order_detail_id',$id);
      $rows[]=$this->product_order_status_model->findAll();
      $this->product_order_detail_model->_table ="odr_product_order_detail";      
      $this->db->where('id',$id);
      $rows[]= $this->product_order_detail_model->find(); 
      // echo $this->db->last_query(); 
      $this->product_order_detail_model->_table ="view_custom_order";
      $this->db->where('order_id',$id);
      $rows[]=$this->product_order_detail_model->findAll();

      // echo"<pre>";
      // print_r($rows);
      // exit;
      echo json_encode(array('rows'=>$rows));
      exit;

   }


  
   //niroj function 
   public function invoice_update()
   {
      $row = json_decode($this->input->post('row'));
      $status = $this->input->post('status');
      $deposite = $this->input->post('deposite');
      $order_id = $this->input->post('order_id');
      $this->db->where('order_id',$order_id);
      $this->db->delete('odr_product_order');

      foreach ($row as $key => $value) {
         $order['order_id'] = $order_id;
         $this->db->where('product_group',$value->product_group);
         $colors=$this->product_color_model->find();
         $order['color_group_id'] = $colors->id;
         $order['product_weight_id'] = $value->product_weight_id;
         $order['qty'] = $value->qty;
         $order['price'] = $value->price;
         $order['total'] = $value->total;
         $this->product_order_model->insert($order);

      }
      // $data['id'] = $order_id;
      // $data['deposite'] = $deposite;
      $this->db->set('deposite',$deposite);
      $this->db->where('id',$order_id);
      $this->db->update('odr_product_order_detail');
      $this->db->set('status',$status);
      $this->db->where('product_order_detail_id',$order_id);
      $this->db->update('rel_product_order_status');
      $success = TRUE;
      $msg=lang('general_success');
      echo json_encode(array('msg'=>$msg,'success'=>$success));
      exit;
      
   }
   //niroj function
   public function final_invoice_update()
   {
      $row = json_decode($this->input->post('row'));
      $status = $this->input->post('status');
      // $deposite = $this->input->post('deposite');
      $order_id = $this->input->post('order_id');

      $this->db->where('order_id',$order_id);
      $this->db->delete('odr_product_order');
      foreach ($row as $key => $value) {
         $order['order_id'] = $order_id;
         $this->db->where('product_group',$value->product_group);
         $colors=$this->product_color_model->find();

         $order['color_group_id'] = $colors->id;
         $order['product_weight_id'] = $value->product_weight_id;
         $order['qty'] = $value->qty;
         $order['price'] = $value->price;
         $order['total'] = $value->total;      
         $order['product_cancellation_status'] = $value->cancel_order;
         // print_r($order); exit;
         $this->product_order_model->insert($order);
      } 

      $this->db->set(array('order_status'=>$status));
      $this->db->where('id',$order_id);
      $this->db->update('odr_product_order_detail');
      $success = TRUE;
      $msg=lang('general_success');
      echo json_encode(array('msg'=>$msg,'success'=>$success));
      exit;
    
   }
   //niroj function 
   public function save_payment_informtaion()
   {
      $deposite = $this->input->post('deposite');
      $order_id = $this->input->post('order_id');
      $deposite_date = $this->input->post('deposite_date');
      $this->db->set(array('deposite_received'=>$deposite,'deposite_date'=>$deposite_date));
      $this->db->where('id',$order_id);
      $this->db->update('odr_product_order_detail');
      $success = TRUE;
      $msg=lang('general_success');
      echo json_encode(array('msg'=>$msg,'success'=>$success));
      exit;

   }
   public function delete()
   {
       $data['id'] = $this->input->post('id')[0];
       $success = $this->product_order_detail_model->delete($data['id']);
     
       $this->db->where('order_id',$data['id']);
       $this->product_order_model->delete_by();
       $success = TRUE;
       $msg=lang('general_success');    
       echo json_encode(array('msg'=>$msg,'success'=>$success));
       exit;

  }
  public function getPrices(){
    $product_weight_id = $this->input->post('id');
    $user_id = $this->session->userdata('id');
    $this->user_model->_table= "aauth_users";
   
    $this->db->where('id',$user_id);
    $wholesaler=$this->user_model->find();

    $this->wholesaler_profile_model->_table = "auth_wholesaler_profile";
    $this->db->where('id',$wholesaler->w_id);
    $currency=$this->wholesaler_profile_model->find();

 $this->product_order_detail_model->_table="view_product_price";
 $where = array('currency_id'=>$currency->currency_id,'product_weight_id'=>$product_weight_id);

     $rows=$this->product_order_detail_model->find($where);

    
    echo json_encode($rows);

    exit;
  
  }


function view_confirm_order(){

   
    $data['header'] = 'Confirm Orders';
    $data['id'] =$this->session->userdata('id');
    $data['logingrop'] = $this->session->userdata('group_id');     
    $data['page'] = $this->config->item('template_admin') . "view_confirm_order";
    $data['module'] = 'product_order_details';
      // echo $this->confirm_order();exit;
    $this->load->view($this->_container,$data);
}

  function confirm_detail_json($id){

      if($this->session->userdata('group_id')== 2){
        $this->product_order_detail_model->_table ="view_confirm_order";

    search_params();
  
    $this->db->where('flag_id',1);
    $total=$this->product_order_detail_model->find_count();
   
    paging('id');
    
    search_params();
    $this->db->where('flag_id',1);
    
    $rows=$this->product_order_detail_model->findAll();
    // print_r($rows);
    // exit;
    echo json_encode(array('total'=>$total,'rows'=>$rows));
    exit;
    }
    else{
      $this->product_order_detail_model->_table ="view_confirm_order";

    search_params();
  
    $this->db->where('created_by',$id);
     $this->db->where('flag_id',1);

    $total=$this->product_order_detail_model->find_count();
    // print_r($total);
    // exit;
    paging('id');
    
    search_params();
    $this->db->where('flag_id',1);
    
    $rows=$this->product_order_detail_model->findAll();
    // print_r($rows);
    // exit;
    echo json_encode(array('total'=>$total,'rows'=>$rows));
    exit;
    }

     
  }


  function confirmOrder(){
    $data['id'] = $this->input->post('id');
    $data['flag_id'] =1;


     $success=$this->product_order_detail_model->update($data['id'],$data);
      echo json_encode($success);
    exit;
  }
}