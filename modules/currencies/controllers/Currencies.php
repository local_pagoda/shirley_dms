<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Currencies
 *
 * Extends the Public_Controller class
 * 
 */

class Currencies extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Currencies');

        $this->load->model('currencies/currency_model');
        $this->lang->load('currencies/currency');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('currencies');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'currencies';
		$this->load->view($this->_container,$data);
	}
}