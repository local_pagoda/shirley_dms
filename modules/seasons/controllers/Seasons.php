<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Seasons
 *
 * Extends the Public_Controller class
 * 
 */

class Seasons extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Seasons');

        $this->load->model('seasons/season_model');
        $this->lang->load('seasons/season');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('seasons');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'seasons';
		$this->load->view($this->_container,$data);
	}
}