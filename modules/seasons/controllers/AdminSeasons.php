<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Seasons
 *
 * Extends the Project_Controller class
 * 
 */

class AdminSeasons extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Seasons');

        $this->load->model('seasons/season_model');
        $this->lang->load('seasons/season');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('seasons');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'seasons';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->season_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->season_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->season_model->insert($data);
        }
        else
        {
            $success=$this->season_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		
		$data['season'] = $this->input->post('season');

        return $data;
   }
   public function getseason(){

		search_params();
		
		$total=$this->season_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->season_model->findAll();
		
		echo json_encode($rows);
		exit;
	
   }

   public function deleteSeasons(){
   	$data['id'] = $this->input->post('id');

   	 $success = $this->season_model->delete($data['id']);
      
       
      
       $success = TRUE;
       $msg=lang('general_success');    
       echo json_encode(array('msg'=>$msg,'success'=>$success));
       exit;
   }
}