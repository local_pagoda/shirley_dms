<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['order_id'] = 'Order Id';
$lang['color_id'] = 'Color Id';
$lang['product_weight_id'] = 'Product Weight Id';
$lang['qty'] = 'Qty';
$lang['price'] = 'Price';
$lang['total'] = 'Total';

$lang['custom_color_orders']='Custom Color Orders';