<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('custom_color_orders'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('custom_color_orders'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridCustom_color_orderToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridCustom_color_orderInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridCustom_color_orderFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridCustom_color_order"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowCustom_color_order">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-custom_color_orders', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "custom_color_orders_id"/>
            <table class="form-table">
				<tr>
					<td><label for='created_by'><?php echo lang('created_by')?></label></td>
					<td><div id='created_by' class='number_general' name='created_by'></div></td>
				</tr>
				<tr>
					<td><label for='updated_by'><?php echo lang('updated_by')?></label></td>
					<td><div id='updated_by' class='number_general' name='updated_by'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_by'><?php echo lang('deleted_by')?></label></td>
					<td><div id='deleted_by' class='number_general' name='deleted_by'></div></td>
				</tr>
				<tr>
					<td><label for='created_at'><?php echo lang('created_at')?></label></td>
					<td><input id='created_at' class='text_input' name='created_at'></td>
				</tr>
				<tr>
					<td><label for='updated_at'><?php echo lang('updated_at')?></label></td>
					<td><input id='updated_at' class='text_input' name='updated_at'></td>
				</tr>
				<tr>
					<td><label for='deleted_at'><?php echo lang('deleted_at')?></label></td>
					<td><input id='deleted_at' class='text_input' name='deleted_at'></td>
				</tr>
				<tr>
					<td><label for='order_id'><?php echo lang('order_id')?></label></td>
					<td><div id='order_id' class='number_general' name='order_id'></div></td>
				</tr>
				<tr>
					<td><label for='color_id'><?php echo lang('color_id')?></label></td>
					<td><div id='color_id' class='number_general' name='color_id'></div></td>
				</tr>
				<tr>
					<td><label for='product_weight_id'><?php echo lang('product_weight_id')?></label></td>
					<td><div id='product_weight_id' class='number_general' name='product_weight_id'></div></td>
				</tr>
				<tr>
					<td><label for='qty'><?php echo lang('qty')?></label></td>
					<td><div id='qty' class='number_general' name='qty'></div></td>
				</tr>
				<tr>
					<td><label for='price'><?php echo lang('price')?></label></td>
					<td><input id='price' class='text_input' name='price'></td>
				</tr>
				<tr>
					<td><label for='total'><?php echo lang('total')?></label></td>
					<td><input id='total' class='text_input' name='total'></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxCustom_color_orderSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxCustom_color_orderCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

	var custom_color_ordersDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'order_id', type: 'number' },
			{ name: 'color_id', type: 'number' },
			{ name: 'product_weight_id', type: 'number' },
			{ name: 'qty', type: 'number' },
			{ name: 'price', type: 'string' },
			{ name: 'total', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/custom_color_orders/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	custom_color_ordersDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridCustom_color_order").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridCustom_color_order").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridCustom_color_order").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: custom_color_ordersDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridCustom_color_orderToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editCustom_color_orderRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("order_id"); ?>',datafield: 'order_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("color_id"); ?>',datafield: 'color_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("product_weight_id"); ?>',datafield: 'product_weight_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("qty"); ?>',datafield: 'qty',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("price"); ?>',datafield: 'price',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("total"); ?>',datafield: 'total',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridCustom_color_order").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridCustom_color_orderFilterClear', function () { 
		$('#jqxGridCustom_color_order').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridCustom_color_orderInsert', function () { 
		openPopupWindow('jqxPopupWindowCustom_color_order', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowCustom_color_order").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowCustom_color_order").on('close', function () {
        reset_form_custom_color_orders();
    });

    $("#jqxCustom_color_orderCancelButton").on('click', function () {
        reset_form_custom_color_orders();
        $('#jqxPopupWindowCustom_color_order').jqxWindow('close');
    });

    /*$('#form-custom_color_orders').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#order_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#order_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#color_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#color_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#product_weight_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#product_weight_id').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#qty', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#qty').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#price', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#price').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#total', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#total').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxCustom_color_orderSubmitButton").on('click', function () {
        saveCustom_color_orderRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveCustom_color_orderRecord();
                }
            };
        $('#form-custom_color_orders').jqxValidator('validate', validationResult);
        */
    });
});

function editCustom_color_orderRecord(index){
    var row =  $("#jqxGridCustom_color_order").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#custom_color_orders_id').val(row.id);
        $('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#created_at').val(row.created_at);
		$('#updated_at').val(row.updated_at);
		$('#deleted_at').val(row.deleted_at);
		$('#order_id').jqxNumberInput('val', row.order_id);
		$('#color_id').jqxNumberInput('val', row.color_id);
		$('#product_weight_id').jqxNumberInput('val', row.product_weight_id);
		$('#qty').jqxNumberInput('val', row.qty);
		$('#price').val(row.price);
		$('#total').val(row.total);
		
        openPopupWindow('jqxPopupWindowCustom_color_order', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveCustom_color_orderRecord(){
    var data = $("#form-custom_color_orders").serialize();
	
	$('#jqxPopupWindowCustom_color_order').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/custom_color_orders/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_custom_color_orders();
                $('#jqxGridCustom_color_order').jqxGrid('updatebounddata');
                $('#jqxPopupWindowCustom_color_order').jqxWindow('close');
            }
            $('#jqxPopupWindowCustom_color_order').unblock();
        }
    });
}

function reset_form_custom_color_orders(){
	$('#custom_color_orders_id').val('');
    $('#form-custom_color_orders')[0].reset();
}
</script>