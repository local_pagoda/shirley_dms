<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Custom_color_orders
 *
 * Extends the Project_Controller class
 * 
 */

class AdminCustom_color_orders extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Custom Color Orders');

        $this->load->model('custom_color_orders/custom_color_order_model');
        $this->lang->load('custom_color_orders/custom_color_order');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('custom_color_orders');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'custom_color_orders';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->custom_color_order_model->_table ="view_custom_order";
		search_params();
		
		$total=$this->custom_color_order_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->custom_color_order_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->custom_color_order_model->insert($data);
        }
        else
        {
            $success=$this->custom_color_order_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['created_at'] = $this->input->post('created_at');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['order_id'] = $this->input->post('order_id');
		$data['color_id'] = $this->input->post('color_id');
		$data['product_weight_id'] = $this->input->post('product_weight_id');
		$data['qty'] = $this->input->post('qty');
		$data['price'] = $this->input->post('price');
		$data['total'] = $this->input->post('total');

        return $data;
   }

   public function getCustomOrders(){
   	$order_id = $this->input->post('id');
   	$this->custom_color_order_model->_table ="view_custom_order";
		search_params();
		$this->db->where('order_id',$order_id);
		$total=$this->custom_color_order_model->find_count();
		
		paging('id');
		
		search_params();
		$this->db->where('order_id',$order_id);
		$rows=$this->custom_color_order_model->findAll();
		
		 echo json_encode($rows);
		exit;

		
    
   }

   public function getCustomcolorOrders(){
   	$order_id = $this->input->post('id');
   	$this->custom_color_order_model->_table ="view_custom_order";
		search_params();
		$this->db->where('order_id',$order_id);
		$total=$this->custom_color_order_model->find_count();
		
		paging('id');
		
		search_params();
		$this->db->where('order_id',$order_id);
		$rows=$this->custom_color_order_model->findAll();
		
		 echo json_encode($rows);
		exit;

		
    
   }
}