<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Custom_color_orders
 *
 * Extends the Public_Controller class
 * 
 */

class Custom_color_orders extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Custom Color Orders');

        $this->load->model('custom_color_orders/custom_color_order_model');
        $this->lang->load('custom_color_orders/custom_color_order');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('custom_color_orders');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'custom_color_orders';
		$this->load->view($this->_container,$data);
	}
}