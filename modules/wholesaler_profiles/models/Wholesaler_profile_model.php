<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class Wholesaler_profile_model extends MY_Model
{

    public  $_table = 'auth_wholesaler_profile';

    public $blamable = TRUE;

}