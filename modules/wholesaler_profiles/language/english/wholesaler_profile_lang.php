<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['deleted_by'] = 'Deleted By';
$lang['created_at'] = 'Created At';
$lang['updated_at'] = 'Updated At';
$lang['deleted_at'] = 'Deleted At';
$lang['company_name'] = 'Company Name';
$lang['email_id'] = 'Email Id';
$lang['depositer_type_id'] = 'Depositer Type';
$lang['currency_id'] = 'Currency';
$lang['contact_no'] = 'Contact No';
$lang['country_id'] = 'Country';
$lang['state'] = 'State';
$lang['city'] = 'City';
$lang['street'] = 'Street';
$lang['zip'] = 'Zip';
$lang['year_id'] = 'Year';

$lang['wholesaler_profiles']='Wholesaler Profiles';