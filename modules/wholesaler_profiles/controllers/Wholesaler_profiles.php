<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Wholesaler_profiles
 *
 * Extends the Public_Controller class
 * 
 */

class Wholesaler_profiles extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Wholesaler Profiles');

        $this->load->model('wholesaler_profiles/wholesaler_profile_model');
        $this->lang->load('wholesaler_profiles/wholesaler_profile');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('wholesaler_profiles');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'wholesaler_profiles';
		$this->load->view($this->_container,$data);
	}
}