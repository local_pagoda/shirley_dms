<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Wholesaler_profiles
 *
 * Extends the Project_Controller class
 * 
 */

class AdminWholesaler_profiles extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Wholesaler Profiles');

        $this->load->model('wholesaler_profiles/wholesaler_profile_model');
        $this->lang->load('wholesaler_profiles/wholesaler_profile');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('wholesaler_profiles');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'wholesaler_profiles';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		$this->wholesaler_profile_model->_table = "view_wholesaler_profile";
		search_params();
		
		$total=$this->wholesaler_profile_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->wholesaler_profile_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function get_wholesaler()
	{
		$rows=$this->wholesaler_profile_model->findAll();
		echo json_encode(array('rows'=>$rows));
		exit;
	}

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data
        

        if(!$this->input->post('id'))
        {
            $success=$this->wholesaler_profile_model->insert($data);
           

        }
        else
        {
            $success=$this->wholesaler_profile_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		// $data['created_by'] = $this->input->post('created_by');
		// $data['updated_by'] = $this->input->post('updated_by');
		// $data['deleted_by'] = $this->input->post('deleted_by');
		// $data['created_at'] = $this->input->post('created_at');
		// $data['updated_at'] = $this->input->post('updated_at');
		// $data['deleted_at'] = $this->input->post('deleted_at');
		$data['company_name'] = $this->input->post('company_name');
		$data['email_id'] = $this->input->post('email_id');
		$data['depositer_type_id'] = $this->input->post('depositer_type_id');
		$data['currency_id'] = $this->input->post('currency_id');
		$data['contact_no'] = $this->input->post('contact_no');
		$data['country_id'] = $this->input->post('country_id');
		$data['state'] = $this->input->post('state');
		$data['city'] = $this->input->post('city');
		$data['street'] = $this->input->post('street');
		$data['zip'] = $this->input->post('zip');
		

        return $data;
   }
}