<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('wholesaler_profiles'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('wholesaler_profiles'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridWholesaler_profileToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridWholesaler_profileInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridWholesaler_profileFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridWholesaler_profile"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowWholesaler_profile">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-wholesaler_profiles', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "wholesaler_profiles_id"/>


<div class="row">
	<div class="col-sm-6">
		 <div class="form-group">
		 	<div class="row">
				<div class="col-sm-3"> <label for="company_name"><?php echo lang('company_name')?></label></div>
				<div class="col-sm-4">  <input type="company_name"  name="company_name" class="text_input" id="company_name"></div>
			</div>
   

   
  </div>
	</div>

	<div class="col-sm-6">
		 <div class="form-group">

   <div class="row">
				<div class="col-sm-3">  <label for="email_id"><?php echo lang('email_id')?></label></div>
				<div class="col-sm-4">   <input type="email_id"  name="email_id" class="text_input" id="email_id"></div>
			</div>
  </div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3"> <label for="depositer_type_id"><?php echo lang('depositer_type_id')?></label></div>
				<div class="col-sm-4">  <div  class="number_general"  id="depositer_type_id" name="depositer_type_id"></div></div>
			</div>
   
  
  </div>
	</div>
	<div class="col-sm-6">
		 <div class="form-group">

		 	<div class="row">
				<div class="col-sm-3"> 
    			<label for="currency_id"><?php echo lang('currency_id')?></label></div>
				<div class="col-sm-4">  <div  class="number_general" id="currency_id" name="currency_id"></div></div>
			</div>
  
  </div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
    <div class="row">
				<div class="col-sm-3">  <label for="contact_no"><?php echo lang('contact_no')?></label></div>
				<div class="col-sm-4"><div  id='contact_no' class='number_general' name='contact_no'></div></div>
				
			</div>

  </div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
    <div class="row">
				<div class="col-sm-3"> <label for="country_id"><?php echo lang('country_id')?></label></div>
				<div class="col-sm-4"><div name="country_id" class="number_general" id="country_id"></div></div>
			</div>
  </div>

	</div>

</div>
<div class="row">
	<div class="col-sm-6">
		
  <div class="form-group">
   <div class="row">
				<div class="col-sm-3"> <label for="state"><?php echo lang('state')?></label></div>
				<div class="col-sm-4"> <input type="state"   name="state"  class="text_input" id="state"></div>
			</div>
  </div>
	</div>
	<div class="col-sm-6">
  <div class="form-group">
  <div class="row">
				<div class="col-sm-3"><label for="city"><?php echo lang('city')?></label></div>
				<div class="col-sm-4"> <input type="city"  name="city" class="text_input" id="city"></div>
			</div>
  </div>
	</div>

</div>
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
  <div class="row">
				<div class="col-sm-3"> <label for="street"><?php echo lang('street')?></label></div>
				<div class="col-sm-4">  <input type="street" name="street" class="text_input" id="street"></div>
			</div>
  </div>
	</div>
	<div class="col-sm-6">
		 <div class="form-group">
    <div class="row">
				<div class="col-sm-3"> <label for="zip"><?php echo lang('zip')?></label></div>
				<div class="col-sm-4"> <input type="zip" name="zip"  class="text_input" id="zip"></div>
			</div>
  </div>
	</div>

</div>

				
	<div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
     <button type="button" class="btn btn-success " id="jqxWholesaler_profileSubmitButton"><?php echo lang('general_save'); ?></button>
     <button type="button" class="btn btn-default " id="jqxWholesaler_profileCancelButton"><?php echo lang('general_cancel'); ?></button>
    </div>
    </div>                

        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">
 var depositerAdapter;
var currencyAdapter;
var countryAdapter;
$(function(){

	var wholesaler_profilesDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			// { name: 'created_by', type: 'number' },
			// { name: 'updated_by', type: 'number' },
			// { name: 'deleted_by', type: 'number' },
			// { name: 'created_at', type: 'date' },
			// { name: 'updated_at', type: 'date' },
			// { name: 'deleted_at', type: 'date' },
			{ name: 'currency_id', type: 'number' },
			{ name: 'country_id', type: 'number' },
			{ name: 'depositer_type_id', type: 'number' },
			
			{ name: 'company_name', type: 'string' },
			{ name: 'email_id', type: 'string' },
			{ name: 'name', type: 'string' },
			{ name: 'currency', type: 'string' },
			{ name: 'contact_no', type: 'number' },
			{ name: 'country_name', type: 'string' },
			{ name: 'state', type: 'string' },
			{ name: 'city', type: 'string' },
			{ name: 'street', type: 'string' },
			{ name: 'zip', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/wholesaler_profiles/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	wholesaler_profilesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridWholesaler_profile").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridWholesaler_profile").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridWholesaler_profile").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: wholesaler_profilesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridWholesaler_profileToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editWholesaler_profileRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			//{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
			{ text: '<?php echo lang("company_name"); ?>',datafield: 'company_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("email_id"); ?>',datafield: 'email_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("depositer_type_id"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("currency_id"); ?>',datafield: 'currency',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("contact_no"); ?>',datafield: 'contact_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("country_id"); ?>',datafield: 'country_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("state"); ?>',datafield: 'state',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("city"); ?>',datafield: 'city',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("street"); ?>',datafield: 'street',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("zip"); ?>',datafield: 'zip',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridWholesaler_profile").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridWholesaler_profileFilterClear', function () { 
		$('#jqxGridWholesaler_profile').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridWholesaler_profileInsert', function () { 
		openPopupWindow('jqxPopupWindowWholesaler_profile', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowWholesaler_profile").jqxWindow({ 
        theme: theme,
        width: '1000px',
        maxWidth: '75%',
        height: '450px',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowWholesaler_profile").on('close', function () {
        reset_form_wholesaler_profiles();
    });

    $("#jqxWholesaler_profileCancelButton").on('click', function () {
        reset_form_wholesaler_profiles();
        $('#jqxPopupWindowWholesaler_profile').jqxWindow('close');
    });

    /*$('#form-wholesaler_profiles').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#company_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#company_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#email_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#email_id').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#depositer_type', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#depositer_type').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#currency', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#currency').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#contact_no', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#contact_no').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#country', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#country').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#state', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#state').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#city', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#city').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#street', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#street').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#zip', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#zip').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxWholesaler_profileSubmitButton").on('click', function () {
        saveWholesaler_profileRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveWholesaler_profileRecord();
                }
            };
        $('#form-wholesaler_profiles').jqxValidator('validate', validationResult);
        */
    });

     var depositercount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'name',type: 'string'},
    	],
    	url:'<?php echo site_url('admin/deposit_types/get_depositer')?>'

    }
    depositerAdapter = new $.jqx.dataAdapter(depositercount);

		 $("#depositer_type_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: depositerAdapter,
			displayMember: "name",
			valueMember: "id",
		});


		  var currencycount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'currency',type: 'string'},
    	],
    	url:'<?php echo site_url('admin/currencies/get_currency')?>'

    }
    currencyAdapter = new $.jqx.dataAdapter(currencycount);

		 $("#currency_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: currencyAdapter,
			displayMember: "currency",
			valueMember: "id",
		});

var countrycount=
    {
    	datatype:"json",
    	datafields:[
    	{ name: 'id',type: 'string'},
         { name: 'name',type: 'string'},
    	],
    	url:'<?php echo site_url('admin/currencies/get_countries')?>'

    }
    countryAdapter = new $.jqx.dataAdapter(countrycount);

		 $("#country_id").jqxComboBox({
			theme: theme,
			width: 195,
			height: 25,
			selectionMode: 'dropDownList',
			autoComplete: true,
			searchMode: 'containsignorecase',
			source: countryAdapter,
			displayMember: "name",
			valueMember: "id",
		});

});

function editWholesaler_profileRecord(index){
    var row =  $("#jqxGridWholesaler_profile").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#wholesaler_profiles_id').val(row.id);
  //       $('#created_by').jqxNumberInput('val', row.created_by);
		// $('#updated_by').jqxNumberInput('val', row.updated_by);
		// $('#deleted_by').jqxNumberInput('val', row.deleted_by);
		// $('#created_at').val(row.created_at);
		// $('#updated_at').val(row.updated_at);
		// $('#deleted_at').val(row.deleted_at);
		$('#company_name').val(row.company_name);
		$('#email_id').val(row.email_id);
		// $('#depositer_type_id').jqxNumberInput('val',row.depositer_type_id);
		// $('#currency_id').jqxNumberInput('val',row.currency_id);
		$('#currency_id').jqxComboBox('val',row.currency_id);
		$('#country_id').jqxComboBox('val',row.country_id);
		$('#depositer_type_id').jqxComboBox('val',row.depositer_type_id);
		
		$('#contact_no').jqxNumberInput('val', row.contact_no);
		// $('#country_id').jqxNumberInput('val',row.country_id);
		$('#state').val(row.state);
		$('#city').val(row.city);
		$('#street').val(row.street);
		$('#zip').val(row.zip);
		
        openPopupWindow('jqxPopupWindowWholesaler_profile', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveWholesaler_profileRecord(){
    var data = $("#form-wholesaler_profiles").serialize();
	
	$('#jqxPopupWindowWholesaler_profile').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/wholesaler_profiles/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_wholesaler_profiles();
                $('#jqxGridWholesaler_profile').jqxGrid('updatebounddata');
                $('#jqxPopupWindowWholesaler_profile').jqxWindow('close');
            }
            $('#jqxPopupWindowWholesaler_profile').unblock();
        }
    });
}

function reset_form_wholesaler_profiles(){
	$('#wholesaler_profiles_id').val('');
    $('#form-wholesaler_profiles')[0].reset();
}
</script>