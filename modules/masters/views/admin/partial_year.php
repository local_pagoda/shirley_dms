

<div id="jqxPopupWindowYear">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-years', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "years_id"/>
            <table class="form-table">
				
				<tr>
					<td><label for='year'><?php echo lang('year')?></label></td>
					<td><input id='year' class='text_input' name='year'></td>
				</tr>
				
				
				<tr>
					<td><label for='status'><?php echo lang('status')?></label></td>
					<td><input id='status1' type='radio' value="1" name='status'>Yes
					<input id='status0' type='radio' value ="0" name='status'> No</td>
				</tr>

                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxYearSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxYearCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>
<div id='jqxGridYearToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridYearInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridYearFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridYear"></div>
<script language="javascript" type="text/javascript">

var mst_year= function(){

	var yearsDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			// { name: 'created_by', type: 'number' },
			// { name: 'updated_by', type: 'number' },
			// { name: 'deleted_by', type: 'number' },
			// { name: 'created_at', type: 'date' },
			// { name: 'updated_at', type: 'date' },
			// { name: 'deleted_at', type: 'date' },
			{ name: 'year', type: 'string' },
			{ name: 'status', type: 'string' },
		
			
        ],
		url: '<?php echo site_url("admin/years/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	yearsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridYear").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridYear").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridYear").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: yearsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridYearToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editYearRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					var f = '<a href ="javascript:void(0)" onclick="deleteYearRecord('+index+');return false;" title="Delete"><i class="fa fa-trash"></i></a>'
					return '<div style="text-align: center; margin-top: 8px;">' + e + '&nbsp;'+ f +'</div>';
				}
			},
			//{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
			{ text: '<?php echo lang("year"); ?>',datafield: 'year',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("status"); ?>',datafield: 'status',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridYear").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridYearFilterClear', function () { 
		$('#jqxGridYear').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridYearInsert', function () { 
		openPopupWindow('jqxPopupWindowYear', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowYear").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowYear").on('close', function () {
        reset_form_years();
    });

    $("#jqxYearCancelButton").on('click', function () {
        reset_form_years();
        $('#jqxPopupWindowYear').jqxWindow('close');
    });

    /*$('#form-years').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#year_one', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#year_one').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#year_two', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#year_two').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxYearSubmitButton").on('click', function () {
        saveYearRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveYearRecord();
                }
            };
        $('#form-years').jqxValidator('validate', validationResult);
        */
    });
}

function deleteYearRecord(index){
	var row =  $("#jqxGridYear").jqxGrid('getrowdata', index);
	var id =row.id;
	if(confirm("Are you sure want to delete!")==true){
		$.post("<?php echo site_url('admin/years/deleteYears')?>",{id:id},function(result){
			
			if(result){


				reset_form_years();
				
				$('#jqxGridYear').jqxGrid('updatebounddata');
			}
		});

}

}
function editYearRecord(index){
    var row =  $("#jqxGridYear").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#years_id').val(row.id);
  //       $('#created_by').jqxNumberInput('val', row.created_by);
		// $('#updated_by').jqxNumberInput('val', row.updated_by);
		// $('#deleted_by').jqxNumberInput('val', row.deleted_by);
		// $('#created_at').val(row.created_at);
		// $('#updated_at').val(row.updated_at);
		// $('#deleted_at').val(row.deleted_at);
		$('#year').val(row.year);
		
		if(row.status==1){
			
			$('input:radio[name=status][id=status1]').prop('checked',true);
			
		}else{
			
			$('input:radio[name=status][id=status0]').prop('checked',true);
		}
		
		
		
        openPopupWindow('jqxPopupWindowYear', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveYearRecord(){
    var data = $("#form-years").serialize();
	
	$('#jqxPopupWindowYear').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/years/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_years();
                $('#jqxGridYear').jqxGrid('updatebounddata');
                $('#jqxPopupWindowYear').jqxWindow('close');
            }
            $('#jqxPopupWindowYear').unblock();
        }
    });
}

function reset_form_years(){
	$('#years_id').val('');
    $('#form-years')[0].reset();
}
</script>