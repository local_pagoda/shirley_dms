

<div id="jqxPopupWindowDeposit_type">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-deposit_types', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "deposit_types_id"/>
            <table class="form-table">
				<!-- <tr>
					<td><label for='created_by'><?php echo lang('created_by')?></label></td>
					<td><div id='created_by' class='number_general' name='created_by'></div></td>
				</tr>
				<tr>
					<td><label for='updated_by'><?php echo lang('updated_by')?></label></td>
					<td><div id='updated_by' class='number_general' name='updated_by'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_by'><?php echo lang('deleted_by')?></label></td>
					<td><div id='deleted_by' class='number_general' name='deleted_by'></div></td>
				</tr>
				<tr>
					<td><label for='created_at'><?php echo lang('created_at')?></label></td>
					<td><input id='created_at' class='text_input' name='created_at'></td>
				</tr>
				<tr>
					<td><label for='updated_at'><?php echo lang('updated_at')?></label></td>
					<td><input id='updated_at' class='text_input' name='updated_at'></td>
				</tr>
				<tr>
					<td><label for='deleted_at'><?php echo lang('deleted_at')?></label></td>
					<td><input id='deleted_at' class='text_input' name='deleted_at'></td>
				</tr> -->
				<tr>
					<td><label for='deposit_types_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></td>
					<td><input id='deposit_types_name' class='text_input' name='name'></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxDeposit_typeSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxDeposit_typeCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<div id='jqxGridDeposit_typeToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridDeposit_typeInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridDeposit_typeFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridDeposit_type"></div>

<script language="javascript" type="text/javascript">

var mst_depositer= function(){

	var deposit_typesDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			// { name: 'created_by', type: 'number' },
			// { name: 'updated_by', type: 'number' },
			// { name: 'deleted_by', type: 'number' },
			// { name: 'created_at', type: 'date' },
			// { name: 'updated_at', type: 'date' },
			// { name: 'deleted_at', type: 'date' },
			{ name: 'name', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/deposit_types/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	deposit_typesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridDeposit_type").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridDeposit_type").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridDeposit_type").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: deposit_typesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridDeposit_typeToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editDeposit_typeRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					var f = '<a href ="javascript:void(0)" onclick="deleteDepositerRecord('+index+');return false;" title="Delete"><i class="fa fa-trash"></i></a>'
					return '<div style="text-align: center; margin-top: 8px;">' + e + '&nbsp;'+ f +'</div>';
				}
			},
			//{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridDeposit_type").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridDeposit_typeFilterClear', function () { 
		$('#jqxGridDeposit_type').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridDeposit_typeInsert', function () { 
		openPopupWindow('jqxPopupWindowDeposit_type', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowDeposit_type").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowDeposit_type").on('close', function () {
        reset_form_deposit_types();
    });

    $("#jqxDeposit_typeCancelButton").on('click', function () {
        reset_form_deposit_types();
        $('#jqxPopupWindowDeposit_type').jqxWindow('close');
    });

    /*$('#form-deposit_types').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deposit_types_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deposit_types_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxDeposit_typeSubmitButton").on('click', function () {
        saveDeposit_typeRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveDeposit_typeRecord();
                }
            };
        $('#form-deposit_types').jqxValidator('validate', validationResult);
        */
    });
}

function editDeposit_typeRecord(index){
    var row =  $("#jqxGridDeposit_type").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#deposit_types_id').val(row.id);
  //       $('#created_by').jqxNumberInput('val', row.created_by);
		// $('#updated_by').jqxNumberInput('val', row.updated_by);
		// $('#deleted_by').jqxNumberInput('val', row.deleted_by);
		// $('#created_at').val(row.created_at);
		// $('#updated_at').val(row.updated_at);
		// $('#deleted_at').val(row.deleted_at);
		$('#deposit_types_name').val(row.name);
		
        openPopupWindow('jqxPopupWindowDeposit_type', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}
function deleteDepositerRecord(index){
	var row =  $("#jqxGridDeposit_type").jqxGrid('getrowdata', index);
	var id =row.id;
	if(confirm("Are you sure want to delete!")==true){
		$.post("<?php echo site_url('admin/deposit_types/deleteDepositer')?>",{id:id},function(result){
			
			if(result){


				reset_form_styles();
				
				$('#jqxGridDeposit_type').jqxGrid('updatebounddata');
			}
		});

}

}
function saveDeposit_typeRecord(){
    var data = $("#form-deposit_types").serialize();
	
	$('#jqxPopupWindowDeposit_type').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/deposit_types/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_deposit_types();
                $('#jqxGridDeposit_type').jqxGrid('updatebounddata');
                $('#jqxPopupWindowDeposit_type').jqxWindow('close');
            }
            $('#jqxPopupWindowDeposit_type').unblock();
        }
    });
}

function reset_form_deposit_types(){
	$('#deposit_types_id').val('');
    $('#form-deposit_types')[0].reset();
}
</script>