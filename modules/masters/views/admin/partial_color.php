

<div id="jqxPopupWindowColor">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-colors', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "colors_id"/>
            <table class="form-table">
				<!-- <tr>
					<td><label for='created_by'><?php echo lang('created_by')?></label></td>
					<td><div id='created_by' class='number_general' name='created_by'></div></td>
				</tr>
				<tr>
					<td><label for='updated_by'><?php echo lang('updated_by')?></label></td>
					<td><div id='updated_by' class='number_general' name='updated_by'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_by'><?php echo lang('deleted_by')?></label></td>
					<td><div id='deleted_by' class='number_general' name='deleted_by'></div></td>
				</tr>
				<tr>
					<td><label for='created_at'><?php echo lang('created_at')?></label></td>
					<td><input id='created_at' class='text_input' name='created_at'></td>
				</tr>
				<tr>
					<td><label for='updated_at'><?php echo lang('updated_at')?></label></td>
					<td><input id='updated_at' class='text_input' name='updated_at'></td>
				</tr>
				<tr>
					<td><label for='deleted_at'><?php echo lang('deleted_at')?></label></td>
					<td><input id='deleted_at' class='text_input' name='deleted_at'></td>
				</tr> -->
				<tr>
					<td><label for='color'><?php echo lang('color')?></label></td>
					<td><input id='color' class='text_input' name='color'></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxColorSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxColorCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>
<div id='jqxGridColorToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridColorInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridColorFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridColor"></div>
<script language="javascript" type="text/javascript">

var mst_color =function(){

	var colorsDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			// { name: 'created_by', type: 'number' },
			// { name: 'updated_by', type: 'number' },
			// { name: 'deleted_by', type: 'number' },
			// { name: 'created_at', type: 'date' },
			// { name: 'updated_at', type: 'date' },
			// { name: 'deleted_at', type: 'date' },
			{ name: 'color', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/colors/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	colorsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridColor").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridColor").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridColor").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: colorsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridColorToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editColorRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';

					var f = '<a href ="javascript:void(0)" onclick="deleteColorRecord('+index+');return false;" title="Delete"><i class="fa fa-trash"></i></a>'
					return '<div style="text-align: center; margin-top: 8px;">' + e + '&nbsp;'+ f +'</div>';
				}
			},
			
			{ text: '<?php echo lang("color"); ?>',datafield: 'color',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridColor").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridColorFilterClear', function () { 
		$('#jqxGridColor').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridColorInsert', function () { 
		openPopupWindow('jqxPopupWindowColor', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowColor").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowColor").on('close', function () {
        reset_form_colors();
    });

    $("#jqxColorCancelButton").on('click', function () {
        reset_form_colors();
        $('#jqxPopupWindowColor').jqxWindow('close');
    });

    /*$('#form-colors').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#color', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#color').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxColorSubmitButton").on('click', function () {
        saveColorRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveColorRecord();
                }
            };
        $('#form-colors').jqxValidator('validate', validationResult);
        */
    });
}

function deleteColorRecord(index){
	var row =  $("#jqxGridColor").jqxGrid('getrowdata', index);
	var id =row.id;
	if(confirm("Are you sure want to delete!")==true){
		$.post("<?php echo site_url('admin/colors/deleteColors')?>",{id:id},function(result){
			
			if(result){


				reset_form_colors();
				
				$('#jqxGridColor').jqxGrid('updatebounddata');
			}
		});

}

}

function editColorRecord(index){
    var row =  $("#jqxGridColor").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#colors_id').val(row.id);
  //       $('#created_by').jqxNumberInput('val', row.created_by);
		// $('#updated_by').jqxNumberInput('val', row.updated_by);
		// $('#deleted_by').jqxNumberInput('val', row.deleted_by);
		// $('#created_at').val(row.created_at);
		// $('#updated_at').val(row.updated_at);
		// $('#deleted_at').val(row.deleted_at);
		$('#color').val(row.color);
		
        openPopupWindow('jqxPopupWindowColor', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveColorRecord(){
    var data = $("#form-colors").serialize();
	
	$('#jqxPopupWindowColor').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/colors/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_colors();
                $('#jqxGridColor').jqxGrid('updatebounddata');
                $('#jqxPopupWindowColor').jqxWindow('close');
            }
            $('#jqxPopupWindowColor').unblock();
        }
    });
}

function reset_form_colors(){
	$('#colors_id').val('');
    $('#form-colors')[0].reset();
}
</script>