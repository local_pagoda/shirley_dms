

<div id="jqxPopupWindowStyle">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-styles', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "styles_id"/>
            <table class="form-table">
				
				<tr>
					<td><label for='style_no'><?php echo lang('style_no')?></label></td>
					<td><input id='style_no' class='text_input' name='style_no'></td>
				</tr>
				<tr>
					<td><label for='style_name'><?php echo lang('style_name')?></label></td>
					<td><input id='style_name' class='text_input' name='style_name'></td>
				</tr>
				

				
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxStyleSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxStyleCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>
<div id='jqxGridStyleToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridStyleInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridStyleFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridStyle"></div>
<script language="javascript" type="text/javascript">

var mst_style= function(){

	var stylesDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			
			{ name: 'style_no', type: 'string' },
			{ name: 'style_name', type: 'string' },
			
			
			
        ],
		url: '<?php echo site_url("admin/styles/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	stylesDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridStyle").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridStyle").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridStyle").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: stylesDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridStyleToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editStyleRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';

					var f = '<a href ="javascript:void(0)" onclick="deleteStyleRecord('+index+');return false;" title="Delete"><i class="fa fa-trash"></i></a>'
					return '<div style="text-align: center; margin-top: 8px;">' + e + '&nbsp;'+ f +'</div>';
				}
			},
			
			{ text: '<?php echo lang("style_no"); ?>',datafield: 'style_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("style_name"); ?>',datafield: 'style_name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridStyle").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridStyleFilterClear', function () { 
		$('#jqxGridStyle').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridStyleInsert', function () { 
		openPopupWindow('jqxPopupWindowStyle', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowStyle").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowStyle").on('close', function () {
        reset_form_styles();
    });

    $("#jqxStyleCancelButton").on('click', function () {
        reset_form_styles();
        $('#jqxPopupWindowStyle').jqxWindow('close');
    });

    /*$('#form-styles').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#created_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#style_no', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#style_no').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#style_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#style_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxStyleSubmitButton").on('click', function () {
        saveStyleRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveStyleRecord();
                }
            };
        $('#form-styles').jqxValidator('validate', validationResult);
        */
    });
}

function editStyleRecord(index){
    var row =  $("#jqxGridStyle").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#styles_id').val(row.id);
  //       $('#created_by').jqxNumberInput('val', row.created_by);
		// $('#updated_by').jqxNumberInput('val', row.updated_by);
		// $('#deleted_by').jqxNumberInput('val', row.deleted_by);
		// $('#created_at').val(row.created_at);
		// $('#updated_at').val(row.updated_at);
		// $('#deleted_at').val(row.deleted_at);
		$('#style_no').val(row.style_no);
		$('#style_name').val(row.style_name);
		
		
        openPopupWindow('jqxPopupWindowStyle', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function deleteStyleRecord(index){
	var row =  $("#jqxGridStyle").jqxGrid('getrowdata', index);
	var id =row.id;
	if(confirm("Are you sure want to delete!")==true){
		$.post("<?php echo site_url('admin/styles/deleteStyles')?>",{id:id},function(result){
			
			if(result){


				reset_form_styles();
				
				$('#jqxGridStyle').jqxGrid('updatebounddata');
			}
		});

}

}


function saveStyleRecord(){
    var data = $("#form-styles").serialize();
	
	$('#jqxPopupWindowStyle').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/styles/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_styles();
                $('#jqxGridStyle').jqxGrid('updatebounddata');
                $('#jqxPopupWindowStyle').jqxWindow('close');
            }
            $('#jqxPopupWindowStyle').unblock();
        }
    });
}

function reset_form_styles(){
	$('#styles_id').val('');
    $('#form-styles')[0].reset();
}
</script>