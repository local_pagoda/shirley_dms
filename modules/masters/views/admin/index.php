<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Master Data</h1>
		<ol class="breadcrumb">
	        <li><a href="<?php echo site_url();?>"><?php echo lang('menu_home');?></a></li>
	        <li class="active"><?php echo lang('menu_masters');?></li>
      	</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div id='jqxTabs'>
					<ul>
						<li style="margin-left: 30px;"><?php echo lang('tab_index');?></li>
						<li style="margin-left: 30px;"><?php echo lang('tab_style');?></li>
						<li style="margin-left: 30px;"><?php echo lang('tab_color');?></li>
						<li style="margin-left: 30px;"><?php echo lang('tab_size');?></li>
						<li style="margin-left: 30px;"><?php echo lang('tab_currency');?></li>
						<li style="margin-left: 30px;"><?php echo lang('tab_depositer_type');?></li>
						<li><?php echo lang('tab_year');?></li>
						<li><?php echo lang('tab_season');?></li>
				
						
					</ul>
					
					<div class="tab_content"><?php echo $this->load->view('partial_index');?></div>
					<div class="tab_content"><?php echo $this->load->view('partial_style');?></div>
					<div class="tab_content"><?php echo $this->load->view('partial_color');?></div>
					<div class="tab_content"><?php echo $this->load->view('partial_sizes');?></div>
					<div class="tab_content"><?php echo $this->load->view('partial_currency');?></div>
					<div class="tab_content"><?php echo $this->load->view('partial_depositer_type');?></div>
					<div class="tab_content"><?php echo $this->load->view('partial_year');?></div>
					<div class="tab_content"><?php echo $this->load->view('partial_season');?></div>
					
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
			

<script language="javascript" type="text/javascript">

$(function(){
	var initWidgets = function (tab) {
		var tabName = $('#jqxTabs').jqxTabs('getTitleAt', tab);
		switch (tabName) {
			case '<?php echo lang("tab_style");?>':
				mst_style();
				break;
				case '<?php echo lang("tab_color");?>':
				mst_color();
				break;
				case '<?php echo lang("tab_size");?>':
				mst_size();
				break;
				case '<?php echo lang("tab_currency");?>':
				mst_currency();
				break;
				case '<?php echo lang("tab_depositer_type");?>':
				mst_depositer();
				break;
				case '<?php echo lang("tab_year");?>':
				mst_year();
				break;
				case '<?php echo lang("tab_season");?>':
				mst_season();
				break;

				
			
		}
	};

	$('#jqxTabs').jqxTabs({ width: '100%', height: gridHeight, position: 'top', theme: theme, initTabContent: initWidgets});    

	$('.select-tab').on('click', function(){
		var tabindex = parseInt(this.getAttribute("data-tabindex"));
		$('#jqxTabs').jqxTabs({ selectedItem: tabindex });
	});
});



</script>
