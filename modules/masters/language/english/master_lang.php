<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['style_no'] = 'Style No';
$lang['style_name'] = 'Style Name';
$lang['product_code'] = 'Product Code';
$lang['color'] = 'Color';
$lang['size'] = 'Size';
$lang['description'] = 'Description';
$lang['currency'] = 'Currency';
$lang['year'] = 'Year';
$lang['season'] = 'Season';

$lang['price'] = 'Price';
$lang['status'] = 'Status';


$lang['name'] = 'Name';

$lang['deposit_types']='Deposit Types';
$lang['styles']='Styles';

$lang['masters'] = "Master Data";

$lang['tab_color'] 	= 'Color';
$lang['tab_style'] 	= 'Style';
$lang['tab_size'] 	= 'Size';
$lang['tab_currency'] 	= 'Currency';
$lang['tab_depositer_type'] 	= 'Depositer Type';
$lang['tab_year'] 	= 'Year';
$lang['tab_season'] 	= 'Season';





$lang['tab_category']		= 'Category';
$lang['tab_index'] 			= 'Choose Master';
$lang['showroom_workshop'] 	= 'Workshop / Showroom';





