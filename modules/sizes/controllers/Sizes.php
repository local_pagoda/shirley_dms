<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Sizes
 *
 * Extends the Public_Controller class
 * 
 */

class Sizes extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Sizes');

        $this->load->model('sizes/size_model');
        $this->lang->load('sizes/size');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('sizes');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'sizes';
		$this->load->view($this->_container,$data);
	}
}