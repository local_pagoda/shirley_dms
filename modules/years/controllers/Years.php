<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Years
 *
 * Extends the Public_Controller class
 * 
 */

class Years extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Years');

        $this->load->model('years/year_model');
        $this->lang->load('years/year');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('years');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'years';
		$this->load->view($this->_container,$data);
	}
}